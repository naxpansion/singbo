

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta charset="UTF-8" /><meta name="format-detection" content="telephone=no" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /><title>
	singbet9
</title>
    <link rel="stylesheet" href="../css/style.css" type="text/css" />
    <link rel="stylesheet" href="../css/mod.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700italic,700" rel="stylesheet" type="text/css" /><link href="https://fonts.googleapis.com/css?family=Marcellus" rel="stylesheet" type="text/css" /><link href="https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic" rel="stylesheet" type="text/css" /><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css" /><link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../lib/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../lib/js/jquery.form.min.js"></script>
    <link rel="icon" type="image/png" href="../images/icon_tb9.png" /><link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato" />


    <!-- ************************** LOGIN ************************** -->
    <script type="text/javascript" src="../lib/js/checkFields.js"></script>
    <script type="text/javascript">
        function doSubmit() {
            var txtUsername = document.getElementById("ctl00_txtUsername");
            var txtPass = document.getElementById("ctl00_txtPass");

            if (txtUsername.value == "") { alert("Please key in Username."); txtUsername.focus(); return false; }
            else if (txtPass.value == "") { alert("Please key in Password."); txtPass.focus(); return false; }
            else { return true; }
        }

        function doDeskVer() {
            var btnDeskVer = document.getElementById("ctl00_btnDeskVer");
            btnDeskVer.click();
        }
    </script>
    <!-- ************************** LOGIN ************************** -->

    <!-- ************************** SCRIPT: TICKER ************************** -->
    <script type="text/javascript" src="../lib/js/jquery.marquee/jquery.marquee.min.js"></script>
    <!-- ************************** SCRIPT: TICKER ************************** -->

    <!-- *************** NIVO SLIDER *************** -->
	<link rel="stylesheet" href="../lib/js/nivo/themes/default/default.css" type="text/css" media="screen" /><link rel="stylesheet" href="../lib/js/nivo/nivo-slider.css" type="text/css" media="screen" />
    <!-- *************** NIVO SLIDER *************** -->

    
    <script type="text/javascript">
        if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) { location.href = "/"; }

        function doFPWD() {
            var txtContNo = document.getElementById("ctl00_cphBody_txtContNo");
            var txtMFC = txtContNo.value.charAt(0);

            if (txtContNo.value == "") { alert("Please key in Contact Number"); txtContNo.focus(); return false; }
            else if (!isNumeric(txtContNo)) { alert("Please key in numerical values only."); txtContNo.focus(); return false; }
            else if (txtMFC != "0") { alert("Invalid Country Code"); txtContNo.focus(); return false; }
            else if (txtContNo.value.length < 10) { alert("Please key in minimum 10 characters"); txtContNo.focus(); return false; }
            else { return true; }
        }

        function doReset() {
            var txtContNo = document.getElementById("ctl00_cphBody_txtContNo");
            txtContNo.value = ""; return false;
        }
    </script>
</head>
<body style="-webkit-overflow-scrolling: touch;">

    <form name="aspnetForm" method="post" action="_fp.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE4NTk5MzIwMDAPZBYCZg9kFgICAw9kFhBmDw8WAh4EVGV4dAUFTE9HSU5kZAIBDw8WAh8ABQhSRUdJU1RFUmRkAgIPDxYCHwAFB1BST0ZJTEVkZAIDD2QWDGYPDxYCHwAFB0RFUE9TSVRkZAIBDw8WAh8ABQhXSVRIRFJBV2RkAgIPDxYCHwAFCFRSQU5TRkVSZGQCAw8PZBYCHgtwbGFjZWhvbGRlcgUIVXNlcm5hbWVkAgQPD2QWAh8BBQhQYXNzd29yZGQCBQ8PFgIfAAUFTE9HSU4WAh4Hb25jbGljawUScmV0dXJuIGRvU3VibWl0KCk7ZAIGDw8WAh8ABRZTd2l0Y2ggdG8gRGVza3RvcCBWaWV3ZGQCDg9kFgQCAw8PFgIfAAUFUmVzZXQWAh8CBRFyZXR1cm4gZG9SZXNldCgpO2QCBA8PFgIfAAUGU3VibWl0FgIfAgUQcmV0dXJuIGRvRlBXRCgpO2QCEQ8PFgIfAAULTU9CSUxFIFNJVEVkZAISDw8WAh8ABQxERVNLVE9QIFZFUi5kZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBQUPY3RsMDAkYnRuTGFuZ0NOBQ9jdGwwMCRidG5MYW5nRU4FEGN0bDAwJG1CdG5MYW5nRU4FEGN0bDAwJG1CdG5MYW5nQk0FEGN0bDAwJG1CdG5MYW5nQ06hG3GGfO7hWZuPK+n4tj1RafnJIg==" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="372805FB" />
        <!-- ------------- MARQUEE ------------- -->
        
        <div id="cover-layer" class="mask" onclick="closeNav()"></div>
        <div id="forheader">
            <div id="header">
                <div id="logo">
                    <div id="menu-open" class="m-menu" onclick="openNav()">
                        <img src="../images/m_select.png" class="selectpic" alt="" />
                    </div>
                    <div id="menu-close" class="m-menu" onclick="closeNav()" style="display: none;">
                        <img src="../images/close-btn.png" class="selectpic" alt="" />
                    </div>
                    <a href="../index.html"><img src="../images/logo.png" class="m_logo" alt="" /></a>
                    
                    
                        <div class="m-login">
                            <input type="submit" name="ctl00$m_btnLogin" value="LOGIN" id="ctl00_m_btnLogin" class="btn-login btn-login-m" />
                            <input type="submit" name="ctl00$m_btnReg" value="REGISTER" id="ctl00_m_btnReg" class="btn btn-xm" />
                        </div>
                    
                        <div class="l_button" onclick="location.href='/m/login.aspx'">
                            <span class="l_button_effect">
                                <p>LOGIN</p>
                            </span>
                        </div>
                    
                </div>
                <div hidden class="contact" >
                    <img src="../images/call.png" alt="" /><p>016-3020303 / 016-3020505</p>
                    <img src="../images/wechat.png" alt="" /><p>singbet9 / singbet9</p>
                    <img src="../images/mail.png" alt="" /><p><a href="../cdn-cgi/l/email-protection/index.html" class="__cf_email__" data-cfemail="f59c96998097ccb59298949c99db969a98">[email&#160;protected]</a></p>
                </div>
                <div style="float: right;">
                    <div class="center">
                        <div class="center-panel">
                            <div id="ctl00_pnlProfile">
	
                                
                                <div class="login">
                                    <input name="ctl00$txtUsername" type="text" id="ctl00_txtUsername" tabindex="1" class="headfld" placeholder="Username" />
                                    <div style="position: relative;">
                                        <div>
                                            <input name="ctl00$txtPass" type="password" maxlength="16" id="ctl00_txtPass" tabindex="2" class="headfld" placeholder="Password" />
                                        </div>
                                        <input id="pop-btnFp" type="button" value="Forgot?" onclick="doFP()" class="btnfp" disabled />
                                    </div>
                                    <input type="submit" name="ctl00$btnSubmit" value="LOGIN" onclick="return doSubmit();" id="ctl00_btnSubmit" tabindex="3" class="btn-login" disabled="" />
                                    <input id="pop-btnReg" type="button" value="REGISTER" onclick="doReg()" class="btn" disabled />
                                </div>
                                
</div>
                        </div>

                        <div id="language" class="lang" style="display:none">
                            <input type="image" name="ctl00$btnLangCN" id="ctl00_btnLangCN" src="../images/lang_cn.png" border="0" style="margin-left: 2px;" />
                            
                            <input type="image" name="ctl00$btnLangEN" id="ctl00_btnLangEN" src="../images/lang_en.png" border="0" style="margin-left: 2px;" />
                        </div>
                    </div>

                </div>

            </div>
        </div>
        
        <div id="mySidenav" class="sidenav">
            <div class="sidenav-container">
                <a href="../index.html">HOME</a>
                <div class="sidenav-profile">
                    
                    <a href="../r_m/index.html">REGISTER</a>
                    
                </div>
                <a href="../p/sport/index.html">SPORTSBOOK</a>
                <a href="../p/live/index.html">LIVE CASINO</a>
                <a href="../index.html">SLOT GAME</a>
                <a href="../promo/index.html">PROMOTIONS</a>
                <a href="../c/index-9661.html">VIP CLUB</a>
                <a href="../c/index-25913.html">BANKING</a>
                <a href="../c/index-26115.html">CONTACT US</a>
                <a href="../lang.aspx" style="display:none">LANGUAGE</a>
                
                <div style="width:100%; clear:both; height: 15vh;"></div>
            </div>
            <div class="sidenav-footer">
                <input type="submit" name="ctl00$btnDeskVer" value="Switch to Desktop View" id="ctl00_btnDeskVer" class="btnDesk" />
                
                <p>Copyright© 2019 singbet9 All Rights Reserved</p>
            </div>
        </div>
        <div><script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="../lib/js/navbar.js"></script></div>
        
        <div class="nav">
            <div class="nav-container">
                <ul>
                    <li class="">
                        <div onclick="window.location='/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_home.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>HOME</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/p/sport/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_sport.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>SPORTSBOOK</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/p/live/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_live.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>LIVE CASINO</p>
                            </div>
                        </div>
                    </li>
                    <li class="nav-over">
                        <div onclick="window.location='/p/slot/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_slot.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>SLOT GAME</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/promo/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_promo.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>PROMOTIONS</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/c/?i=c6'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_vip.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>VIP CLUB</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/c/?i=c2'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="../images/icon_bank.png" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>BANKING</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <!-- ************************ BANNERS ************************ -->
        <div id="divBan">
            <div id="slider">
                <div class="divBanS">
                    <div class="slider-wrapper theme-default">
                        <div class="nivoSlider homeslider">
                            <img src="../images/ban_home1.jpg" alt="" />
                            <img src="../images/ban_home2.jpg" alt="" />
                            <img src="../images/ban_home3.jpg" alt="" />
                            <img src="../images/ban_home4.jpg" alt="" />
                        </div>
                    </div>
                </div>
	        </div>
            <div id="m-slider" style="display:none;">
                <div style="width:100%;">
                    <div class="slider-wrapper theme-default">
                        <div class="nivoSlider homeslider">
                            <img src="../images/m-pban1.jpg" alt="" />
                            <img src="../images/m-pban2.jpg" alt="" />
                            <img src="../images/m-pban3.jpg" alt="" />
                            <img src="../images/m-pban4.jpg" alt="" />
                            <img src="../images/m-pban5.jpg" alt="" />
                            <img src="../images/m-pban6.jpg" alt="" />
                            <img src="../images/m-pban7.jpg" alt="" />
                            <img src="../images/m-pban8.jpg" alt="" />
                        </div>
                    </div>
                </div>
	        </div>
        </div>
        <!-- ************************ BANNERS ************************ -->

        <div class="m-nav" id="m-nav">
            <div class="m-nav-container">
                <ul>
                    <li class="mnc-over" onclick="location.href='/'"><div class="m-nav-items"><div class="m-nav-items-text">SLOT GAME</div></div></li>
                    <li class="" onclick="location.href='/p/live/'"><div class="m-nav-items"><div class="m-nav-items-text">LIVE GAME</div></div></li>
                    <li class="" onclick="location.href='/p/sport/'"><div class="m-nav-items"><div class="m-nav-items-text">SPORT</div></div></li>
                    <li class="" onclick="location.href='/promo/'"><div class="m-nav-items"><div class="m-nav-items-text">PROMOTIONS</div></div></li>
                </ul>
            </div>
        </div>

        

        <div id="forcontainer" style="display: none">
            <div id="m_language" style="display: none;">
                <input type="image" name="ctl00$mBtnLangEN" id="ctl00_mBtnLangEN" src="../images/m_lang_en.png" border="0" style="margin: 20px" />
                <input type="image" name="ctl00$mBtnLangBM" id="ctl00_mBtnLangBM" src="../images/m_lang_my.png" border="0" style="margin: 20px" />
                <input type="image" name="ctl00$mBtnLangCN" id="ctl00_mBtnLangCN" src="../images/m_lang_cn.png" border="0" style="margin: 20px" />
            </div>

            <div id="container">
                <input type="checkbox" id="show-menu" role="button" />
                <div id="menu" style="display: none">
                    <ul>
                        
                        <li><a href="../index.html">HOME</a></li>
                        
                        <li><a href="../p/index-684.html">SPORTSBOOK</a></li>
                        <li><a href="../p/index-36024.html">LIVE CASINO</a></li>
                        <li><a href="../p/index-10741.html">MOBILE CASINO</a></li>
                        <li><a href="../promo/index.html">PROMOTIONS</a></li>
                        <li><a href="../c/index-25913.html">BANKING</a></li>
                        
                        <li class="divfp"><a href="_fp.aspx">FORGOT PASSWORD</a></li>
                        
                    </ul>
                </div>
            </div>

            

            <div id="approved_menthod" style="display: none">
                <img src="../images/approved.png" style="position: absolute; margin-top: -50px;" alt="" />
                

                <div id="bank">
                    <img src="../images/payment.png" alt="" /><br />
                    <br />
                    <img src="../images/cimbbank.jpg" alt="" />
                    <img src="../images/maybank.jpg" alt="" />
                    <img src="../images/public.jpg" alt="" />
                    <img src="../images/hongleong.jpg" alt="" />
                </div>
                <div class="contact">
                    <br />
                    <img src="../images/call.png" alt="" /><p>016-3020303 / 016-3020505</p>
                    <img src="../images/wechat.png" alt="" /><p>singbet9 / singbet9</p>
                    <img src="../images/mail.png" alt="" /><p><a href="../cdn-cgi/l/email-protection/index.html" class="__cf_email__" data-cfemail="b8d1dbd4cdda81f8dfd5d9d1d496dbd7d5">[email&#160;protected]</a></p>
                </div>
            </div>

        </div>
        
        
        
        <div style="display: none;">
            
        </div>

        
    <div class="m-fp">
        <div class="errMsg">
            <div style="width:90%;display:inline-block;">
                
                
            </div>
        </div>

        <div align="center">
            <h2 style="color:#fff;">FORGOT PASSWORD</h2>
        </div>
        <div class="fp-desc">
            <span>Please fill in your contact number, our<br /> customer service will contact you shortly.</span>
        </div>
        <div class="fp-field">
            <label class="field-lbl">Contact No *</label>
            <input name="ctl00$cphBody$txtContNo" type="text" id="ctl00_cphBody_txtContNo" placeholder="0123456789" class="field-input" />
            <span class="alertfont"><span class="red">*</span> Phone number format should be in 0123456789 <span class="red">*</span></span>
        </div>
        <div class="fp-btn">
            <div class="con-btn">
                <input type="submit" name="ctl00$cphBody$btnReset" value="Reset" onclick="return doReset();" id="ctl00_cphBody_btnReset" class="btn clear" />
                <input type="submit" name="ctl00$cphBody$btnSend" value="Submit" onclick="return doFPWD();" id="ctl00_cphBody_btnSend" class="btn" />
            </div>
        </div>
    </div>


        <div class="spaceFoot"></div>

        <div class="p-contact">
            <div class="m-social">

                <div class="m-xapps " onclick="location.href='/'">
                    <img src="../images/m-home.png" alt="" />
                    HOME
                </div>
                
                    <div class="m-xapps " onclick="location.href='/c/?i=c10'">
                        <img src="../images/m-contact.png" alt="" />
                        CONTACT
                    </div>
                    <div class="m-xapps">
                        <img src="../images/m-whatsapp.png" alt="" onclick="window.open('https://api.whatsapp.com/send?phone=60162653626');" />
                        WHATSAPP
                    </div>
                    <div class="m-xapps" onclick="doDeskVer()">
                        <img src="../images/m-desktop.png" alt="" />
                        DESKTOP
                    </div>
                
                <div class="m-xapps" onclick="window.open('https://secure.livechatinc.com/licence/10564322/open_chat.cgi');">
                    <img src="../images/m-livechat.png" alt="" />
                    LIVE CHAT
                </div>

                
            </div>
        </div>
        
        <div class="footer">

            

            <div align="center">
                <div id="footer">
                    

                    <div id="menu2">
                        <div align="center">
                            <div class="menu-footer">
                                <a href="../c/index-5598.html">ABOUT US</a>
                                <a href="../c/index-1535.html">TERMS & CONDITIONS</a>
                                <a href="../c/index-29976.html">FAQ</a>
                                <a href="../c/index-26115.html">CONTACT US</a>
                                

                                
                            </div>
                        </div>
                    </div>

                    <div style="width:100%; clear: both; height: 2vh"></div>

                    <div class="divfoot1">
                    
                        <div class="divfoot2" style="width:18%;">
                            <span class="gry">24/7 CUSTOMER SUPPORT</span><br /><br />
                            LIVE CHAT<br />
                            wechat id : <b class="gry lowCase">singbet9</b><br />                            email : <a href="../cdn-cgi/l/email-protection/index.html#79101a150c1b400e1017391e14181015571a1614" class="lowCase gry bold" target="_blank"><span class="__cf_email__" data-cfemail="59303a352c3b602e3037193e34383035773a3634">[email&#160;protected]</span></a><br />                            whatsapp: <a href="http://singbet9.wasap.my" class="lowCase gry bold" target="_blank">singbet9.wasap.my</a>
                        </div>
                        <div class="divfoot2" style="width:15%;">
                            <span class="gry">singbet9 PRODUCT</span><br /><br />
                            <a href="../p/slot/index.html">SLOT GAME</a><br />
                            <a href="../p/live/index.html">LIVE CASINO</a><br />
                            <a href="../p/sport/index.html">SPORTSBOOK</a>
                        </div>
                        <div class="divfoot2" style="width:18%;">
                            <span class="gry">payment methods</span><br /><br />
                            <a href="../c/index-25913.html">MAYBANK</a><br />
                            <a href="../c/index-25913.html">HONG LEONG</a><br />
                            <a href="../c/index-25913.html">PUBLIC BANK</a><br />
                            <a href="../c/index-25913.html">RHB BANK</a><br />
                            <a href="../c/index-25913.html">INTERBANK TRANSFER</a>
                        </div>
                        <div class="divfoot2" style="width:18%;">
                            <span class="gry">singbet9 links</span><br /><br />
                            <a href="../c/index-5598.html">ABOUT US</a><br />
                            <a href="../c/index-1535.html">TERMS & CONDITIONS</a><br />
                            <a href="../promo/index.html">PROMOTIONS</a>
                        </div>
                        <div class="divfoot2">
                            <span class="gry">HELP</span><br /><br />
                            <a href="../c/index-13724.html">HOW TO DEPOSIT</a><br />
                            <a href="../c/index-50291.html">HOW TO WITHDRAW</a><br />
                            <a href="../c/index-54354.html">HOW TO TRANSFER</a>
                        </div>
                    </div>

                    <div class="divfootsep"></div>

                    <div class="divfootc1">
                        <div class="divfootc1-1">
                            <span class="dfc-tit">About Us</span><br /><br />
                            singbet9 is the number one online casino gaming site for Malaysian players looking to enjoy the best online slots, blackjack, baccarat, roulette, arcade games, sportsbook betting and other exciting games. The games in singbet9 are divided into suites, each with its own uniqueness and superlative features to ensure you will enjoy a prolonged gaming experience.<br /><br />

                            singbet9 Online Casino and Sportsbook is powered by the industry’s top software platforms –
                            Maxbet, SBO, Rollex, Lucky Palace, 918Kiss, Newtown, Ace333, 3win8, JOKER and Playboy.
                            We offering safe and fair gaming services in online slots and games, live dealer casino, and sports betting.
                            <br /><br /><br />

                            <span class="dfc-tit">Download Free Online Casino Software or Play Instantly</span><br /><br />                            Play singbet9’s Casino games the way you like! singbet9 offers a 'no download' Flash gaming platform, enabling you to instantly play a huge and diverse range of over 150 popular, as well as up and coming new games. Most of the popular game titles are available with free trials and complimentary credits, without any risks involved, under the ‘fun play’ option. Or, you may choose to add some thrill and excitement to your game with real money! Choosing to download our free casino software directly onto your computer gives you access to a bigger and wider collection of games that you may not find in the instant play 'no download' version. For those who are always on the move, singbet9’s mobile casino will keep you entertained wherever you go. Our mobile casino consists of multiple gaming applications which supports Android and iOS. It will definitely be worth your while if you happen to be stuck waiting for something and you might just want to kill time by slipping in some casino fun time!<br /><br /><br />
                        </div>
                        <div class="divfootc1-2">
                            <span class="dfc-tit">The Bonuses Just Get Bigger and It Never Ends</span><br /><br />                            One of the most popular bonuses for newly registered players is singbet9’s Welcome Bonus, which offers a 100% Bonus of up to MYR 388. Other promotions which are open to all singbet9 members got First Daily Deposit, Special Daily Deposit, Referral BONUS and many! Almost all of singbet9’s fabulous promotions run 365 days a year, meaning there will never be a day you’ll play without a bonus!<br /><br /><br />

                            <span class="dfc-tit">Secured, Trusted & Reliable Online Casino Malaysia</span><br /><br />
                            singbet9 Online Casino Malaysia offers all Malaysians the most secured, trusted and reliable online gaming and sportsbook betting services, allowing everyone to play online casino games or bet on their favourite sports matches conveniently with peace of mind. We guarantee you that we are the best online casino site, and we ensure your privacy of sensitive personal information and the security of all money transactions are not shared to anyone at all costs. singbet9’s casino and betting software system has been installed with the latest firewall and encryption technology. All money and wagering transactions are carried out in Malaysian Ringgit (MYR) via local online transfer or manual transfer via local cash deposit machines.<br /><br /><br />

                            <span class="dfc-tit">Quality Customer Service 24/7</span><br /><br />
                            To ensure the best experience and hassle-free casino playing session at singbet9, we have made our customer support team available to all, 24 hours a day and 7 days a week. Our in-house support team are not robots, but they are a group of skilled customer service representatives who know exactly how to help and assist you with anything related to singbet9. The fastest and most efficient way to reach us is by chatting with us on LiveChat, where you’ll get your answers anytime of the day within seconds. You can also contact us via Whatsapp or WeChat, details are available at our website page.<br /><br /><br />
                        </div>
                    </div>

                    <div class="divfootsep"></div>

                    <div class="divfootbnk">
                        BANKS<br />
                        <img src="../images/bank_bw_cimb.png" alt="" />
                        <img src="../images/bank_bw_mbb.png" alt="" />
                        <img src="../images/bank_bw_rhb.png" alt="" />
                        <img src="../images/bank_bw_hlb.png" alt="" />
                        <br /><br />
                        <span class="cpyrght">Copyright &copy; 2019 singbet9. All Rights Reserved.</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- *************************************  MAGNIFIC POPUP ************************************* -->
        <a href="#mfp-reg" class="open-popup-link" id="mfpReg"></a>
        <div id="mfp-reg" class="black-popup2 mfpReg mfp-hide"></div>

        <a href="#mfp-fp" class="open-popup-link" id="mfpfp"></a>
        <div id="mfp-fp" class="black-popup2 mfpfp mfp-hide"></div>

        <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">

            

            function doFP() {
            
                $.get("/m/fp.aspx", function (respondHtml) {
                    if (respondHtml) { $('#mfp-fp').html(respondHtml); $('#mfpfp').trigger('click'); }
                })
                //$('#mfp-fp').html("Hello <b>world</b>!"); $('#mfpfp').trigger('click');
            
            }
            function doReg() {
            
                $.get("/r/index.aspx").done(function (respondHtml) {
                    if (respondHtml) { $('#mfp-reg').html(respondHtml); $('#mfpReg').trigger('click'); }
                })
                //$('#mfp-reg').html("Hello <b>world</b>!"); $('#mfpReg').trigger('click');
            
            }

            // -- ******************  MAGNIFIC POPUP ****************** -- //
            $(window).load(function () {
                $("#pop-btnFp, #pop-btnReg, #pop-btnJoin, #ctl00_btnSubmit").removeAttr("disabled");
            });
            $(document).ready(function () {
                // --  MFP -- //
                $('.open-popup-link').magnificPopup({
                    type: 'inline',
                    midClick: true,
                    callbacks: {
                        close: function () {
                            var elemID = this.currItem.src;
                        

                            if (elemID == "#mfp-post-login") {
                            
                            }
                            else if ((elemID == "#mfp-txn") || (elemID == "#mfp-toreb") || (elemID == "#mfp-agrep") || (elemID == "#mfp-agdl") || (elemID == "#mfp-ann")) {
                                // -- DO NOTHING -- //
                            }
                            else {
                                location.reload();
                            }
                        }
                    }
                });
            });

            // -- ****************** JQUERY.MARQUEE ****************** -- //
            $('.marquee').marquee({
                //speed in milliseconds of the marquee
                duration: 15000,
                //gap in pixels between the tickers
                gap: 50,
                //time in milliseconds before the marquee will start animating
                delayBeforeStart: 0,
                //'left' or 'right'
                direction: 'left',
                //true or false - should the marquee be duplicated to show an effect of continues flow
                duplicated: true,
                pauseOnHover: true
            });
        </script>

        <!-- *************************************  MAGNIFIC POPUP ************************************* -->
        <link rel="stylesheet" href="../lib/js/mfp/mfp.css" />
        <script type="text/javascript" src="../lib/js/mfp/mfp.js"></script>
        <!-- *************************************  MAGNIFIC POPUP ************************************* -->

        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript" src="../lib/js/nivo/jquery.nivo.slider.js"></script>
        <script type="text/javascript">
            $(window).load(function () {
                $('.homeslider').nivoSlider({ controlNav: false, effect: 'fade' });
                //Hide Nivo Left Right Nav if only one image
                var count_nivoimg = $(".nivoSlider>img").length //count children with img tag
                if (count_nivoimg <= 2) {
                    $(".nivoSlider .nivo-directionNav").hide();
                } 
            })

            
        </script>

        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            if (matchMedia('only screen and (min-width: 641px)').matches) {
              window.__lc = window.__lc || {};
              window.__lc.license = 10564322;
              (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
              })();
            }
        </script>
        <noscript>
        <a href="https://www.livechatinc.com/chat-with/10564322/" rel="nofollow">Chat with us</a>,
        powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
        </noscript>
        <!-- End of LiveChat code -->

        

    <script type="text/javascript">
        document.getElementById("divBan").style.display = "none";
        document.getElementById("m-nav").style.display = "none";
        //document.getElementById("bank_container").id = 'register_container';
        //document.getElementById("faq_content").id = 'register_content';
    </script>


    </form>
</body>
</html>

<!-- Localized -->