


//Initialize for READ MORE
ddaccordion.init({
	headerclass: "accordionBanner", //Shared CSS class name of headers group
	contentclass: "accordionContent", //Shared CSS class name of contents group
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["closedreadMore", "openreadMore"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["none", "", ""],
	//togglehtml: ["src", "images/ico_plus.gif", "images/ico_minus.gif"],
	//togglehtml: ["suffix", "<img src='http://i13.tinypic.com/80mxwlz.gif' /> ", "<img src='http://i18.tinypic.com/6tpc4td.gif' /> "], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "normal", //speed of animation: "fast", "normal", or "slow"
	oninit:function(expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isclicked){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})