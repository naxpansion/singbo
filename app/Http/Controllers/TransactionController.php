<?php

namespace App\Http\Controllers;

use App\Transaction;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\BankRecord;
use App\Log;
use Auth;
use App\User;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.transaction.index');
    }

    public function data(Datatables $datatables)
    {   
        
        $arrStart = explode("-", Input::get('from'));
        $arrEnd = explode("-", Input::get('to'));
        $from = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        $transactions = Transaction::between($from, $to);

        return Datatables::of($transactions)
            ->addColumn('actions', function($transaction) {
                return view('admin.transaction.action', compact('transaction'))->render();
            })
            ->editColumn('transaction_id', function ($transaction) {
                return '#'.sprintf('%05d', $transaction->id);
            })
            ->editColumn('status', function ($transaction) {
                
                if($transaction->status == 1)
                {
                    return '<span class="label label-warning">Progress</span>';
                }
                else if($transaction->status == 2)
                {
                    return '<span class="label label-success">Complete</span>';
                }
                else if($transaction->status == 3)
                {
                    return '<span class="label label-danger">Decline</span>';
                }


            })
            ->editColumn('group', function ($transaction) {
                return $transaction->user->group->name;
            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at ? with(new Carbon($transaction->created_at))->format('d M Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function deposit()
    {
        if(\Auth::user()->role == 1)
        {

            return view('admin.transaction.deposit');

        }
        else
        {
            return view('staff.transaction.deposit');
        }

    }

    public function depositData(Datatables $datatables,Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['from_date']);
        $arrEnd = explode("-", $input['to_date']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        if($input['status'] == '')
        {
            $transactions = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->with('user')->with('logLatest');
        }
        else
        {
            $transactions = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->where('status',$input['status'])->latest()->with('user')->with('logLatest');
        }

        return Datatables::of($transactions)
            ->addColumn('actions', function($transaction) {
                return view('admin.transaction.action', compact('transaction'))->render();
            })
            ->editColumn('transaction_id', function ($transaction) {
                return '#D'.sprintf('%06d', $transaction->id);
            })
            ->editColumn('email', function ($transaction) {
                return $transaction->user->email;
            })
            ->editColumn('group', function ($transaction) {
                return $transaction->user->group->name;
            })
            ->editColumn('referred_by', function ($transaction) {
                if($transaction->user->referred_by == null)
                {
                    return '-';
                }
                else
                {
                    $referred = \App\User::where('affiliate_id',$transaction->user->referred_by)->first();

                    if($referred){
                        return $referred->name;
                    }
                }
            })
            ->editColumn('game', function ($transaction) {

                $data = json_decode($transaction->data, true);
                $game = \App\Game::find($data['game_id']);


                if($game)
                {
                    $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();
                    if($game_id){
                        $username =  $game_id->username;
                    }
                    else
                    {
                        $username = "-";
                    }

                    return $game->name.' / '.$username;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('bonus_id', function ($transaction) {
                if($transaction->bonus_id == null)
                {
                    return 'No Bonus';
                }
                else
                {
                    return $transaction->bonus->name;
                }
            })
            ->editColumn('group', function ($transaction) {
                return $transaction->user->group->name;
            })
            ->editColumn('updated_by', function ($transaction) {
                if($transaction->logLatest->count() > 0)
                {
                    return $transaction->logLatest->first()->user->name;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('status', function ($transaction) {
                
                if($transaction->status == 1)
                {
                    return '<span class="label label-warning">Progress</span>';
                }
                else if($transaction->status == 2)
                {
                    return '<span class="label label-success">Complete</span>';
                }
                else if($transaction->status == 3)
                {
                    return '<span class="label label-danger">Decline</span>';
                }


            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at ? with(new Carbon($transaction->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function withdrawal()
    {
        if(\Auth::user()->role == 1)
        {

            return view('admin.transaction.withdrawal');

        }
        else
        {
            return view('staff.transaction.withdrawal');
        }


    }

    public function withdrawalData(Datatables $datatables,Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['from_date']);
        $arrEnd = explode("-", $input['to_date']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        if($input['status'] == '')
        {
            $transactions = Transaction::where('transaction_type','withdraw')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->with('user')->with('logLatest');
        }
        else
        {
            $transactions = Transaction::where('transaction_type','withdraw')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->where('status',$input['status'])->latest()->with('user')->with('logLatest');
        }
        return Datatables::of($transactions)
            ->addColumn('actions', function($transaction) {
                return view('admin.transaction.action', compact('transaction'))->render();
            })
            ->editColumn('transaction_id', function ($transaction) {
                return '#W'.sprintf('%06d', $transaction->id);
            })
            ->editColumn('user_id', function ($transaction) {
                return $transaction->user->username;
            })
            ->editColumn('email', function ($transaction) {
                return $transaction->user->email;
            })
            ->editColumn('bonus', function ($transaction) {

                $last_transaction = \App\Transaction::where('user_id',$transaction->user->id)->where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->latest()->first();

                if(!$last_transaction)
                {
                    return '-';
                }
                else
                {
                    if($last_transaction->bonus == null)
                    {
                        return '-';
                    }
                    else
                    {
                        $bonus = \App\Transaction::where('bonus_for',$last_transaction->id)->first();
                        if($bonus)
                        {
                            $bonus_code = \App\Bonus::find($bonus->bonus_id);
                            return $bonus_code->bonus_code;
                        }
                        else
                        {
                            return '-';
                        }
                    }
                }
            })
            ->editColumn('referred_by', function ($transaction) {
                if($transaction->user->referred_by == null)
                {
                    return '-';
                }
                else
                {
                    $referred = \App\User::where('affiliate_id',$transaction->user->referred_by)->first();

                    if($referred){
                        return $referred->name;
                    }
                }
            })
            ->editColumn('game', function ($transaction) {
                
                $data = json_decode($transaction->data, true);
                $game = \App\Game::find($data['game_id']);

                if($game)
                {
                    $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();
                    if($game_id){
                        $username =  $game_id->username;
                    }
                    else
                    {
                        $username = "-";
                    }

                    return $game->name.' / '.$username;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('updated_by', function ($transaction) {
                if($transaction->logLatest->count() > 0)
                {
                    return $transaction->logLatest->first()->user->name;
                }
                else
                {
                    return '-';
                }
                
            })
            ->editColumn('status', function ($transaction) {
                
                if($transaction->status == 1)
                {
                    return '<span class="label label-warning">Progress</span>';
                }
                else if($transaction->status == 2)
                {
                    return '<span class="label label-success">Complete</span>';
                }
                else if($transaction->status == 3)
                {
                    return '<span class="label label-danger">Decline</span>';
                }


            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at ? with(new Carbon($transaction->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function transfer()
    {
        if(\Auth::user()->role == 1)
        {

            return view('admin.transaction.transfer');

        }
        else
        {
            return view('staff.transaction.transfer');
        }

    }

    public function transferData(Datatables $datatables, Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['from_date']);
        $arrEnd = explode("-", $input['to_date']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        if($input['status'] == '')
        {
            $transactions = Transaction::where('transaction_type','transfer')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->with('user')->with('logLatest');
        }
        else
        {
            $transactions = Transaction::where('transaction_type','transfer')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->where('status',$input['status'])->latest()->with('user')->with('logLatest');
        }

        return Datatables::of($transactions)
            ->editColumn('transaction_id', function ($transaction) {
                return '#T'.sprintf('%06d', $transaction->id);
            })
            ->addColumn('actions', function($transaction) {
                return view('admin.transaction.action', compact('transaction'))->render();
            })
            ->editColumn('user_id', function ($transaction) {
                return $transaction->user->username;
            })
            ->editColumn('from_game', function ($transaction) {
                
                $data = json_decode($transaction->data, true);
                $transfer_from = \App\Game::find($data['from_game']);
                

                if(!$transfer_from)
                {
                    return 'Game not selected';
                }
                else
                {
                    $from_game = \App\GameAccount::where('game_id',$transfer_from->id)->where('user_id',$transaction->user->id)->first();

                    if($from_game){
                        $username_from =  $from_game->username;
                    }
                    else
                    {
                        $username_from = "-";
                    }
                    return $transfer_from->name .' / '. $username_from;
                }


            })
            ->editColumn('to_game', function ($transaction) {
                
                $data = json_decode($transaction->data, true);
                $transfer_to = \App\Game::find($data['to_game']);
                

                if(!$transfer_to)
                {
                    return 'Game not selected';
                }
                else
                {
                    $to_game = \App\GameAccount::where('game_id',$transfer_to->id)->where('user_id',$transaction->user->id)->first();

                    if($to_game){
                        $username_to =  $to_game->username;
                    }
                    else
                    {
                        $username_to = "-";
                    }
                    return $transfer_to->name .' / '. $username_to;
                }

            })
            ->editColumn('status', function ($transaction) {
                
                if($transaction->status == 1)
                {
                    return '<span class="label label-warning">Progress</span>';
                }
                else if($transaction->status == 2)
                {
                    return '<span class="label label-success">Complete</span>';
                }
                else if($transaction->status == 3)
                {
                    return '<span class="label label-danger">Decline</span>';
                }


            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at ? with(new Carbon($transaction->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function birthday()
    {
        if(\Auth::user()->role == 1)
        {

            return view('admin.transaction.birthday');

        }
        else
        {
            return view('staff.transaction.birthday');
        }

    }

    public function birthdayData(Datatables $datatables,Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['from_date']);
        $arrEnd = explode("-", $input['to_date']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        if($input['status'] == '')
        {
            $transactions = Transaction::where('transaction_type','deposit')->where('deposit_type','birthday')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->latest()->with('user')->with('logLatest');
        }
        else
        {
            $transactions = Transaction::where('transaction_type','deposit')->where('deposit_type','birthday')->where('created_at','<=',$to_date)->where('created_at','>=',$from_date)->where('status',$input['status'])->latest()->with('user')->with('logLatest');
        }

        return Datatables::of($transactions)
            ->addColumn('actions', function($transaction) {
                return view('admin.transaction.action', compact('transaction'))->render();
            })
            ->editColumn('transaction_id', function ($transaction) {
                return '#BD'.sprintf('%06d', $transaction->id);
            })
            ->editColumn('user_id', function ($transaction) {
                return $transaction->user->username;
            })
            ->editColumn('email', function ($transaction) {
                return $transaction->user->email;
            })
            ->editColumn('group', function ($transaction) {
                return $transaction->user->group->name;
            })
            ->editColumn('game', function ($transaction) {

                $data = json_decode($transaction->data, true);
                $game = \App\Game::find($data['game_id']);


                if($game)
                {
                    $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();
                    if($game_id){
                        $username =  $game_id->username;
                    }
                    else
                    {
                        $username = "-";
                    }

                    return $game->name.' / '.$username;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('bonus_id', function ($transaction) {
                if($transaction->bonus_id == null)
                {
                    return 'No Bonus';
                }
                else
                {
                    return $transaction->bonus->name;
                }
            })
            ->editColumn('group', function ($transaction) {
                return $transaction->user->group->name;
            })
            ->editColumn('updated_by', function ($transaction) {
                if($transaction->logLatest->count() > 0)
                {
                    return $transaction->logLatest->first()->user->name;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('status', function ($transaction) {
                
                if($transaction->status == 1)
                {
                    return '<span class="label label-warning">Progress</span>';
                }
                else if($transaction->status == 2)
                {
                    return '<span class="label label-success">Complete</span>';
                }
                else if($transaction->status == 3)
                {
                    return '<span class="label label-danger">Decline</span>';
                }


            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at ? with(new Carbon($transaction->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {   
        $user = User::find(1);
        
        foreach ($user->unreadNotifications as $notification) {

            if($notification->type == 'App\Notifications\NewDeposit')
            {
                if($notification->data['transaction_id'] == $transaction->id)
                {
                    $notification->markAsRead();
                }
            }

            if($notification->type == 'App\Notifications\NewWithdraw')
            {
                if($notification->data['transaction_id'] == $transaction->id)
                {
                    $notification->markAsRead();
                }
            }

            if($notification->type == 'App\Notifications\NewTransfer')
            {
                if($notification->data['transaction_id'] == $transaction->id)
                {
                    $notification->markAsRead();
                }
            }
            
        }

        $logs = Log::where('transaction_id',$transaction->id)->latest('id')->get();
        $bonuses = Transaction::where('bonus_for',$transaction->id)->get();


        if(\Auth::user()->role == 1)
        {
            return view('admin.transaction.show',compact('transaction','logs','bonuses'));
        }
        else
        {
            return view('staff.transaction.show',compact('transaction','logs','bonuses'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        if($transaction->bonus_id == null)
        {
            $bonus = "No Bonus Applied";
        }
        else
        {
            $bonus = $transaction->bonus->name;
        }

        if(\Auth::user()->role == 1)
        {
            return view('admin.transaction.edit',compact('transaction','bonus'));
        }
        else
        {
            return view('staff.transaction.edit',compact('transaction','bonus'));
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $input = $request->all();

        $log = new Log;
        $log->user_id = Auth::user()->id;

        if($input['status'] == 1)
        {
            $status = '<span class="label label-warning">Progress</span>';
        }
        else if($input['status'] == 3)
        {
            $status = '<span class="label label-danger">Rejected</span>';
        }
        else if($input['status'] == 2)
        {
            $status = '<span class="label label-success">Success</span>';  
        }

        if($input['type_transaction'] == 'deposit')
        {
            // if($input['bonus_amount'] != '0')
            // {
            //     $transaction_bonus = new Transaction;

            //     $transaction_bonus->user_id = $transaction->user_id;
            //     $transaction_bonus->transaction_id = time();
            //     $transaction_bonus->transaction_type = 'deposit';
            //     $transaction_bonus->deposit_type = 'bonus';
            //     $transaction_bonus->data = $transaction->data;
            //     $transaction_bonus->amount = $input['bonus_amount'];
            //     $transaction_bonus->status = 2;
            //     $transaction_bonus->remarks = 'Bonus for deposit transaction [#'.sprintf('%06d', $transaction->id).']';

            //     $transaction_bonus->save();

            //     $log->detail = 'Add bonus for transaction [#'.sprintf('%06d', $transaction->id).']';
            // }

            // if(isset($input['bank']))
            // {
            //     $transaction->bank_id = $input['bank'];
            // }

            if($input['status'] == 2)
            {
                $user = User::find($transaction->user_id);
                $current_token = $user->token;
                $user->token = $current_token + 1;
                $user->save();
            }

            if($transaction->deposit_type == 'normal')
            {
                $transaction->bank_id = $input['bank'];   
            }

            $transaction->amount = $input['amount'];
            $transaction->status = $input['status'];
            $transaction->remarks = $input['remarks'];

            if($transaction->deposit_type == 'normal')
            {
                if($input['status'] == 2)
                {
                    $record = new BankRecord;
                    $record->user_id = Auth::user()->id;
                    $record->bank_id = $input['bank'];
                    $record->transaction_type = "Deposit";
                    $record->description = 'Deposit for transaction [#D'.sprintf('%06d', $transaction->id).']';
                    $record->record = 1;
                    $record->amount = $input['amount'];

                    $record->save();
                }

                if($input['status'] == 3)
                {
                    $record = BankRecord::where('description','LIKE','%'.sprintf('%06d', $transaction->id).'%')->first();
                    $record->delete();
                }
            }
            

        }
        else if($input['type_transaction'] == 'withdraw')
        {
            $transaction->amount = $input['amount'];
            $transaction->status = $input['status'];
            $transaction->bank_id = $input['bank'];
            $transaction->remarks = $input['remarks'];

            if($input['status'] == 2)
            {
                $record = BankRecord::where('description','LIKE','%'.sprintf('%06d', $transaction->id).'%')->first();
                if(is_null($record)){
                    $record = new BankRecord;
                    $record->user_id = Auth::user()->id;
                    $record->bank_id = $transaction->bank_id;
                    $record->transaction_type = "Withdraw";
                    $record->description = 'Withdrawal for transaction [#W'.sprintf('%06d', $transaction->id).']';
                    $record->record = 0;
                    $record->amount = $input['amount'];
                }

                $record->save();
            }

            if($input['status'] == 3)
            {
                $record = BankRecord::where('description','LIKE','%'.sprintf('%06d', $transaction->id).'%')->first();
                if(!is_null($record) ){

                    $record->delete();
                }
            }

        }

        else if($input['type_transaction'] == 'transfer')
        {
            $transaction->amount = $input['amount'];
            $transaction->status = $input['status'];
            $transaction->remarks = $input['remarks'];
        }

        else if($input['type_transaction'] == 'is_rebate')
        {
            


            $transaction->amount = $input['amount'];
            $transaction->status = $input['status'];
            $transaction->remarks = $input['remarks'];
            

        }

        $log->transaction_id = $transaction->id;
        $log->detail = 'SET transaction as '.$status;
        $log->save();

        $transaction->save();

        Session::flash('message', 'Transaction succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        if(\Auth::user()->role == 1)
        {

            return redirect('admin/transaction/'.$transaction->id);

        }
        else
        {
            return redirect('staff/transaction/'.$transaction->id);
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        Session::flash('message', 'Transaction succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/users/'.$transaction->user->id);

    }

    public function addbonus(Request $request)
    {
        $input = $request->all();

        $transaction = Transaction::find($input['transaction_id']);

        $transaction_bonus = new Transaction;

        $transaction_bonus->user_id = $transaction->user_id;
        $transaction_bonus->transaction_id = time();
        $transaction_bonus->transaction_type = 'deposit';
        $transaction_bonus->deposit_type = 'bonus';
        $transaction_bonus->data = $transaction->data;
        $transaction_bonus->amount = $input['bonus_amount'];
        $transaction_bonus->bonus_id = $transaction->bonus_id;
        $transaction_bonus->bonus_for = $transaction->id;
        $transaction_bonus->status = 2;
        $transaction_bonus->remarks = 'Bonus for deposit transaction [#'.sprintf('%06d', $transaction->id).']';

        $transaction_bonus->save();

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->transaction_id = $transaction->id;
        $log->detail = 'Add [SGD'.$input['bonus_amount'].'] bonus for transaction [#'.sprintf('%06d', $transaction->id).']';
        $log->save();



        Session::flash('message', 'Bonus Succesfully Added!'); 
        Session::flash('alert-class', 'alert-success');

        if(\Auth::user()->role == 1)
        {

            return redirect('admin/transaction/'.$transaction->id);

        }
        else
        {
            return redirect('staff/transaction/'.$transaction->id);
        }
    }

    public function deletebonus($transaction_id)
    {
        $transaction = Transaction::find($transaction_id);
        $transaction->delete();

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->transaction_id = $transaction->bonus_for;
        $log->detail = 'Delete Bonus For Transaction[#'.sprintf('%06d', $transaction->bonus_for).']';
        $log->save();

        Session::flash('message', 'Bonus Succesfully Deleted!'); 
        Session::flash('alert-class', 'alert-success');

        if(\Auth::user()->role == 1)
        {
            return redirect('admin/transaction/'.$transaction->bonus_for);
        }
        else
        {
            return redirect('staff/transaction/'.$transaction->bonus_for);
        }
    }

    public function approve(Transaction $transaction)
    {
        $transaction->status = 2;
        $transaction->save();


        if($transaction->transaction_type == 'deposit')
        {
            if($transaction->deposit_type == 'birthday')
            {
                $current_deposits = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('user_id', $transaction->user_id)->get();

                foreach($current_deposits as $current_deposit)
                {
                    $current_deposit->bday_mark = 1;
                    $current_deposit->save();
                }
            }
            else
            {
                if($transaction->status == 2)
                {
                    $user = User::find($transaction->user_id);
                    $current_token = $user->token;
                    $user->token = $current_token + 1;
                    $user->save();
                }

                if($transaction->status == 2)
                {
                    $record = new BankRecord;
                    $record->user_id = Auth::user()->id;
                    $record->bank_id = $transaction->bank_id;
                    $record->transaction_type = "Deposit";
                    $record->description = 'Deposit for transaction [#D'.sprintf('%06d', $transaction->id).']';
                    $record->record = 1;
                    $record->amount = $transaction->amount;

                    $record->save();
                }
            }

            
            

        }
        else if($transaction->transaction_type == 'withdraw')
        {

            if($transaction->status == 2)
            {
                $record = new BankRecord;
                $record->user_id = Auth::user()->id;
                $record->bank_id = $transaction->bank_id;
                $record->transaction_type = "Withdraw";
                $record->description = 'Withdrawal for transaction [#W'.sprintf('%06d', $transaction->id).']';
                $record->record = 0;
                $record->amount = $transaction->amount;

                $record->save();
            }

        }

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->transaction_id = $transaction->id;
        $log->detail = 'SET transaction as <span class="label label-success">Success</span>';
        $log->save();
        

        return redirect('admin/transaction/'.$transaction->id);
    }

    public function reject(Transaction $transaction)
    {
        $transaction->status = 3;
        $transaction->save();

        $log = new Log;
        $log->user_id = Auth::user()->id;
        $log->transaction_id = $transaction->id;
        $log->detail = 'SET transaction as <span class="label label-danger">Reject</span>';
        $log->save();
        
        return redirect('admin/transaction/'.$transaction->id);
    }
}
