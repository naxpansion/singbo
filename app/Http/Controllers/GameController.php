<?php

namespace App\Http\Controllers;

use App\Game;
use App\GameAccount;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Storage;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::all();

        if(\Auth::user()->role == 1)
        {

            return view('admin.games.index');

        }
        else
        {
            return view('staff.games.index');
        }
    }

    public function data(Datatables $datatables)
    {
        $games = Game::with('GameAccountsNotAssigned')->get();
        return Datatables::of($games)
            ->addColumn('actions', function($game) {
                return view('admin.games.action', compact('game'))->render();
            })
            ->editColumn('logo', function ($game) {
                return '<img width="100%" src="'.url('storage/games/'.$game->logo).'">';
            })
            ->editColumn('game_accounts', function ($game) {
                return $game->GameAccountsNotAssigned->count();
            })
            ->editColumn('created_at', function ($game) {
                return $game->created_at ? with(new Carbon($game->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->editColumn('status', function ($game) {
                if($game->status == 1)
                {
                    return '<span class=badge badge-success">Active</span>';
                }
                else
                {
                    return '<span class=badge badge-danger">Not Active</span>';
                }
            })
            ->rawColumns(['actions','logo','status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $path = $request->file('logo')->store('public/games');
        $logo_name = $request->file('logo')->hashName(); //file name

        $game = new Game;

        $game->name = $input['name'];
        $category_string = implode(",",$input['category']);
        $game->category = $category_string;
        $game->logo = $logo_name;

        $game->desktop_link = $input['desktop_link'];
        $game->android_link = $input['android_link'];
        $game->ios_link = $input['ios_link'];

        $game->save();

        Session::flash('message', 'New game succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/games');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        if(\Auth::user()->role == 1)
        {

            return view('admin.games.show',compact('game'));

        }
        else
        {
            return view('staff.games.show',compact('game'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        return view('admin.games.edit',compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        $input = $request->all();

        if ($request->hasFile('logo'))
        {
            $path = $request->file('logo')->store('public/games');
            $logo_name = $request->file('logo')->hashName();

            $game->logo = $logo_name;
        }

        $game->name = $input['name'];
        $category_string = implode(",",$input['category']);
        $game->category = $category_string;

        $game->desktop_link = $input['desktop_link'];
        $game->android_link = $input['android_link'];
        $game->ios_link = $input['ios_link'];

        $game->save();

        Session::flash('message', 'Game succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/games/'.$game->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        $game->delete();

        Session::flash('message', 'Game succesfully deleted!'); 
        Session::flash('alert-class', 'alert-danger');

        return redirect('admin/games');
    }

    public function createAccount(Game $game)
    {
        return view('admin.games.account',compact('game'));
    }

    public function storeAccount(Game $game,Request $request)
    {
        $account = New GameAccount;
        $account->game_id = $game->id;
        $account->username = $request->username;
        $account->password = $request->password;
        $account->save();

        Session::flash('message', 'New Account succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/games/'.$game->id);
    }

    public function accountData(Game $game, Datatables $datatables)
    {
        $account = GameAccount::where('game_id',$game->id)->get();

        return Datatables::of($account)
            ->addColumn('actions', function($account) {
                return '<a href="'.url('admin/gameaccount/'.$account->id).'" onclick="return confirm(\'Are you sure?\');" class="label label-danger">delete</a>';
            })
            ->editColumn('status', function ($account) {
                if($account->status == 0)
                {
                    return '<span class="label label-warning">Not Assigned</span>';
                }
                else
                {
                    return '<span class="label label-success">Assigned</span>';
                }
            })
            ->editColumn('created_at', function ($account) {
                return $account->created_at ? with(new Carbon($account->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function deleteAccount(GameAccount $gameAccount)
    {
        $game_id = $gameAccount->game->id;
        $gameAccount->delete();

        Session::flash('message', 'Game succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/games/'.$game_id);
    }

    public function import(Game $game)
    {

        return view('admin.games.import',compact('game'));
    }

    public function importStore(Game $game, Request $request)
    {

        $save = $request->file('file')->store('public/import');
        $file = Storage::disk('local')->path($save);
        

        // Reading file
            $file = fopen($file,"r");

            $datas = array();
            $i = 0;

            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata );
             
                 // Skip first row (Remove below comment if you want to skip the first row)
                 if($i == 0){
                    $i++;
                    continue; 
                 }
                 for ($c=0; $c < $num; $c++) {
                    $datas[$i][] = $filedata [$c];
                 }
                 $i++;
            }
            fclose($file);

            // dd($datas);
            foreach($datas as $data)
            {
                $account = New GameAccount;
                $account->game_id = $game->id;
                $account->username = $data[0];
                $account->password =$data[1];
                $account->save();
                
                
            }

        Session::flash('message', 'Successfuly Import Account'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/games/'.$game->id);
    }
}
