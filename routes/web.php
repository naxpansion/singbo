<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. 
|
*/
Route::any('/webhook', 'WebhookController@test');

Route::get('/sportsbooks', function(){ 
    return Redirect::to('/best-online-sportsbook-betting-singapore', 301); 
});

Route::get('/live_casinos', function(){ 
    return Redirect::to('/best-online-live-casino-singapore', 301); 
});

Route::get('/slots', function(){ 
    return Redirect::to('/best-online-slots-game-singapore', 301); 
});

Route::get('/promotions', function(){ 
    return Redirect::to('/online-casino-promotion-singapore', 301); 
});


Route::get('/', 'FrontController@index');
Route::get('/new', 'FrontController@new_index');
Route::get('/about_us', 'FrontController@about_us');
Route::get('/downloads', 'FrontController@downloads');
Route::get('/best-online-sportsbook-betting-singapore', 'FrontController@sportsbooks');
Route::get('/best-online-live-casino-singapore', 'FrontController@live_casinos');
Route::get('/best-online-slots-game-singapore', 'FrontController@slots');
Route::get('/arcades', 'FrontController@arcades');
Route::get('/game-live22', 'FrontController@game_live22');
Route::get('/lottery', 'FrontController@lottery');
Route::get('/online-casino-promotion-singapore', 'FrontController@promotions');
Route::get('/banking', 'FrontController@banking');
Route::get('/affiliates', 'FrontController@affiliate');
Route::get('/vip', 'FrontController@vip');
Route::get('/registration', 'FrontController@registration');
Route::get('/contact_us', 'FrontController@contact_us');
Route::get('/faq', 'FrontController@faq');
Route::get('/tnc', 'FrontController@tnc');
Route::get('/player/ban', 'PlayerController@banplayer');
Route::get('/sitemap', 'FrontController@sitemap');


// API START

Route::post('api/admin/notification', 'ApiController@adminNotofication');

Route::get('api/admin/depositTotal', 'ApiController@depositTotal');
Route::get('api/admin/withdrawTotal', 'ApiController@withdrawTotal');

Route::get('api/admin/depositTotalUser', 'ApiController@depositUserTotal');
Route::get('api/admin/withdrawTotalUser', 'ApiController@withdrawUserTotal');

Route::get('api/switch-desktop', 'ApiController@desktop');
Route::get('api/switch-mobile', 'ApiController@mobile');

Route::post('api/spin_ip', 'ApiController@spin');
Route::post('api/reward/guest', 'ApiController@guestReward');
Route::post('api/reward/auth', 'ApiController@authReward');

Route::post('password_reset', 'ApiController@password_reset');








// API END

Route::get('/player/verification', 'PlayerController@verification');
Route::post('/player/verification', 'PlayerController@verification_confirm');
Route::get('/aff', 'AffiliateController@viewregister');
Route::post('/aff', 'AffiliateController@register');
Auth::routes();

Route::get('/admin/login', 'DashboardController@login');

Route::middleware(['isadmin'])->group(function () {
    
    Route::get('/admin', 'DashboardController@index');

    Route::get('/admin/gameaccount/{gameAccount}', 'GameController@deleteAccount');
    Route::get('/admin/gameaccounts/{game_id}/delete/{user_id}', 'GameAccountController@destroy');
    

    Route::get('/admin/games/{game}/import', 'GameController@import');
    Route::post('/admin/games/{game}/import', 'GameController@importStore');
    Route::get('/admin/games/{game}/account-data', 'GameController@accountData');
    Route::get('/admin/games/{game}/new_account', 'GameController@createAccount');
    Route::post('/admin/games/{game}/new_account', 'GameController@storeAccount');
    Route::get('/admin/games/data', 'GameController@data');
    Route::resource('/admin/games', 'GameController');

    Route::get('/admin/gameaccounts/{account}/edit', 'GameAccountController@edit');
    Route::patch('/admin/gameaccounts/{account}', 'GameAccountController@update');

    Route::get('/admin/banks/data', 'BankController@data');
    Route::resource('/admin/banks', 'BankController');
    Route::get('/admin/banks/{bank_id}/data', 'BankRecordController@data');
    Route::post('/admin/banks/{bank_id}/debit', 'BankController@debit');
    Route::post('/admin/banks/{bank_id}/credit', 'BankController@credit');

    Route::get('/admin/bonuses/data-active', 'BonusController@dataActive');
    Route::get('/admin/bonuses/data-deactive', 'BonusController@dataDeactive');
    Route::resource('/admin/bonuses', 'BonusController');
    Route::get('/admin/bonuses/{bonus_id}/restore', 'BonusController@restore');

    Route::get('/admin/accounts', 'AccountController@index');
    Route::get('/admin/accounts-data', 'AccountController@data');
    Route::post('/admin/accounts/debit', 'AccountController@debit');
    Route::post('/admin/accounts/credit', 'AccountController@credit');
    Route::get('/admin/accounts/{account}/delete', 'AccountController@destroy');

    Route::delete('/admin/transaction/{transaction}/delete', 'TransactionController@destroy');

    Route::get('/admin/transaction/{transaction}/approve', 'TransactionController@approve');
    Route::get('/admin/transaction/{transaction}/reject', 'TransactionController@reject');

    Route::post('/admin/transaction/addbonus', 'TransactionController@addbonus');
    Route::get('/admin/transaction/{transaction_id}/deletebonus', 'TransactionController@deletebonus');
    Route::get('/admin/transaction/data', 'TransactionController@data');

    Route::get('/admin/transaction/deposit', 'TransactionController@deposit');
    Route::get('/admin/transaction/deposit-data', 'TransactionController@depositData');

    Route::get('/admin/transaction/withdrawal', 'TransactionController@withdrawal');
    Route::get('/admin/transaction/withdrawal-data', 'TransactionController@withdrawalData');

    Route::get('/admin/transaction/transfer', 'TransactionController@transfer');
    Route::get('/admin/transaction/transfer-data', 'TransactionController@transferData');

    Route::get('/admin/transaction/birthday', 'TransactionController@birthday');
    Route::get('/admin/transaction/birthday-data', 'TransactionController@birthdayData');


    Route::get('admin/rewards/spin/{id}/approve', 'SpinController@accept');
    Route::get('admin/rewards/spin/{id}/reject', 'SpinController@reject');
    Route::get('admin/rewards/spin', 'SpinController@index');
    Route::get('admin/rewards/spin-data', 'SpinController@data');
    Route::get('admin/rewards/spin/{id}', 'SpinController@show');
    Route::get('admin/rewards/spin/{id}/edit', 'SpinController@edit');
    Route::patch('admin/rewards/spin/{id}', 'SpinController@update');
    Route::delete('admin/rewards/spin/{id}', 'SpinController@destroy');

    Route::get('/admin/reports', 'ReportController@index');
    Route::get('/admin/reports/product', 'ReportController@product');

    Route::resource('/admin/transaction', 'TransactionController');

    Route::get('/admin/settings', 'settingController@index');
    Route::post('/admin/settings', 'settingController@update');

    Route::get('/admin/affiliate/data', 'UserController@dataAffiliateList');
    Route::get('/admin/affiliate', 'UserController@affiliate');

    Route::get('/admin/affiliate-request/data', 'UserController@dataAffiliateListRequest');
    Route::get('/admin/affiliate-request', 'UserController@affiliateRequest');
    Route::get('/admin/users/{user}/affiliate/approve', 'UserController@approveAff');
    Route::get('/admin/users/{user}/affiliate/reject', 'UserController@rejectAff');

    Route::get('/admin/list/data', 'UserController@dataAdminList');
    Route::get('/admin/list', 'UserController@list');
    Route::get('/admin/list/new', 'UserController@listnew');
    Route::post('/admin/list', 'UserController@liststore');
    Route::get('/admin/list/{user}', 'UserController@lishshow');
    Route::get('/admin/list/{user}/permission', 'UserController@permission');
    Route::post('/admin/list/{user}/permission', 'UserController@permissionStore');

    Route::get('/admin/users/forgot', 'UserController@forgotList');
    Route::get('/admin/users/forgot-data', 'UserController@forgotData');
    Route::get('/admin/users/forgot/{forgot}', 'UserController@forgotShow');

    Route::get('/admin/users/{user_id}/aff', 'UserController@showAff');
    Route::post('/admin/users/password', 'UserController@password');
    Route::get('/admin/users/export', 'UserController@export');
    Route::post('/admin/users/export', 'UserController@runExport');
    Route::get('/admin/users/data', 'UserController@data');
    Route::get('/admin/users/{user_id}/transaction-data', 'UserController@transactiondata');
    Route::get('/admin/users/{user_id}/logs-data', 'UserController@logsdata');
    Route::get('/admin/users/{user_id}/ban', 'UserController@ban');
    Route::get('/admin/users/{user_id}/unban', 'UserController@unban');
    Route::get('/admin/users/{user}/deactivate-affiliate', 'UserController@Affzero');
    Route::get('/admin/users/{user}/active-affiliate', 'UserController@Afftwo');
    Route::get('/admin/users/{user_id}/delete', 'UserController@destroy');
    Route::resource('/admin/users', 'UserController');

    Route::post('admin/user/transaction/deposit', 'UserController@deposit');
    Route::post('admin/user/transaction/withdraw', 'UserController@withdraw');
    Route::post('admin/user/transaction/transfer', 'UserController@transfer');
    Route::post('admin/user/transaction/rebate', 'UserController@rebate');

    Route::get('/admin/groups/data', 'GroupController@data');
    Route::resource('/admin/groups', 'GroupController');

    Route::get('/admin/annoucement', 'AnnoucementController@index');
    Route::get('/admin/annoucement/create', 'AnnoucementController@create');
    Route::post('/admin/annoucement', 'AnnoucementController@store');
    Route::get('/admin/annoucement/{annoucement_id}/delete', 'AnnoucementController@destroy');

    Route::get('/admin/backup', 'BackupController@index');
    Route::post('/admin/backup', 'BackupController@act');

    Route::get('/admin/customers/export', 'CustomerController@export')->name('customers.export');
    Route::get('/admin/customers/data', 'CustomerController@data')->name('customers.data');
    Route::resource('/admin/customers', 'CustomerController');

    Route::resource('/admin/bank_records', 'BankRecordController');

    Route::get('/admin/systemlogs-data', 'SystemLogController@data')->name('systemlogs.data');
    Route::resource('/admin/systemlogs', 'SystemLogController');

    Route::get('/admin/sports/{sport}/delete', 'SportController@destroy');
    Route::get('/admin/sports/data', 'SportController@data')->name('sports.data');
    Route::resource('/admin/sports', 'SportController');

    Route::get('/admin/slots/data', 'SlotController@data')->name('slots.data');
    Route::resource('/admin/slots', 'SlotController');

    Route::get('/admin/promotions/data', 'PromotionController@data')->name('promotions.data');
    Route::resource('/admin/promotions', 'PromotionController');

    Route::get('/admin/promotionscategory/data', 'PromotionCategoryController@data')->name('promotionscategory.data');
    Route::resource('/admin/promotionscategory', 'PromotionCategoryController');

     Route::get('/admin/wheel', 'WheelController@index');
     Route::post('/admin/wheel', 'WheelController@sync');
    
});

Route::middleware(['isstaff'])->group(function () {
    
    Route::get('/staff', 'DashboardController@index');

    Route::get('/staff/games/data', 'GameController@data');
    Route::resource('/staff/games', 'GameController');

    Route::get('/staff/banks/data', 'BankController@data');
    Route::resource('/staff/banks', 'BankController');
    Route::get('/staff/banks/{bank_id}/data', 'BankRecordController@data');
    Route::post('/staff/banks/{bank_id}/debit', 'BankController@debit');
    Route::post('/staff/banks/{bank_id}/credit', 'BankController@credit');

    Route::get('/staff/bonuses/data-active', 'BonusController@dataActive');
    Route::get('/staff/bonuses/data-deactive', 'BonusController@dataDeactive');
    Route::resource('/staff/bonuses', 'BonusController');
    Route::get('/staff/bonuses/{bonus_id}/restore', 'BonusController@restore');

    Route::post('/staff/transaction/addbonus', 'TransactionController@addbonus');
    Route::get('/staff/transaction/{transaction_id}/deletebonus', 'TransactionController@deletebonus');
    Route::get('/staff/transaction/data', 'TransactionController@data');

    Route::get('/staff/transaction/deposit', 'TransactionController@deposit');
    Route::get('/staff/transaction/deposit-data', 'TransactionController@depositData');

    Route::get('/staff/transaction/withdrawal', 'TransactionController@withdrawal');
    Route::get('/staff/transaction/withdrawal-data', 'TransactionController@withdrawalData');

    Route::get('/staff/transaction/transfer', 'TransactionController@transfer');
    Route::get('/staff/transaction/transfer-data', 'TransactionController@transferData');

    Route::get('/staff/reports', 'ReportController@index');

    Route::resource('/staff/transaction', 'TransactionController');

    Route::get('/staff/settings', 'settingController@index');
    Route::post('/staff/settings', 'settingController@update');

    Route::get('/staff/users/data', 'UserController@data');
    Route::get('/staff/users/{user_id}/transaction-data', 'UserController@transactiondata');
    Route::get('/staff/users/{user_id}/logs-data', 'UserController@logsdata');
    Route::get('/staff/users/{user_id}/ban', 'UserController@ban');
    Route::get('/staff/users/{user_id}/unban', 'UserController@unban');
    Route::resource('/staff/users', 'UserController');

    Route::post('staff/user/transaction/deposit', 'UserController@deposit');
    Route::post('staff/user/transaction/withdraw', 'UserController@withdraw');
    Route::post('staff/user/transaction/transfer', 'UserController@transfer');
    Route::post('staff/user/transaction/rebate', 'UserController@rebate');

    Route::get('/staff/groups/data', 'GroupController@data');
    Route::resource('/staff/groups', 'GroupController');
    
});

Route::middleware(['isplayer','phoneverification','isban'])->group(function () {
    
    Route::get('/player', 'PlayerController@account');

    Route::get('/player/request/{game}', 'GameAccountController@request');

    Route::post('/player/bank', 'PlayerController@bank');

    Route::get('/player/birthday', 'PlayerController@birthday');
    Route::post('/player/birthday', 'PlayerController@birthday_store');

    Route::get('/player/deposit/step1', 'PlayerController@deposit_step1');
    Route::get('/player/deposit/step2', 'PlayerController@deposit_step2');
    Route::get('/player/deposit/step3', 'PlayerController@deposit_step3');
    Route::post('/player/deposit', 'PlayerController@deposit_store');

    Route::get('/player/withdrawal/step1', 'PlayerController@withdrawal_step1');
    Route::get('/player/withdrawal/step2', 'PlayerController@withdrawal_step2');
    Route::post('/player/withdrawal/step3', 'PlayerController@withdrawal_store');

    Route::get('/player/transfer/step1', 'PlayerController@transfer_step1');
    Route::post('/player/transfer/step2', 'PlayerController@transfer_store');

    Route::get('/player/transaction', 'PlayerController@transaction');
    Route::get('/player/rewards', 'PlayerController@rewards');
    Route::get('/player/rewards/claim/{id}', 'PlayerController@claimrewards');
    Route::post('/player/rewards/claim/{id}', 'PlayerController@updaterewards');

    Route::get('/player/profile', 'PlayerController@profile');
    Route::post('/player/profile/update', 'PlayerController@profile_update');
    Route::post('/player/profile/password', 'PlayerController@password_update');

    Route::get('/player/affiliate', 'PlayerController@affiliate');
    Route::get('/player/affiliate/request', 'PlayerController@requestAffiliate');

    
});

Route::middleware(['isaffiliate'])->group(function () {

    Route::get('/affiliate', 'AffiliateController@dashboard');
    Route::get('/affiliate/reports', 'AffiliateController@reports');

});

Route::middleware(['ismaster'])->group(function () {

    Route::get('/master', 'MasterController@dashboard');
    Route::get('/master/agent/{user}', 'MasterController@agent');
    Route::get('/master/reports', 'MasterController@reports');

});