<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

@include ('front.header')

<body class="
@if(Request::is('new'))
body-new
@endif
">
    <!-- Google Tag Manager (noscript) -->

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KG3SGWL"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="topLanguage">
        <div class="container">
            <div class="social-menu">
                @if(session('view') !== null)
                <span><a href="{{ url('api/switch-mobile') }}" class="social-link" style=""></i> Switch To Mobile Site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                @endif
                <span><a href="https://www.facebook.com/M11-Sport-1680177768956015/" class="social-link"><i class="fab fa-facebook"></i></a></span>
                <span><a href="https://www.instagram.com/mb_steady/" class="social-link"><i class="fab fa-instagram"></i></a></span>
            </div>
            <div class="help-bar">
                <div class="languageWrap">
                    <div class="dropdown">
                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-language"> English <span class="caret" style=""></span> </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a id="ctl00_lnkbtnen" class="language-en" href="javascript:__doPostBack(&#39;ctl00$lnkbtnen&#39;,&#39;&#39;)">English</a></li>{{--
                            <li><a id="ctl00_lnkbtncn" class="language-cn" href="javascript:__doPostBack(&#39;ctl00$lnkbtncn&#39;,&#39;&#39;)">中文</a></li>
                            <li><a id="ctl00_lnkbtnbm" class="language-my" href="javascript:__doPostBack(&#39;ctl00$lnkbtnbm&#39;,&#39;&#39;)">Bahasa Melayu</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="header">
        <div class="headWrap container">
            <div class="logo">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('images/common/mbet_logo_2.png') }}" alt="" height="120px">
                </a>

                <img id="mini_spin" alt="Online casino lucky spin game" src="{{ url('wheel/mini_spin.png') }}" style="z-index: 10000; width: 15%;">
            </div>



            @if(Auth::guest())
            <div class="loginpart pull-right" id="">
                <form method="post" action="{{ route('login') }}">
                    @csrf
                    <div class="loginfield">
                        <input name="phone" type="number" class="txtbox" placeholder="Mobile No"/ value="{{ old('phone') }}" required>
                    </div>
                    <div class="loginfield">
                        <input name="password" type="password" class="txtbox" placeholder="Password" / required>
                    </div>
                    <div class="loginfield">
                        <button type="submit" class="btn-login">Login</button>
                    </div>
                    <div class="loginfield">
                        <a href="{{ url('/register') }}"><button type="button" class="btn-register">Register</button></a>
                    </div>
                </form>
                <div class="invalid">
                    @if ($errors->count() > 0)
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first() }}a</strong>
                        </span>
                    @endif
                </div>
                <div class="login-here" style="display: none;">
                        <span class="invalid-feedback">
                            <strong>Please login here</strong>
                        </span>
                </div>
            </div>
            <div class="forgotpw"><a href="{{ url('password/reset') }}">forgot password?</a></div>
            @else
            <div class="loginpart pull-right">
                <div class="welcome">Welcome {{ Auth::user()->name }}! <i style="color:{{ Auth::user()->group->color }}" class="fas fa-{{ Auth::user()->group->icon }}"></i><span id="ctl00_lblusername" style="xmargin-right: 10px;"></span></div>
                <div class="loginfield">
                    <input type="button" class="btn-register" value="My Account" onclick="location.href='{{ url('player') }}'" />
                </div>
                <div class="loginfield">
                    <input type="button" class="btn-login" value="Logout" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"/>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            @endif
            <div id="menu">
                <ul>
                    <li><a href="{{ url('/') }}" class="">Home</a></li>
                    <li><a href="{{ url('sportsbooks') }}" class="">sportsbooks</a></li>
                    <li><a href="{{ url('live_casinos') }}" class="">live casinos</a></li>
                    <li><a href="{{ url('slots') }}" class="">slots</a></li>
                    <li><a href="{{ url('arcades') }}" class="">arcades</a></li>
                    <li><a href="{{ url('lottery') }}" class="">lottery</a></li>
                    <li><a href="{{ url('promotions') }}" class="">promotions</a></li>
                    <li><a href="{{ url('contact_us') }}" class="">contact us</a></li>
                    <li class="comment"><a href="#" onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false"><i class="fas fa-fw fa-comment-dots"></i>live chat</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div id="ctl00_UpdatePanel1">


        @yield ('content')
    </div>

    @include('front.new_footer')
    @yield('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sticky-kit/1.1.3/sticky-kit.min.js"></script>
    <script type="text/javascript">
        // $("#mini_spin").stick_in_parent();
    </script>
    @php
        $announcement = \App\Annoucement::all()->count();
    @endphp
    @if($announcement == 0)

    @else
        <script type="text/javascript">
            $("#myModal").modal("show");
        </script>
    @endif
    <div id="spin-wheel" class="spin-wheel mfp-hide" style="background-image: url({{ url('images/common/Casino_rouletter_wheel.png')  }});">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <h4>Every visitor are awarded with 1 free spin</h4>
                <h4>Simply click on the 'start spin' to try your luck</h4>
                <h4>Good Luck!</h4>
            </div>
        </div>
        <div id="canvasContainer">
            <canvas id='spinner' width='500' height='500' data-responsiveMinWidth="180" data-responsiveScaleHeight="true" data-responsiveMargin="50" style="margin: auto; display: block; left: 0; right: 0;">
                Canvas not supported, use another browser.
            </canvas>
            <img id="prizePointer" src="{{ url('wheel/arrow.png') }}" alt="V" />
        </div>
        <br />
        <div class="row">
            <div class="col-md-8">
                <button id="spin_button" class="btn btn-block btn-lg btn-warning" onClick="startSpin();">Start Spin</button>
                <button id="reset_button" style="display: none;" class="btn btn-block btn-lg btn-warning" onClick="resetWheel(); return false;">Play Again</button>
            </div>
            <div class="col-md-4">
                <table class="table table-bordered" style="background-color: rgba(0,0,0,0.5);">
                    <tr>
                        <td style="vertical-align: middle;">TOKEN REMAINING</td>
                        @auth
                            <td style="text-align: center;"><h4 id="token">{{ \Auth::user()->token }}</h4></td>
                        @else
                            @php
                                $token_check = \App\SpinIp::where('ip',\Request::ip())->count();
                                if($token_check == 0)
                                {
                                    $token = 1;
                                }
                                else
                                {
                                    $token = 0;
                                }
                            @endphp
                            <td style="text-align: center;"><h4 id="token">{{ $token }}</h4></td>
                        @endauth
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">



        var spinPopup = $('#mini_spin').magnificPopup({
            items: {
                src: '#spin-wheel'
            },
            type: 'inline'
        }, 0);

        // let theWheel = new Winwheel({
        //     'canvasId'        : 'spinner',
        //     'drawMode'     : 'image',    // drawMode must be set to image.
        //     'numSegments'  : 8,          // The number of segments must be specified.
        //     'imageOverlay' : true,       // Set imageOverlay to true to display the overlay.
        //     'lineWidth'    : 1,          // Overlay uses wheel line width and stroke style so can set these
        //     'strokeStyle'  : 'red'       // as desired to change appearance of the overlay.
        // });

        let theWheel = new Winwheel({
            'canvasId'        : 'spinner',
            'drawMode'     : 'image',
            'responsive'      : true,
            'outerRadius'     : 212,        // Set outer radius so wheel fits inside the background.
            'innerRadius'     : 75,         // Make wheel hollow so segments dont go all way to center.
            'drawText'          : false,         // Need to set this true if want code-drawn text on image wheels.
            // 'textFontSize'      : 18,
            // 'textOrientation'   : 'curved',     // Note use of curved text.
            // 'textDirection'     : 'reversed',   // Set other text options as desired.
            // 'textAlignment'     : 'outer',
            // 'textMargin'        : 20,   // Align text to outside of wheel.
            'numSegments'     : 8,         // Specify number of segments.
            'segments'        :             // Define segments including colour and text.
            [                               // font size and text colour overridden on backrupt segments.
               {'fillStyle' : '#ee1c24', 'text' : 'RM 88'},
               {'fillStyle' : '#3cb878', 'text' : '30%'},
               {'fillStyle' : '#f6989d', 'text' : 'JACK POT'},
               {'fillStyle' : '#00aef0', 'text' : 'RM 111'},
               {'fillStyle' : '#f26522', 'text' : '30%'},
               {'fillStyle' : '#f6989d', 'text' : 'Try Again'},
               {'fillStyle' : '#e70697', 'text' : 'RM 4'},
               {'fillStyle' : '#fff200', 'text' : 'RM 8'},
            ],
            'animation' :           // Specify the animation to use.
            {
                'type'     : 'spinToStop',
                'duration' : 10,
                'spins'    : 3,
                'callbackFinished' : alertPrize,  // Function to call whent the spinning has stopped.
                'callbackSound'    : playSound,   // Called when the tick sound is to be played.
                'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound.
            },
            'pins' :                // Turn pins on.
            {
                'visible'    : false,
                'responsive' : true,
                'number'     : 24,
                'fillStyle'  : 'silver',
                'outerRadius': 4,
            }
        });

        // Create new image object in memory.
        let loadedImg = new Image();

        // Create callback to execute once the image has finished loading.
        loadedImg.onload = function()
        {
            theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
            theWheel.draw();                    // Also call draw function to render the wheel.
        }

        // Set the image source, once complete this will trigger the onLoad callback (above).
        loadedImg.src = "{{ secure_asset('wheel/wheel_all.png') }}";


        // Loads the tick audio sound in to an audio object.
            let audio = new Audio('{{ secure_asset('wheel/tick.mp3') }}');

            // This function is called when the sound is to be played.
            function playSound()
            {
                // Stop and rewind the sound if it already happens to be playing.
                audio.pause();
                audio.currentTime = 0;

                // Play the sound.
                audio.play();
            }

            // Vars used by the code in this page to do power controls.
            let wheelPower    = 0;
            let wheelSpinning = false;

            // -------------------------------------------------------
            // Click handler for spin button.
            // -------------------------------------------------------
            function startSpin()
            {
                var token = parseInt($("#token").text());

                if(token < 1)
                {
                    swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                        buttons: {
                            cancel: "Login",
                            catch: {
                                text: "Register",
                                value: "register",
                            },
                        },
                        icon: "warning",
                        title: "Whoops!!!!",
                    })
                    .then((value) => {
                        switch (value) {

                            case "register":
                                window.location.replace('{{ route('register') }}');
                                break;

                            default:
                                var magnificPopup = $.magnificPopup.instance;
                                magnificPopup.close();
                                $('input[name="email"]').focus();
                                $(".login-here").show();

                        }
                    });
                }
                else
                {
                    if (wheelSpinning == false)
                    {

                        var d = Math.random();
                        console.log(d);

                        if (d < 0.4)
                        {
                            var prize = 259; //Try again
                        }
                        else if (d < 0.41)
                        {
                            var prize = 359; //RM8
                        }
                        else if (d < 0.45)
                        {
                            var prize = 75; //30% Daily
                        }
                        else if (d < 0.46)
                        {
                            var prize = 120; //Jackpot
                        }
                        else if (d < 0.5)
                        {
                            var prize = 269; //30% Daily
                        }
                        else if (d < 1)
                        {
                            var prize = 170; //RM4
                        }
                        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                        // to rotate with the duration of the animation the quicker the wheel spins.
                        theWheel.animation.spins = 6;

                        // Disable the spin button so can't click again while wheel is spinning.
                        $("#spin_button").hide();

                        // Begin the spin animation by calling startAnimation on the wheel object.
                        theWheel.animation.stopAngle = prize;
                        theWheel.startAnimation();

                        // Set to true so that power can't be changed and spin button re-enabled during
                        // the current animation. The user will have to reset before spinning again.
                        wheelSpinning = true;

                        $.post("{{ url('api/spin_ip') }}", { 'ip':'{{ \Request::ip() }}'}, function(result,status){
                            console.log(result);
                        });

                        var new_token = token - 1;
                        $("#token").text(new_token);


                    }
                }


            }

            // -------------------------------------------------------
            // Function for reset button.
            // -------------------------------------------------------
            function resetWheel()
            {
                theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                theWheel.draw();                // Call draw to render changes to the wheel.

                wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
                $("#spin_button").show();
                $("#reset_button").hide();
            }

            // -------------------------------------------------------
            // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
            // -------------------------------------------------------
            function alertPrize(indicatedSegment)
            {
                // Just alert to the user what happened.
                // In a real project probably want to do something more interesting than this with the result.
                // alert("You have won " + indicatedSegment.endAngle);
                @auth
                    $.post("{{ url('api/reward/auth') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                            console.log(result);
                        });

                    if(indicatedSegment.endAngle == 45)
                    {
                        swal("To claim this reward please click button below.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "You Won RM88!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ url('player/rewards') }}');
                                    break;

                                default:

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 90)
                    {
                        swal("To claim this reward please make a deposit.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "You Won 30% For Next Deposit!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ url('player/deposit/step1') }}');
                                    break;

                                default:

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 135)
                    {
                        swal("To claim this reward please click button below.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "Jackpot!!!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ url('player/rewards') }}');
                                    break;

                                default:

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 180)
                    {
                        swal("To claim this reward please click button below.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "You Won RM4!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ url('player/rewards') }}');
                                    break;

                                default:

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 225)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "You Won 30% For Next Deposit!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 270)
                    {
                        swal("You can try again by making any deposit to us!", {
                            buttons: {
                                cancel: "Try Again",
                                catch: {
                                    text: "Get Token By Deposit",
                                    value: "register",
                                },
                            },
                            icon: "warning",
                            title: "Awww! We Are So Sorry :(",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ url('player/deposit/step1') }}');
                                    break;

                                default:

                            }
                        });
                    }

                    else if(indicatedSegment.endAngle == 360)
                    {
                        swal("To claim this reward please click button below.", {
                            buttons: {
                                catch: {
                                    text: "Claim",
                                    value: "claim",
                                },
                            },
                            icon: "success",
                            title: "You Won RM8!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "claim":
                                    window.location.replace('{{ url('player/rewards') }}');
                                    break;

                                default:

                            }
                        });
                    }
                @else

                    $.post("{{ url('api/reward/guest') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                            console.log(result);
                        });

                    if(indicatedSegment.endAngle == 45)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won RM88!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();
                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 90)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won 30% For Next Deposit!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 135)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "JACKPOT!!!!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 180)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won RM4!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 225)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won 30% For Next Deposit!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 270)
                    {
                        swal("You can try again by making any deposit to us!", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "warning",
                            title: "Awww! We Are So Sorry :(",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 315)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won RM4!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                    else if(indicatedSegment.endAngle == 360)
                    {
                        swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "success",
                            title: "You Won RM8!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ route('register') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    $('input[name="email"]').focus();
                                    $(".login-here").show();

                            }
                        });
                    }
                @endauth
                $("#reset_button").show();
            }
    </script>

<!-- Start of LiveChat (www.livechatinc.com) code
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 9644895;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <script type="text/javascript">
        $("#stickywell").sticky({topSpacing:0});
    </script>
     End of LiveChat code -->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
<script type="text/javascript">
    $(".phonemask").mask('601000000000');
</script>
<script type="text/javascript">
    setInterval(function(){ $("#mini_spin").effect( "shake", { direction: "up", times: 4, distance: 10}, 5000 ); }, 1000);

    window.__lc = window.__lc || {};
    window.__lc.license = 10319712;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <noscript>
    <a href="https://www.livechatinc.com/chat-with/10319712/">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
    </noscript>
    <!-- End of LiveChat code -->
    <script type="text/javascript">
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/82e2eba1f1d8b74281f74203195cf20c.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
    })(window,document);
  </script>
    </body>
</html>
