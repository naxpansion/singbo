@extends ('front.master')
@section('content')


<div style="display:none;">
    Banking
</div>
<div id="ctl00_cphBody_bg" class="banking">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>BANKING</p>
    </div>
    <center>
        <!--<div id="bank2">
            <img src="/images/cimbbank.jpg" />
            <img src="/images/maybank.jpg" />
            <img src="/images/public.jpg" />
            <img src="/images/hongleong.jpg" />
            </div>-->
        <div id="b-type">
            <h4 style="text-align: left;">DEPOSIT</h4>
            <div id="b-deposit" class="b-method">
                <table>
                    <tr>
                        <th style="width: 40%;">Banking Methods</th>
                        <th>Minimum</th>
                        <th>Maximum</th>
                        <th style="width: 23%;">Processing Time</th>
                    </tr>
                    <tr>
                        <td>Online transfer &amp; ATM &amp; CDM</td>
                        <td>SGD 30</td>
                        <td>SGD 100,000</td>
                        <td>3-10 minutes</td>
                    </tr>
                </table>
            </div>
            <h4 style="text-align: left;">WITHDRAW</h4>
            <div id="b-deposit" class="b-method">
                <table>
                    <tr>
                        <th>Banking Methods</th>
                        <th>Minimum</th>
                        <th>Maximum</th>
                        <th>Processing Time</th>
                    </tr>
                    <tr>
                        <td>Online transfer & ATM & CDM</td>
                        <td>SGD 50</td>
                        <td>SGD 10,000</td>
                        <td>15 minutes</td>
                    </tr>
                    <tr>
                        <td>Online transfer & ATM & CDM</td>
                        <td colspan="2">SGD 10,001&emsp;&emsp;to&emsp;&emsp;SGD 100,000</td>
                        <td>12 hours</td>
                    </tr>
                </table>
            </div>
            
            <div id="b-content" class="b-method">
                <ul>
                    <li>Withdrawals will only be approve and release into the member’s bank account bearing the same name used to register at SINGBET9.</li>
                    <li>Large withdrawal amount will take longer processing time.</li>
                    <li>Member is required to meet at least one (1) time turnover of the deposited amount before withdrawal can be made.</li>
                    <li>Withdrawal is not allowed if the rollover requirements for any bonus claimed have not been fulfilled.</li>
                    <li>Please retain the Bank Receipt (if fund deposited via ATM) or Transaction Reference Number (if fund deposited via internet banking) as we will request for the proof of deposit.</li>
                    <li>All deposits and withdrawals processing time are subject to internet banking availability.</li>
                    <li>For other Local Banks, please liaise with our Customer Service via live chat for more info.</li>
                    <li>Please refer to the terms and condition for more details.</li>
                    <li>Receipt will be forfeit after 24 hours.</li>
                </ul>
            </div>
        </div>
    </center>
</div>
<div class="PReg">
    <p>Not our member yet?</p>
    <a href="{{ route('register') }}" class="btn">Register Now</a>
</div>

    
@endsection
