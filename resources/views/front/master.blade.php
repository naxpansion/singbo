<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143689897- 1"></script>
<script>
window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date());
gtag('config', 'UA-143689897-1'); </script>

<!--Verification Tag for Google Webmaster-->
<meta name="google-site-verification" content="IgKDjWFvewgu0PwyI-vDJ- tzuM1zELyn_b7A20nxo6I" />

        <meta charset="UTF-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=1024,User-scalable=no" />
        <meta property="og:image" content="https://singbet9.com/sharer-image.png"/>
        @yield('metas')
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
         <link rel="stylesheet" href="{{ url('css/constant.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ url('css/style.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ url('css/mod.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ secure_asset('css/custom.css') }}" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700italic,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Marcellus" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="{{ url('lib/js/jquery-1.12.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('lib/js/jquery.form.min.js') }}"></script>
        <!--<link rel="icon" type="image/png" href="{{ url('images/icon_tb9.png') }}" />-->
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('favicon.ico') }}" />
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato" />
        <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" />


        <!-- ************************** LOGIN ************************** -->
        <script type="text/javascript" src="{{ url('lib/js/checkFields.js') }}"></script>
        <script type="text/javascript">
            setInterval(function(){ $("#mini_spin").effect( "shake", { direction: "up", times: 4, distance: 10}, 5000 ); }, 1000);
            function doSubmit() {
                var txtUsername = document.getElementById("ctl00_txtUsername");
                var txtPass = document.getElementById("ctl00_txtPass");

                if (txtUsername.value == "") { alert("Please key in Username."); txtUsername.focus(); return false; }
                else if (txtPass.value == "") { alert("Please key in Password."); txtPass.focus(); return false; }
                else { return true; }
            }
        </script>
        <!-- ************************** LOGIN ************************** -->
        <!-- ************************** SCRIPT: TICKER ************************** -->
        <script type="text/javascript" src="{{ url('lib/js/jquery.marquee/jquery.marquee.min.js') }}"></script>
        <script src="{{ secure_asset('plugins/winwheel.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- ************************** SCRIPT: TICKER ************************** -->
        <!-- *************** NIVO SLIDER *************** -->
        <link rel="stylesheet" href="{{ url('lib/js/nivo/themes/default/default.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ url('lib/js/nivo/nivo-slider.css') }}" type="text/css" media="screen" />
        <!-- *************** NIVO SLIDER *************** -->
    </head>
    <body style="-webkit-overflow-scrolling: touch;">
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KHCM3JV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

            <div id="forheader">
                <div id="header">
                    <div id="logo">
                        <a href="{{ url('/') }}"><img src="{{ url('images/logo.png') }}" class="m_logo" alt="Best online casino Singapore singbet9" /></a>
                    </div>
                    <img id="mini_spin" alt="Online casino lucky spin game" src="{{ url('wheel/mini_spin.png') }}" style="z-index: 5; width: 5%; position: absolute; top: 20px;">
                    <div style="float: right;">
                        <div class="headerform">
                            @auth
                                <a href="{{ url('player') }}" class="btn">My DASHBOARD</a>
                                <input type="button" class="btn" value="LOGOUT" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"/>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @else
                            <form method="post" action="{{ route('login') }}">
                                @csrf
                                <input name="username" type="text" id="phone" tabindex="1" placeholder="Username" />
                                <input name="password" type="password" maxlength="43" id="password" tabindex="2" placeholder="Password" />
                                <button class="btn" type="submit">LOGIN</button>
                                <a href="{{ route('register') }}" class="btn" >REGISTER</a>
                                @if ($errors->count() > 0)
                                    <br />
                                    <span class="errors" style="color: red; margin-left: 10px;">
                                        <strong>{{ $errors->first() }}</strong>
                                    </span>
                                @endif
                            </form>
                            @endauth
                            @auth

                            @else
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('password.request')}}">Forget Password</a>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav">
                <div class="nav-container">
                    <ul>
                        <li>
                            <a href="{{ url('/') }}">
                            <div class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_home.png') }}" alt="Best online casino singbet9 home" />
                                </div>
                                <div class="nav-items-text">
                                    <p>HOME</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/best-online-sportsbook-betting-singapore') }}">
                            <div class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_sport.png') }}" alt="Best sportsbook betting singapore" />
                                </div>
                                <div class="nav-items-text">
                                    <p>SPORTSBOOK</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/best-online-live-casino-singapore') }}">
                            <div class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_live.png') }}" alt="Trusted live online casino singapore" />
                                </div>
                                <div class="nav-items-text">
                                    <p>LIVE CASINO</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/best-online-slots-game-singapore') }}">
                            <div  class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_slot.png') }}" alt="Online slots games" />
                                </div>
                                <div class="nav-items-text">
                                    <p>SLOT GAME</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/online-casino-promotion-singapore') }}">
                            <div  class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_promo.png') }}" alt="Best bonus online casino" />
                                </div>
                                <div class="nav-items-text">
                                    <p>PROMOTIONS</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/vip') }}">
                            <div class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_vip.png') }}" alt="Vip online casino singapore" />
                                </div>
                                <div class="nav-items-text">
                                    <p>VIP CLUB</p>
                                </div>
                            </div>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url('/affiliates') }}">
                            <div  class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('aff-icon.png') }}" alt="Online casino affilate" />
                                </div>
                                <div class="nav-items-text">
                                    <p>AFFILIATE</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/banking') }}">
                            <div  class="nav-items">
                                <div class="nav-items-img">
                                    <img src="{{ url('images/icon_bank.png') }}" alt="Online casino Singapore bank" />
                                </div>
                                <div class="nav-items-text">
                                    <p>BANKING</p>
                                </div>
                            </div>
                            </a>
                        </li>
                        @php
                            $agent = new \Jenssegers\Agent\Agent;
                        @endphp

                    </ul>
                </div>
            </div>
            @yield('content')
            <div class="footer">
                <div class="footer-banner">
                    <h5>POWERED BY</h5>
                    <div class="footer-product-logo">
                        <script type="text/javascript" src="{{ url('lib/js/slide_prod.js') }}"></script>
                    </div>
                    <br /><br />
                    <h5>BANKS</h5>
                    <br />
                    <div class="footer-product-logo">
                        <div class="row"  style="text-align: center;">

                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <img alt="Singapore casino banks" src="{{ secure_asset('images/banks/all.png') }}">
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-12">
                            @yield('footer_title')
                        </div>
                    </div>
                     <div class="row" style="color: white; font-size: 10px;">
                       @yield('footer_section_content')
                    </div>

                </div>
                <div id="footer">
                    <div id="footer_content" style="display:none">

                    </div>
                    <div id="menu2" class="row">
                        <div id="copyright" class="col-md-12" style="text-align: center;">
                            <a><img style="padding:10px" src="{{ url('images/facebook.png') }}"></a>
                            <a><img style="padding:10px" src="{{ url('images/instagram.png') }}"></a>
                            <a><img style="padding:10px" src="{{ url('images/twitter.png') }}"></a>
                            <a><img style="padding:10px" src="{{ url('images/youtube.png') }}"></a>
                            <p>Copyright© 2018 SINGBET9 | All Rights Reserved. <a style="color:#ffc019;text-decoration:none" href="{{ url('/sitemap') }}">Sitemap</a></p>
                            @if($agent->isMobile())
                                <a href="{{ url('/api/switch-mobile') }}" style="color: gold;">Switch To Mobile Version </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- *************************************  MAGNIFIC POPUP ************************************* -->
            <a href="#mfp-reg" class="open-popup-link" id="mfpReg"></a>
            <div id="mfp-reg" class="black-popup2 mfpReg mfp-hide"></div>
            <a href="#mfp-fp" class="open-popup-link" id="mfpfp"></a>
            <div id="mfp-fp" class="black-popup2 mfpfp mfp-hide"></div>
            <div id="test-popup0" class="black-popup mfp-hide">
                <div id="mfpdiv" class="mfp-indiv">
                    <div style="padding: 20px; color: #ffffff;">Welcome bonus up to 150%</div>
                </div>
            </div>
            <div id="spin-wheel" class="spin-wheel mfp-hide" style="background-image: url({{ url('wheel/bg.jpeg')  }});">
                <div class="row">
                    {{-- <div class="col-md-12" style="text-align: center; color: white;">
                        <h4>Every visitor are awarded with 1 free spin</h4>
                        <h4>Simply click on the 'start spin' to try your luck</h4>
                        <h4>Good Luck!</h4>
                    </div> --}}
                </div>
                <div id="canvasContainer">
                    <canvas id='spinner' width='500' height='500' data-responsiveMinWidth="180" data-responsiveScaleHeight="true" data-responsiveMargin="50" style="margin: auto; display: block; left: 0; right: 0;">
                        Canvas not supported, use another browser.
                    </canvas>
                    <img id="prizePointer" src="{{ url('wheel/arrow.png') }}" alt="V" />
                </div>
                <br />
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <button id="spin_button" class="btn btn-block btn-lg btn-warning btn-spin" onClick="startSpin();">Start Spin</button>
                        <button id="reset_button" style="display: none;" class="btn btn-block btn-lg btn-warning" onClick="resetWheel(); return false;">Play Again</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <br />
                        <table class="table table-bordered table-condensed" style="background-color: rgba(0,0,0,0.5); color: white; border-collapse: collapse;
    border-radius: 10px;
    border-style: hidden; /* hide standard table (collapsed) border */
    box-shadow: 0 0 0 2px #fff; /* this draws the table border  */ ">
                            <tr>
                                <td style="vertical-align: middle;">TOKEN REMAINING</td>
                                @auth
                                    <td style="text-align: center;"><h4 id="token">{{ \Auth::user()->token }}</h4></td>
                                @else
                                    @php
                                        $token_check = \App\SpinIp::where('ip',\Request::ip())->count();
                                        if($token_check == 0)
                                        {
                                            $token = 0;
                                        }
                                        else
                                        {
                                            $token = 0;
                                        }
                                    @endphp
                                    <td style="text-align: center;"><h4 id="token" style="color: white;">{{ $token }}</h4></td>
                                @endauth
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
            @yield('popup')
            <script type="text/javascript">
                function doFP() {

                    $.get("/m/fp.aspx", function (respondHtml) {
                        if (respondHtml) { $('#mfp-fp').html(respondHtml); $('#mfpfp').trigger('click'); }
                    })
                    //$('#mfp-fp').html("Hello <b>world</b>!"); $('#mfpfp').trigger('click');

                }
                function doReg() {

                    $.get("/r/index.aspx").done(function (respondHtml) {
                        if (respondHtml) { $('#mfp-reg').html(respondHtml); $('#mfpReg').trigger('click'); }
                    })
                    //$('#mfp-reg').html("Hello <b>world</b>!"); $('#mfpReg').trigger('click');

                }
                $(window).load(function () {
                    fillup2();
                })

                // -- ******************  MAGNIFIC POPUP ****************** -- //
                $(window).load(function () {
                    $("#pop-btnFp, #pop-btnReg, #pop-btnJoin, #ctl00_btnSubmit").removeAttr("disabled");
                });
                $(document).ready(function () {
                    // --  MFP -- //
                    $('.open-popup-link').magnificPopup({
                        type: 'inline',
                        midClick: true,
                        callbacks: {
                            close: function () {
                                var elemID = this.currItem.src;


                                if (elemID == "#mfp-post-login") {

                                }
                                else if ((elemID == "#mfp-txn") || (elemID == "#mfp-toreb") || (elemID == "#mfp-agrep") || (elemID == "#mfp-agdl") || (elemID == "#mfp-ann")) {
                                    // -- DO NOTHING -- //
                                }
                                else {
                                    location.reload();
                                }
                            }
                        }
                    });
                });

                // -- ****************** JQUERY.MARQUEE ****************** -- //
                $('.marquee').marquee({
                    //speed in milliseconds of the marquee
                    duration: 15000,
                    //gap in pixels between the tickers
                    gap: 50,
                    //time in milliseconds before the marquee will start animating
                    delayBeforeStart: 0,
                    //'left' or 'right'
                    direction: 'left',
                    //true or false - should the marquee be duplicated to show an effect of continues flow
                    duplicated: true,
                    pauseOnHover: true
                });
            </script>
            <!-- *************************************  MAGNIFIC POPUP ************************************* -->
            <link rel="stylesheet" href="{{ secure_asset('lib/js/mfp/mfp.css') }}">
            <script src="{{ secure_asset('lib/js/mfp/mfp.js') }}"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>
            <!-- *************************************  MAGNIFIC POPUP ************************************* -->
            <!--Start of Zopim Live Chat Script-->
            <!--End of Zopim Live Chat Script-->
            <script type="text/javascript" src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
            <script type="text/javascript" src="{{ secure_asset('lib/js/nivo/jquery.nivo.slider.js') }}"></script>
            <script type="text/javascript">
                //document.getElementById("bank_container").style.display = "none";
                $(window).load(function () {
                    $('.homeslider').nivoSlider({ controlNav: false, effect: 'fade' });
                    //Hide Nivo Left Right Nav if only one image
                    var count_nivoimg = $(".nivoSlider>img").length //count children with img tag
                    if (count_nivoimg <= 2) {
                        $(".nivoSlider .nivo-directionNav").hide();
                    }
                })
            </script>

            <script type="text/javascript">

                window.addEventListener('DOMContentLoaded', function(){
                    if (window.location.href.indexOf('/register')>0) {
                        //Hide the element.
                        $('.footer-banner').hide();
                    }
                });
                var spinPopup = $('#mini_spin').magnificPopup({
                    items: {
                        src: '#spin-wheel'
                    },
                    type: 'inline'
                }, 0);

                // let theWheel = new Winwheel({
                //     'canvasId'        : 'spinner',
                //     'drawMode'     : 'image',    // drawMode must be set to image.
                //     'numSegments'  : 8,          // The number of segments must be specified.
                //     'imageOverlay' : true,       // Set imageOverlay to true to display the overlay.
                //     'lineWidth'    : 1,          // Overlay uses wheel line width and stroke style so can set these
                //     'strokeStyle'  : 'red'       // as desired to change appearance of the overlay.
                // });

                let theWheel = new Winwheel({
                    'canvasId'        : 'spinner',
                    'drawMode'     : 'image',
                    'responsive'      : true,
                    'outerRadius'     : 212,        // Set outer radius so wheel fits inside the background.
                    'innerRadius'     : 75,         // Make wheel hollow so segments dont go all way to center.
                    'drawText'          : false,         // Need to set this true if want code-drawn text on image wheels.
                    // 'textFontSize'      : 18,
                    // 'textOrientation'   : 'curved',     // Note use of curved text.
                    // 'textDirection'     : 'reversed',   // Set other text options as desired.
                    // 'textAlignment'     : 'outer',
                    // 'textMargin'        : 20,   // Align text to outside of wheel.
                    'numSegments'     : 8,         // Specify number of segments.
                    'segments'        :             // Define segments including colour and text.
                    [                               // font size and text colour overridden on backrupt segments.
                       @foreach(\App\Wheel::all() as $wheel)
                            {'fillStyle' : '#ee1c24', 'text' : '{{ $wheel->name }}'},
                       @endforeach
                    ],
                    'animation' :           // Specify the animation to use.
                    {
                        'type'     : 'spinToStop',
                        'duration' : 10,
                        'spins'    : 3,
                        'callbackFinished' : alertPrize,  // Function to call whent the spinning has stopped.
                        'callbackSound'    : playSound,   // Called when the tick sound is to be played.
                        'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound.
                    },
                    'pins' :                // Turn pins on.
                    {
                        'visible'    : false,
                        'responsive' : true,
                        'number'     : 24,
                        'fillStyle'  : 'silver',
                        'outerRadius': 4,
                    }
                });

                // Create new image object in memory.
                let loadedImg = new Image();

                // Create callback to execute once the image has finished loading.
                loadedImg.onload = function()
                {
                    theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
                    theWheel.draw();                    // Also call draw function to render the wheel.
                }

                // Set the image source, once complete this will trigger the onLoad callback (above).
                loadedImg.src = "{{ secure_asset('wheel/wheel_all.png') }}";


                // Loads the tick audio sound in to an audio object.
                    let audio = new Audio('{{ secure_asset('wheel/tick.mp3') }}');

                    // This function is called when the sound is to be played.
                    function playSound()
                    {
                        // Stop and rewind the sound if it already happens to be playing.
                        audio.pause();
                        audio.currentTime = 0;

                        // Play the sound.
                        audio.play();
                    }

                    // Vars used by the code in this page to do power controls.
                    let wheelPower    = 0;
                    let wheelSpinning = false;

                    // -------------------------------------------------------
                    // Click handler for spin button.
                    // -------------------------------------------------------
                    function startSpin()
                    {
                        var token = parseInt($("#token").text());

                        if(token < 1)
                        {
                            @auth

                            swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                                buttons: {
                                    cancel: "Deposit",
                                },
                                icon: "warning",
                                title: "Whoops!!!!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ route('register') }}');
                                        break;

                                    default:
                                        window.location.replace('{{ url('player/deposit/step1') }}');

                                }
                            });

                            @else

                            swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "warning",
                                title: "Whoops!!!!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ route('register') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        $('input[name="email"]').focus();
                                        $(".login-here").show();

                                }
                            });

                            @endauth

                        }
                        else
                        {
                            if (wheelSpinning == false)
                            {

                                var d = Math.random();
                                console.log(d);

                                var section_1 = {{ \App\Wheel::find(1)->percentage }} * 0.01;
                                var section_2 = {{ \App\Wheel::find(2)->percentage }} * 0.01;
                                var section_3 = {{ \App\Wheel::find(3)->percentage }} * 0.01;
                                var section_4 = {{ \App\Wheel::find(4)->percentage }} * 0.01;
                                var section_5 = {{ \App\Wheel::find(5)->percentage }} * 0.01;
                                var section_6 = {{ \App\Wheel::find(6)->percentage }} * 0.01;
                                var section_7 = {{ \App\Wheel::find(7)->percentage }} * 0.01;
                                var section_8 = {{ \App\Wheel::find(8)->percentage }} * 0.01;

                                if (d < section_1)
                                {
                                    var prize = 23; //Try again
                                }
                                else if (d < section_1 + section_2)
                                {
                                    var prize = 70; //RM8
                                }
                                else if (d < section_1 + section_2 + section_3)
                                {
                                    var prize = 121; //30% Daily
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4)
                                {
                                    var prize = 160; //Jackpot
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5)
                                {
                                    var prize = 200; //30% Daily
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6)
                                {
                                    var prize = 260; //RM4
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7)
                                {
                                    var prize = 290; //RM4
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7 + section_8)
                                {
                                    var prize = 340; //RM4
                                }
                                // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                                // to rotate with the duration of the animation the quicker the wheel spins.
                                theWheel.animation.spins = 6;

                                // Disable the spin button so can't click again while wheel is spinning.
                                $("#spin_button").hide();

                                // Begin the spin animation by calling startAnimation on the wheel object.
                                theWheel.animation.stopAngle = prize;
                                theWheel.startAnimation();

                                // Set to true so that power can't be changed and spin button re-enabled during
                                // the current animation. The user will have to reset before spinning again.
                                wheelSpinning = true;

                                $.post("{{ url('api/spin_ip') }}", { 'ip':'{{ \Request::ip() }}'}, function(result,status){
                                    console.log(result);
                                });

                                var new_token = token - 1;
                                $("#token").text(new_token);


                            }
                        }


                    }

                    // -------------------------------------------------------
                    // Function for reset button.
                    // -------------------------------------------------------
                    function resetWheel()
                    {
                        theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                        theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                        theWheel.draw();                // Call draw to render changes to the wheel.

                        wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
                        $("#spin_button").show();
                        $("#reset_button").hide();
                    }

                    // -------------------------------------------------------
                    // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
                    // -------------------------------------------------------
                    function alertPrize(indicatedSegment)
                    {
                        // Just alert to the user what happened.
                        // In a real project probably want to do something more interesting than this with the result.
                        // alert("You have won " + indicatedSegment.endAngle);
                        @auth
                            $.post("{{ url('api/reward/auth') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                    console.log(result);
                                });

                            if(indicatedSegment.endAngle == 45)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(1)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 90)
                            {
                                swal("To claim this reward please make a deposit.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(2)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/deposit/step1') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 135)
                            {
                                swal("You can try again by making any deposit to us!", {
                                    buttons: {
                                        cancel: "Try Again",
                                        catch: {
                                            text: "Get Token By Deposit",
                                            value: "register",
                                        },
                                    },
                                    icon: "warning",
                                    title: "Awww! We Are So Sorry :(",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ url('player/deposit/step1') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 180)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(4)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 225)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(5)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 270)
                            {

                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(6)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:

                                    }
                                });
                            }

                            else if(indicatedSegment.endAngle == 315)
                            {

                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(7)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:

                                    }
                                });
                            }

                            else if(indicatedSegment.endAngle == 360)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(8)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:

                                    }
                                });
                            }
                        @else

                            $.post("{{ url('api/reward/guest') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                    console.log(result);
                                });

                            if(indicatedSegment.endAngle == 45)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(1)->name }}!!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 90)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(2)->name }}",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 135)
                            {
                                swal("You can try again by making any deposit to us!", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "warning",
                                    title: "Awww! We Are So Sorry :(",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 180)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(4)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 225)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(5)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 270)
                            {

                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(6)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 315)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(7)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 360)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(8)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance;
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();

                                    }
                                });
                            }
                        @endauth
                        $("#reset_button").show();
                    }

    $('.slider-sport').bxSlider({
        auto: false,
        speed: 1000,
        pause: 7000,
        responsive: false,
    });
            </script>
            <!-- Start of LiveChat (www.livechatinc.com) code -->
           <script type="text/javascript">
             window.__lc = window.__lc || {};
             window.__lc.license = 10962212;
             (function() {
               var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
               lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
               var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
             })();
           </script>
           <noscript>
           <a href="https://www.livechatinc.com/chat-with/10962212/" rel="nofollow">Chat with us</a>,
           powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
           </noscript>
           <!-- End of LiveChat code -->
                <script type="text/javascript" src="{{ secure_asset('js/map.js') }}"></script>
                <script type="text/javascript">
                    $(document).ready(function(e) {
                        $('img[usemap]').rwdImageMaps();
                    });
                </script>
            @yield('script')

        </form>
    </body>
</html>
<!-- Localized -->
