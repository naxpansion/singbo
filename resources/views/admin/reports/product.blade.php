@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	<div class="row">
        		<div class="col-md-12">
        			<h3>Products Total W/L</h3>
        		</div>
        	</div>
        	<hr />
        	<form method="get">
        	<div class="row">
        		@php

        			if(isset($input['date_from']))
        			{
        				$date_from= $input['date_from'];
        				$date_to = $input['date_to'];
        			}
        			else
        			{
        				$date_from = '';
        				$date_to = '';
        			}

        		@endphp
        		<div class="col-md-6">
        			<div class="form-group">
						<label>From</label>
						<input data-toggle="datepicker" type="text" name="date_from" class="form-control" value="{{ $date_from }}" required>
					</div>
        		</div>
        		<div class="col-md-6">
        			<div class="form-group">
						<label>To</label>
						<input data-toggle="datepicker" type="text" name="date_to" value="{{ $date_to }}" class="form-control" required>
					</div>
        		</div>
        		<div class="col-md-12">
        				<div class="form-group">
							<button type="submit" class="btn btn-info btn-block">Filter</button>
						</div>
        		</div>
        	</div>
        	</form>
			<div class="row">
				<div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <br />
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td><strong>Products</strong></td>
                                        <td><strong>Total Deposit</strong></td>
                                        <td><strong>Total Withdraw</strong></td>
                                        <td><strong>Total Win/Lose</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(\App\Game::all() as $game)
                                        <tr>
                                            <td>{{ $game->name }}</td>
                                            <td>
                                                SGD 
                                                @php
                                                    if(isset($_GET['date_from']) && isset($_GET['date_to']))
                                                    {
                                                        $arrStart = explode("-", $input['date_from']);
                                                        $arrEnd = explode("-", $input['date_to']);

                                                        $from = \Carbon\Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
                                                        $to = \Carbon\Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

                                                        $worth_deposit = DB::table('transactions')->where('data','LIKE','%game_id":"'.$game->id.'"%')->where('transaction_type','deposit')->where('deposit_type','normal')->where('status','2')->where('created_at','>=',$from)->where('created_at','<=',$to)->sum('amount');
                                                    }
                                                    else
                                                    {
                                                        $worth_deposit = DB::table('transactions')->where('data','LIKE','%game_id":"'.$game->id.'"%')->where('transaction_type','deposit')->where('deposit_type','normal')->where('status','2')->sum('amount');
                                                    }

                                                @endphp
                                                {{ number_format($worth_deposit,2) }}
                                            </td>
                                            <td>
                                                SGD 
                                                @php
                                                    if(isset($_GET['date_from']) && isset($_GET['date_to']))
                                                    {
                                                        $arrStart = explode("-", $input['date_from']);
                                                        $arrEnd = explode("-", $input['date_to']);

                                                        $from = \Carbon\Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
                                                        $to = \Carbon\Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

                                                        $worth_withdraw = DB::table('transactions')->where('data','LIKE','%game_id":"'.$game->id.'"%')->where('transaction_type','withdraw')->where('created_at','>=',$from)->where('created_at','<=',$to)->where('status','2')->sum('amount');
                                                    }
                                                    else
                                                    {
                                                        $worth_withdraw = DB::table('transactions')->where('data','LIKE','%game_id":"'.$game->id.'"%')->where('transaction_type','withdraw')->where('status','2')->sum('amount');
                                                    }

                                                @endphp
                                                {{ number_format($worth_withdraw,2) }}
                                            </td>
                                            <td>SGD {{ number_format($worth_deposit - $worth_withdraw,2) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>
                
        	</div>

            <hr />
            
        	

            
                    
        </div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy'
	});
</script>
</body></html>