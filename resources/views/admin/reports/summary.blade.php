@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	<div class="row">
        		<div class="col-md-12">
        			<h3>Members Total W/L</h3>
        		</div>
        	</div>
        	<hr />
        	<form method="get">
        	<div class="row">
        		@php

        			if(isset($input['date_from']))
        			{
        				$date_from= $input['date_from'];
        				$date_to = $input['date_to'];

                        if(isset($input['member_id']))
                        {
                            $member_id = $input['member_id'];
                        }
                        else
                        {
                            $member_id = '';
                        }
        			}
        			else
        			{
        				$date_from = '';
        				$date_to = '';
                        $member_id = '';
        			}

        		@endphp
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Member Username</label>
                        <input type="text" name="member_id" class="form-control" value="{{ $member_id }}">
                    </div>
                </div>
        		<div class="col-md-6">
        			<div class="form-group">
						<label>From</label>
						<input data-toggle="datepicker" type="text" name="date_from" class="form-control" value="{{ $date_from }}" required>
					</div>
        		</div>
        		<div class="col-md-6">
        			<div class="form-group">
						<label>To</label>
						<input data-toggle="datepicker" type="text" name="date_to" value="{{ $date_to }}" class="form-control" required>
					</div>
        		</div>
        		<div class="col-md-12">
        				<div class="form-group">
							<button type="submit" class="btn btn-info btn-block">Filter</button>
                            <a href="{{ url('admin/reports') }}" class="btn btn-default btn-block">Reset</a>
						</div>
        		</div>
        	</div>
        	</form>
			<div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><strong>Total User Registered</strong></td>
                                <td>{{ $total_user }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Member Deposit</strong></td>
                                <td>SGD {{ $worth_deposit }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Member Withdraw</strong></td>
                                <td>SGD {{ $worth_withdrawal }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Win / Loss</strong></td>
                                <td>
                                    @if($winlose < 0)
                                        <span class="label label-danger">SGD {{ $winlose }}</span>
                                    @else
                                        <span class="label label-success">SGD {{ $winlose }}</span>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
				<div class="col-md-6">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><strong>Total Bonus Claimed</strong></td>
                                <td>SGD {{ $worth_bonus }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Deposit Transaction</strong></td>
                                <td>{{ $count_deposit }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Withdraw Transaction</strong></td>
                                <td>{{ $count_withdrawal }}</td>
                            </tr>
                            <tr>
                                <td><strong>Total Spin Reward</strong></td>
                                <td>SGD {{ $total_spin_reward }}</td>
                            </tr>
                        </tbody>
                    </table>
				</div>
                
        	</div>

            <hr />
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-body">
                            <br />
                            <table id="games-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Member Username</th>
                                        <th>Type</th>
                                        <th>Amount (SGD)</th>
                                        <th>Product / ID</th>
                                        <th>Updated By</th>
                                        <th>Updated At</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions as $transaction)
                                        @if($transaction->deposit_type == 'bonus')
                                            <tr>
                                                <td>
                                                    #D{{ sprintf('%06d', $transaction->id) }}
                                                </td>
                                                <td>{{ $transaction->user->username }}</td>
                                                <td>
                                                    <span class="label label-warning">Bonus</span>
                                                </td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>
                                                    @php
                                                        $data = json_decode($transaction->data, true);
                                                        $game = \App\Game::find($data['game_id']);

                                                        $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();

                                                        if($game_id)
                                                        {
                                                            $username =  $game_id->username;
                                                        }
                                                        else
                                                        {
                                                            $username = "-";
                                                        }
                                                    @endphp
                                                    {{ $game->name }} / {{ $username }}
                                                </td>
                                                <td>
                                                    @if($transaction->logLatest->count() > 0)
                                                        {{ $transaction->logLatest->first()->user->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->updated_at->format('d M Y, h:i A') }}</td>
                                                <td>-</td>
                                            </tr>
                                        @elseif($transaction->deposit_type == 'rebate')

                                        @elseif($transaction->deposit_type == 'birthday')
                                            <tr>
                                                <td>
                                                    #BD{{ sprintf('%06d', $transaction->id) }}
                                                </td>
                                                <td>{{ $transaction->user->username }}</td>
                                                <td>
                                                    <span class="label label-warning">Birthday</span>
                                                </td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>
                                                    @php
                                                        $data = json_decode($transaction->data, true);
                                                        $game = \App\Game::find($data['game_id']);

                                                        $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();

                                                        if($game_id)
                                                        {
                                                            $username =  $game_id->username;
                                                        }
                                                        else
                                                        {
                                                            $username = "-";
                                                        }
                                                    @endphp
                                                    {{ $game->name }} / {{ $username }}
                                                </td>
                                                <td>
                                                    @if($transaction->logLatest->count() > 0)
                                                        {{ $transaction->logLatest->first()->user->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->updated_at->format('d M Y, h:i A') }}</td>
                                                <td><a href="{{ url('admin/transaction/'.$transaction->id) }}" class="label label-info">view</a></td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td>#
                                                    @if($transaction->transaction_type == 'deposit')
                                                        @if($transaction->deposit_type == 'birthday')
                                                            BD{{ sprintf('%06d', $transaction->id) }}
                                                        @else
                                                            D{{ sprintf('%06d', $transaction->id) }}
                                                        @endif

                                                    @elseif($transaction->transaction_type == 'withdraw')
                                                        W{{ sprintf('%06d', $transaction->id) }}
                                                    @elseif($transaction->transaction_type == 'transfer')
                                                        T{{ sprintf('%06d', $transaction->id) }}
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->user->username }}</td>
                                                <td>
                                                    @if($transaction->transaction_type == 'deposit')
                                                        <span class="label label-success">Deposit</span>
                                                    @elseif($transaction->transaction_type == 'withdraw')
                                                        <span class="label label-danger">Withdraw</span>
                                                    @else
                                                        <span class="label label-info">Transfer</span>
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>
                                                    @php
                                                        $data = json_decode($transaction->data, true);
                                                        $game = \App\Game::find($data['game_id']);

                                                        $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();

                                                        if($game_id)
                                                        {
                                                            $username =  $game_id->username;
                                                        }
                                                        else
                                                        {
                                                            $username = "-";
                                                        }
                                                    @endphp
                                                    {{ $game->name }} / {{ $username }}
                                                </td>
                                                <td>
                                                    @if($transaction->logLatest->count() > 0)
                                                        {{ $transaction->logLatest->first()->user->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>{{ $transaction->updated_at->format('d M Y, h:i A') }}</td>
                                                <td><a href="{{ url('admin/transaction/'.$transaction->id) }}" class="label label-info">view</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $transactions->appends(Request::except('page'))->links() }}
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        	

            
                    
        </div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy'
	});
</script>
</body></html>