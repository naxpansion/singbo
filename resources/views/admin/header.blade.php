<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="{{ secure_asset('assets/img/logo-fav.png') }}">
        <title>Singbet9 | Management System</title>
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/lib/jqvmap/jqvmap.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.4/datepicker.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="{{ secure_asset('assets/css/style.css') }}" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <!-- Magnific Popup core CSS file -->
        <link rel="stylesheet" href="{{ secure_asset('plugins/magnific-popup.css') }}">
        <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=cluh4p05n9l4p32jfwklu09733uq9hlajlgy8a7uoxanksn6"></script>
        <script>
            tinymce.init({
                selector:'.editor',
                plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                    ],
                    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons | code',
                height: 600,
            });

        </script>
        <style type="text/css">
            .datepicker-container { z-index: 10000 !important; }

            input[type=number]::-webkit-inner-spin-button,
            input[type=number]::-webkit-outer-spin-button {
              -webkit-appearance: none;
              margin: 0;
            }
        </style>
    </head>
    <body>
        <div class="be-wrapper be-fixed-sidebar">
            <nav class="navbar navbar-default navbar-fixed-top be-top-header">
                <div class="container-fluid">
                    <div class="navbar-header"><a href="{{ url('/admin') }}" class="navbar-brand"><img alt="Best online casino Singapore singbet9" src="{{ url('images/logo.png') }}" width="185%"></a>
                    </div>
                    <div class="be-right-navbar">
                        <ul class="nav navbar-nav navbar-right be-user-nav">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{ secure_asset('assets/img/avatar.png') }}" alt="Avatar"><span class="user-name">{{ Auth::user()->name }}</span></a>
                                <ul role="menu" class="dropdown-menu">
                                    <li>
                                        <div class="user-info">
                                            <div class="user-name">{{ Auth::user()->name }}</div>
                                            <div class="user-position online">Online</div>
                                        </div>
                                    </li>
                                    <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                                    <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> Logout</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </li>
                        </ul>
                        <div class="page-title"><span></span></div>
                        <ul class="nav navbar-nav navbar-right be-icons-nav">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span>
                                @if(\Auth::user()->unreadNotifications->count() > 0)
                                    <span class="indicator"></span>
                                @endif
                                </a>
                                <ul class="dropdown-menu be-notifications">
                                    <li>
                                        <div class="title">Notifications<span class="badge">{{ \Auth::user()->unreadNotifications->count() }}</span></div>
                                        <div class="list">
                                            <div class="be-scroller">
                                                <div class="content">
                                                    <ul>
                                                        @foreach(Auth::user()->unreadNotifications as $notification)
                                                        <li class="notification notification-unread">
                                                            @if($notification->type == 'App\Notifications\NewDeposit')
                                                                <a href="{{ url('/admin/transaction/'.$notification->data['transaction_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>
                                                            @elseif($notification->type == 'App\Notifications\NewWithdraw')
                                                                <a href="{{ url('/admin/transaction/'.$notification->data['transaction_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>
                                                            @elseif($notification->type == 'App\Notifications\NewTransfer')
                                                                <a href="{{ url('/admin/transaction/'.$notification->data['transaction_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>

                                                            @elseif($notification->type == 'App\Notifications\NewPasswordRequest')
                                                                <a href="{{ url('/admin/users/forgot/'.$notification->data['request_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>

                                                            @elseif($notification->type == 'App\Notifications\NewSpinReward')
                                                                <a href="{{ url('/admin/rewards/spin/'.$notification->data['transaction_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>

                                                            @elseif($notification->type == 'App\Notifications\AffiliateRequest')
                                                                <a href="{{ url('/admin/users/'.$notification->data['transaction_id']) }}">
                                                                    <div class="image"><img src="{{ secure_asset('assets/img/avatar2.png') }}" alt="Avatar"></div>
                                                                    <div class="notification-info">
                                                                        <div class="text"><span class="user-name">{{ $notification->data['title'] }}</span> {{ $notification->data['message'] }}</div>
                                                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                                                    </div>
                                                                </a>

                                                            @endif
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="footer"> <a href="{{ url('admin/') }}">View all notifications</a></div> --}}
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="be-left-sidebar">
                <div class="left-sidebar-wrapper">
                    <a href="#" class="left-sidebar-toggle">Dashboard</a>
                    <div class="left-sidebar-spacer">
                        <div class="left-sidebar-scroll">
                            <div class="left-sidebar-content">
                                <ul class="sidebar-elements">
                                    <li class="divider">Menu</li>
                                    <li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{ url('admin') }}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                                    </li>

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','transaction')->count() > 0)
                                        <li class="parent {{ Request::is('admin/transaction*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-view-list-alt"></i><span>Transactions</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/transaction/deposit') ? 'active' : '' }}"><a href="{{ url('admin/transaction/deposit') }}">Deposit Transaction</a>
                                                </li>
                                                <li class="{{ Request::is('admin/transaction/withdrawal') ? 'active' : '' }}"><a href="{{ url('admin/transaction/withdrawal') }}">Withdrawal Transaction</a>
                                                </li>
                                                <li class="{{ Request::is('admin/transaction/transfer') ? 'active' : '' }}"><a href="{{ url('admin/transaction/transfer') }}">Transfer Transaction</a>
                                                </li>
                                                <li class="{{ Request::is('admin/rewards/spin') ? 'active' : '' }}"><a href="{{ url('admin/rewards/spin') }}">Reward Transaction</a>
                                                </li>
                                                <li class="{{ Request::is('admin/transaction/birthday') ? 'active' : '' }}"><a href="{{ url('admin/transaction/birthday') }}">Birthday Bonus</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','report')->count() > 0)
                                        <li class="parent {{ Request::is('admin/reports*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-chart"></i><span>Reports</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/reports') ? 'active' : '' }}"><a href="{{ url('admin/reports') }}">Members Total W/L</a>
                                                </li>
                                                <li class="{{ Request::is('admin/reports/product') ? 'active' : '' }}"><a href="{{ url('admin/reports/product') }}">Products Total W/L</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','bank')->count() > 0)
                                        <li class="parent {{ Request::is('admin/banks*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-city"></i><span>Banks</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/banks') ? 'active' : '' }}"><a href="{{ url('admin/banks') }}">Banks List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/banks/create') ? 'active' : '' }}"><a href="{{ url('admin/banks/create') }}">Add New Bank</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','product')->count() > 0)
                                        <li class="parent {{ Request::is('admin/games*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-gamepad"></i><span>Products</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/games') ? 'active' : '' }}"><a href="{{ url('admin/games') }}">Products List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/games/create') ? 'active' : '' }}"><a href="{{ url('admin/games/create') }}">Add New Product</a>
                                                </li>
                                            </ul>
                                        </li>

                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','member')->count() > 0)
                                        <li class="parent {{ Request::is('admin/users*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-account"></i><span>Members</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/users') ? 'active' : '' }}"><a href="{{ url('admin/users') }}">Members List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/affiliate') ? 'active' : '' }}"><a href="{{ url('admin/affiliate') }}">Affiliate List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/affiliate-request') ? 'active' : '' }}"><a href="{{ url('admin/affiliate-request') }}">Affiliate Request</a>
                                                </li>
                                                <li class="{{ Request::is('admin/users/create') ? 'active' : '' }}"><a href="{{ url('admin/users/create') }}">Add New Member</a>
                                                </li>
                                                <li class="{{ Request::is('admin/groups') ? 'active' : '' }}"><a href="{{ url('admin/groups') }}">VIP Member Tag</a>
                                                <li class="{{ Request::is('admin/groups/create') ? 'active' : '' }}"><a href="{{ url('admin/groups/create') }}">Add VIP Member Tag</a>
                                                </li>
                                                <li class="{{ Request::is('admin/users/forgot') ? 'active' : '' }}"><a href="{{ url('admin/users/forgot') }}">Password Request</a>
                                                </li>
                                                <li class="{{ Request::is('admin/users/export') ? 'active' : '' }}"><a href="{{ url('admin/users/export') }}">Export Member</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','bonus')->count() > 0)
                                        <li class="parent {{ Request::is('admin/bonuses*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-ticket-star"></i><span>Bonuses</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/bonuses') ? 'active' : '' }}"><a href="{{ url('admin/bonuses') }}">Bonus List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/bonuses/create') ? 'active' : '' }}"><a href="{{ url('admin/bonuses/create') }}">Add New Bonus Code</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','promotion')->count() > 0)
                                        <li class="parent {{ Request::is('admin/promotions*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-desktop-mac"></i><span>Promotions</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/promotions') ? 'active' : '' }}"><a href="{{ url('admin/promotions') }}">Promotions List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/promotions/create') ? 'active' : '' }}"><a href="{{ url('admin/promotions/create') }}">Create New Promotion</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','spin')->count() > 0)
                                        <li class="{{ Request::is('admin/wheel*') ? 'active' : '' }}"><a href="{{ url('admin/wheel') }}"><i class="icon mdi mdi-circle"></i><span>Spin Me</span></a>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','sport')->count() > 0)
                                        <li class="parent {{ Request::is('admin/sports*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-circle"></i><span>Sport Match</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/sports') ? 'active' : '' }}"><a href="{{ url('admin/sports') }}">Sport List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/sports/create') ? 'active' : '' }}"><a href="{{ url('admin/sports/create') }}">Create New Sport</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','annoucement')->count() > 0)
                                        <li class="parent {{ Request::is('admin/annoucement*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-notifications-active"></i><span>Announcement</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/annoucement') ? 'active' : '' }}"><a href="{{ url('admin/annoucement') }}">Announcement List</a>
                                                </li>
                                                <li class="{{ Request::is('admin/annoucement/create') ? 'active' : '' }}"><a href="{{ url('admin/annoucement/create') }}">Create New Announcement</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif



                                    @if(auth::user()->id == 1)

                                        <li class="divider">Admin Action</li>

                                        <li class="parent {{ Request::is('admin/list*') ? 'active' : '' }}">
                                            <a href="#"><i class="icon mdi mdi-star"></i><span>Admin</span></a>
                                            <ul class="sub-menu">
                                                <li class="{{ Request::is('admin/list') ? 'active' : '' }}"><a href="{{ url('admin/list') }}">Admin Users</a>
                                                </li>
                                                <li class="{{ Request::is('admin/list/new') ? 'active' : '' }}"><a href="{{ url('admin/list/new') }}">Add Admin User</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="{{ Request::is('admin/backup*') ? 'active' : '' }}"><a href="{{ url('admin/backup') }}"><i class="icon mdi mdi-cloud-download"></i><span>Backup</span></a>
                                        </li>

                                        <li class="{{ Request::is('admin/systemlogs*') ? 'active' : '' }}"><a href="{{ url('admin/systemlogs') }}"><i class="icon mdi mdi-book"></i><span>System Logs</span></a>
                                        </li>
                                    @endif









                                    @if(\App\Permission::where('user_id',\Auth::user()->id)->where('name','setting')->count() > 0)
                                        <li class="{{ Request::is('admin/settings*') ? 'active' : '' }}"><a href="{{ url('admin/settings') }}"><i class="icon mdi mdi-settings"></i><span>Settings</span></a>
                                         </li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="progress-widget">
                        <div class="progress-data text-center"><span class="name">Copyright {{ now()->format('Y') }} | Version 1.0.0</span></div>
                    </div>
                </div>
            </div>
