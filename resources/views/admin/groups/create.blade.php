@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Add VIP Member Tag</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/groups') }}" enctype="multipart/form-data">
			                	@csrf
							    <div class="form-group">
							        <label>Tag Name</label>
							        <input type="text" name="name"  class="form-control" placeholder="Diamond" required>
							    </div>
							    <div class="form-group">
							        <label>Total Deposit > (SGD)</label>
							        <input  type="text" name="total_deposit"  class="form-control" placeholder="" required>
							    </div>
							    <div class="form-group">
							        <label>Status </label>
							        {{ Form::select('status', ['0' => 'Not Active', '1' => 'Active'], null, ['class' => 'form-control']) }}
							    </div>
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Submit</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>