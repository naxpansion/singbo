@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
				@if($transaction->transaction_type == 'deposit')
				@if($transaction->deposit_type != 'rebate')
			    <div class="col-md-12">
			        <h3>Edit Transaction 
					@if($transaction->deposit_type == 'normal')
						[#D{{ sprintf('%06d', $transaction->id) }}]
					@else
						[#BD{{ sprintf('%06d', $transaction->id) }}]
					@endif
			        </h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/transaction/'.$transaction->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
			                	<input type="hidden" name="type_transaction" value="{{ $transaction->transaction_type }}">
							    <div class="form-group">
							        <label>Amount (SGD)</label>
							        <input type="number" step="0.01" name="amount" class="form-control" value="{{ $transaction->amount }}" required readonly="true">
							    </div>

							    @if($transaction->bank == null)
									@if($transaction->deposit_type == 'normal')
										<div class="form-group">
									        <label>Bank</label>
									        <select name="bank" class="form-control" required>
									        	@foreach(\App\Bank::all() as $bank)
													<option value="{{ $bank->id }}">{{ $bank->account_name }} ({{ $bank->account_no }})</option>
									        	@endforeach
									        </select>
									    </div>
									@endif
							    @else
									<input type="hidden" name="bank" value="{{ $transaction->bank->id }}">
							    @endif
			                	
							    <div class="form-group">
							        <label>Status</label>
							        {{ Form::select('status', ['' => 'Select', '2' => 'Completed', '3' => 'Rejected'], $transaction->status, ['class' => 'form-control', 'required' => 'required']) }}
							    </div>

							    <div class="form-group">
							        <label>Remarks</label>
							        <textarea name="remarks" class="form-control">{{ $transaction->remarks }}</textarea>
							    </div>
							    
							    <div class="form-group">
							    	<a href="{{ url()->previous() }}"><button type="button" class="btn btn-default">Back To Transaction Details</button></a>
							        <button type="submit" class="btn btn-info">Update Transaction</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			    @else
				<div class="col-md-12">
			        <h3>Edit Rebate [#{{ sprintf('%06d', $transaction->id) }}]</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/transaction/'.$transaction->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
			                	<input type="hidden" name="is_rebate" value="rebate">
			                	<input type="hidden" name="type_transaction" value="is_rebate">
							    <div class="form-group">
							        <label>Amount (SGD)</label>
							        <input type="number" step="0.01" name="amount" class="form-control" value="{{ $transaction->amount }}" required>
							    </div>
			                	
							    <div class="form-group">
							        <label>Status</label>
							        {{ Form::select('status', ['' => 'Select', '2' => 'Completed', '3' => 'Rejected'], $transaction->status, ['class' => 'form-control', 'required' => 'required']) }}
							    </div>

							    <div class="form-group">
							        <label>Remarks</label>
							        <textarea name="remarks" class="form-control">{{ $transaction->remarks }}</textarea>
							    </div>
							    
							    <div class="form-group">
							    	<a href="{{ url()->previous() }}"><button type="button" class="btn btn-default">Back To Transaction Details</button></a>
							        <button type="submit" class="btn btn-info">Update Transaction</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			    @endif
			    @elseif($transaction->transaction_type == 'withdraw')
			    <div class="col-md-12">
			        <h3>Edit Transaction [#W{{ sprintf('%06d', $transaction->id) }}]</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/transaction/'.$transaction->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
			                	<input type="hidden" name="type_transaction" value="{{ $transaction->transaction_type }}">
							    <div class="form-group">
							        <label>Amount (SGD)</label>
							        <input type="number" step="0.01" name="amount" class="form-control" value="{{ $transaction->amount }}" required>
							    </div>
			                	
							    <div class="form-group">
							        <label>Status</label>
							        {{ Form::select('status', ['' => 'Select', '2' => 'Completed', '3' => 'Rejected'], $transaction->status, ['class' => 'form-control', 'required' => 'required']) }}
							    </div>

							    <div class="form-group">
							        <label>Bank</label>
							        <select class="form-control" name="bank">
							        	@foreach(\App\Bank::all() as $bank)
							        		<option value="{{ $bank->id }}">{{ $bank->name }} - {{ $bank->account_name }}</option>
							        	@endforeach
							        </select>
							    </div>

							    <div class="form-group">
							        <label>Remarks</label>
							        <textarea name="remarks" class="form-control">{{ $transaction->remarks }}</textarea>
							    </div>
							    
							    <div class="form-group">
							    	<a href="{{ url()->previous() }}"><button type="button" class="btn btn-default">Back To Transaction Details</button></a>
							        <button type="submit" class="btn btn-info">Update Transaction</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			    @elseif($transaction->transaction_type == 'transfer')
			    <div class="col-md-12">
			        <h3>Edit Transaction [#T{{ sprintf('%06d', $transaction->id) }}]</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/transaction/'.$transaction->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
			                	<input type="hidden" name="type_transaction" value="{{ $transaction->transaction_type }}">
							    <div class="form-group">
							        <label>Amount (SGD)</label>
							        <input type="number" step="0.01" name="amount" class="form-control" value="{{ $transaction->amount }}" required>
							    </div>
			                	
							    <div class="form-group">
							        <label>Status</label>
							        {{ Form::select('status', ['' => 'Select', '2' => 'Completed', '3' => 'Rejected'], $transaction->status, ['class' => 'form-control', 'required' => 'required']) }}
							    </div>

							    <div class="form-group">
							        <label>Remarks</label>
							        <textarea name="remarks" class="form-control">{{ $transaction->remarks }}</textarea>
							    </div>
							    
							    <div class="form-group">
							    	<a href="{{ url()->previous() }}"><button type="button" class="btn btn-default">Back To Transaction Details</button></a>
							        <button type="submit" class="btn btn-info">Update Transaction</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			    @endif
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>