@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Transaction Details [
											@if($transaction->transaction_type == 'deposit')
												@if($transaction->deposit_type == 'birthday')
													#BD{{ sprintf('%06d', $transaction->id) }}
				                    			@else
													#D{{ sprintf('%06d', $transaction->id) }}
				                    			@endif

											@elseif($transaction->transaction_type == 'withdraw')
												#W{{ sprintf('%06d', $transaction->id) }}
											@elseif($transaction->transaction_type == 'transfer')
												#T{{ sprintf('%06d', $transaction->id) }}
											@endif]</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="30%"><strong>Member Username</strong></td>
			                					<td>{{ $transaction->user->username }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Transaction Type</strong></td>
			                					<td style="text-transform: capitalize;">
													@if($transaction->deposit_type == 'birthday')
														Birthday Bonus
													@else
														{{ $transaction->transaction_type }}
													@endif
			                					</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Transaction Status</strong></td>
			                					<td>
			                						@if($transaction->status == 1)
			                							<span class="label label-warning">Progress</span>
			                						@elseif($transaction->status == 2)
			                							<span class="label label-success">Success</span>
			                						@elseif($transaction->status == 3)
			                							<span class="label label-danger">Rejected</span>
			                						@endif
			                					</td>
			                				</tr>
			                				@if($transaction->transaction_type == 'deposit')
			                					@php

                                                    $data = json_decode($transaction->data, true);
                                                    $game = \App\Game::find($data['game_id']);

                                                    if($game)
                                                    {
                                                    	$game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();
	                                                    if($game_id){
	                                                    	$username =  $game_id->username;
	                                                    }
	                                                    else
	                                                    {
	                                                    	$username = "-";
	                                                    }
                                                    }
                                                    else
                                                    {
                                                    	$username = "-";
                                                    }
                                                    
                                                @endphp

                                                @if($transaction->deposit_type == 'rebate')
				                					
					                				<tr>
					                					<td><strong>Deposit Type</strong></td>
					                					<td>Rebate</td>
					                				</tr>
					                				<tr>
					                					<td><strong>Game</strong></td>
					                					<td>{{ $game->name }}</td>
					                				</tr>

					                			@elseif($transaction->deposit_type == 'birthday')
													
													<tr>
					                					<td><strong>Scanned NRIC</strong></td>
					                					<td><a href="{{ url('storage/receipt/'.$transaction->receipt_file) }}" target="_blank">{{ $transaction->receipt_file }}</a></td>
					                				</tr>
					                				<tr>
					                					<td>
					                						<strong>Product / ID</strong>
					                					</td>
					                					<td>
															@if(!$game)
																Game not selected
															@else
																{{ $game->name }} / {{ $username }}
															@endif
					                					</td>
					                				</tr>

					                			@else
													<tr>
					                					<td><strong>Product / ID</strong></td>
					                					<td>
															@if(!$game)
																Game not selected
															@else
																{{ $game->name }} / {{ $username }}
															@endif
					                					</td>
					                				</tr>
					                				<tr>
					                					<td><strong>Deposit Amount</strong></td>
					                					<td>SGD {{ $transaction->amount }}</td>
					                				</tr>
					                				@if($transaction->bank == null)
													<tr>
					                					<td><strong>Bank Name</strong></td>
					                					<td><code>Not Set</code></td>
					                				</tr>
					                				@else
					                				<tr>
					                					<td><strong>Bank Name</strong></td>
					                					<td>{{ $transaction->bank->name }}</td>
					                				</tr>
					                				@endif
					                				<tr>
					                					<td><strong>Payment Menthod</strong></td>
					                					<td>{{ $data['payment_method'] }}</td>
					                				</tr>
					                				<tr>
					                					<td><strong>Date & Time</strong></td>
					                					<td>{{ $transaction->datetime }}</td>
					                				</tr>
					                				<tr>
					                					<td><strong>Receipt Image</strong></td>
					                					<td><a href="{{ url('storage/receipt/'.$transaction->receipt_file) }}" target="_blank">{{ $transaction->receipt_file }}</a></td>
					                				</tr>
					                				<tr>
					                					<td><strong>Referrence No</strong></td>
					                					<td>{{ $transaction->refference_no }}</td>
					                				</tr>
					                				@if($transaction->bonus_id == 0)
					                					<tr>
						                					<td><strong>Bonus</strong></td>
						                					<td>No Bonus Code</td>
						                				</tr>
					                				@else
					                					<tr>
						                					<td><strong>Bonus</strong></td>
						                					<td>{{ $transaction->bonus->name }}</td>
						                				</tr>
					                				@endif
					                			@endif
			                				@elseif($transaction->transaction_type == 'withdraw')
			                					@php

                                                    $data = json_decode($transaction->data, true);
                                                    $game = \App\Game::find($data['game_id']);

                                                    $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();

                                                    if($game_id){
                                                    	$username =  $game_id->username;
                                                    }
                                                    else
                                                    {
                                                    	$username = "-";
                                                    }

                                                @endphp
			                					<tr>
				                					<td><strong>Product / ID</strong></td>
				                					<td>{{ $game->name }} / {{ $username }}</td>
				                				</tr>
				                				<tr>
				                					<td><strong>Withdraw Amount</strong></td>
				                					<td>SGD {{ $transaction->amount }}</td>
				                				</tr>
				                				
				                				<tr>
				                					<td><strong>Last Deposit Transaction</strong></td>
				                					@php

				                						$last_transaction = \App\Transaction::where('user_id',$transaction->user->id)->where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->latest()->first();

				                					@endphp
				                					@if(!$last_transaction)
													<td><code style="color: red;">No Last Transaction</code></td>
				                					@else
				                					<td>
														<ul>
															<li>Transaction ID : [#{{ sprintf('%06d', $last_transaction->id) }}]</li>
															<li>Deposit Amount : SGD {{ $last_transaction->amount }}</li>
															@if($last_transaction->bonus == null)
																<li>Bonus Amount : -</li>
															@else
																@php
																	$bonus = \App\Transaction::where('bonus_for',$last_transaction->id)->first();
																	
																@endphp
																@if($bonus)
																	@php
																		$bonus_turnover = \App\Bonus::find($bonus->bonus_id)->turnover;
																	@endphp
																<li>Bonus Amount : SGD {{ $bonus->amount }}</li>
																<li>Turnover : SGD {{ number_format(($bonus->amount + $last_transaction->amount) * $bonus_turnover,2) }}</li>
																@else

																@endif
															@endif
															<li>Deposit Date & Time : {{ $last_transaction->created_at->format('d/m/Y h:i A') }}</li>
														</ul>
				                					</td>
				                					@endif
				                				</tr>
				                				<tr>
				                					<td><strong>Member Bank Name</strong></td>
				                					<td>{{ $transaction->user->bank_name }}</td>
				                				</tr>
												<tr>
				                					<td><strong>Member Fullname</strong></td>
				                					<td>{{ $transaction->user->name }}</td>
				                				</tr>
				                				<tr>
				                					<td><strong>Member Bank Account </strong></td>
				                					<td>{{ $transaction->user->bank_account_no }}</td>
				                				</tr>

				                				@if($transaction->bank_id == null)

				                				@else
				                					<tr>
					                					<td><strong>Withdraw From Bank</strong></td>
					                					<td>{{ $transaction->bank->name }} - {{ $transaction->bank->account_name }}</td>
					                				</tr>
				                				@endif
				                				
				                				
			                				@elseif($transaction->transaction_type == 'transfer')

				                				@php

	                                                $data = json_decode($transaction->data, true);
	                                                $transfer_from = \App\Game::find($data['from_game']);
	                                                $transfer_to = \App\Game::find($data['to_game']);

                                                    if($transfer_from)
                                                    {
                                                    	$from_game = \App\GameAccount::where('game_id',$transfer_from->id)->where('user_id',$transaction->user->id)->first();

                                                    	if($from_game)
                                                    	{
                                                    		$username_from =  $from_game->username;
                                                    	}
                                                    	else
                                                    	{
                                                    		$username_from = "-";
                                                    	}
                                                    }
                                                    else
                                                    {
                                                    	$username_from = "-";
                                                    }

                                                    if($transfer_to)
                                                    {
                                                    	 $to_game = \App\GameAccount::where('game_id',$transfer_to->id)->where('user_id',$transaction->user->id)->first();

                                                    	if($to_game)
                                                    	{
                                                    		$username_to =  $to_game->username;
                                                    	}
                                                    	else
                                                    	{
                                                    		$username_to = "-";
                                                    	}
                                                    }
                                                    else
                                                    {
                                                    	$username_to = "-";
                                                    }

	                                            @endphp
				                				<tr>
				                					<td><strong>From Product / ID</strong></td>
				                					<td>
														@if(!$transfer_from)
															Game not selected
														@else
															{{ $transfer_from->name }} / {{ $username_from }}
														@endif
				                					</td>
				                				</tr>
				                				<tr>
				                					<td><strong>To Product / ID</strong></td>
				                					<td>
				                						@if(!$transfer_to)
															Game not selected
														@else
															{{ $transfer_to->name }} / {{ $username_to }}
														@endif
				                					</td>
				                				</tr>
				                				<tr>
				                					<td><strong>Transfer Amount</strong></td>
				                					<td>SGD {{ $transaction->amount }}</td>
				                				</tr>
				                				
			                				@endif

			                				
			                				<tr>
			                					<td><strong>Remarks / Notes</strong></td>
			                					<td>{{ $transaction->remarks }}</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Request Submited</strong></td>
			                					<td>{{ $transaction->created_at->format('d/m/Y,  h:iA') }}</td>
			                				</tr>
			                			</tbody>
			                		</table>
			                		@if($transaction->status == 1 && $transaction->transaction_type == 'deposit')
			                		<div class="row">
			                			<div class="col-md-6">
			                				<a href="{{ url('admin/transaction/'.$transaction->id.'/approve') }}" onclick="return confirm('Are you sure?')" class="btn btn-block btn-success">APPROVE</a>
			                			</div>
			                			<div class="col-md-6">
			                				<a href="{{ url('admin/transaction/'.$transaction->id.'/reject') }}" onclick="return confirm('Are you sure?')" class="btn btn-block btn-danger">REJECT</a>
			                			</div>
			                		</div>
			                		@endif
			                	</div>
			                	<div class="col-md-6">
			                		<a href="{{ url('admin/transaction/'.$transaction->id.'/edit') }}" class="btn btn-info btn-block">Update Transaction</a><br />
			                		@if($transaction->transaction_type == 'deposit')
			                		@if($transaction->deposit_type == 'rebate')

			                		@elseif($transaction->deposit_type == 'birthday')

			                		@else
			                		
			                		@if($transaction->status == 1)
										<div class="well" style="text-align: center">Bonus only can be added once the transaction is approve.</div>
			                		@elseif($transaction->status == 2)
			                		<button data-toggle="modal" data-target="#modal-bonus" type="button" class="btn btn-info btn-block">Add Bonus For This Transaction</button>
			                		@endif
			                		<br />
			                		<h4>Bonus</h4>
			                		<table class="table table-bordered table-striped">
			                			<thead>
					                        <tr>
					                            <th>Bonus Name</th>
					                            <th>Amount</th>
					                            <th>Date & Time</th>
					                            <th>Action</th>
					                        </tr>
					                    </thead>
			                			<tbody>
			                				@foreach($bonuses as $bonus)
			                				<tr>
			                					<td>{{ $bonus->bonus->name }}</td>
			                					<td>SGD {{ $bonus->amount }}</td>
			                					<td>{{ $bonus->created_at->format('d/m/Y, h:i A') }}</td>
			                					<td><a onclick="return confirm('Are you sure?');" href="{{ url('admin/transaction/'.$bonus->id.'/deletebonus') }}" class="label label-danger">Delete</a></td>
			                				</tr>
			                				@endforeach
			                			</tbody>
			                		</table>
			                		@php
			                			$check_reward = \App\Spin::where('transaction_id',$transaction->id)->first();
			                		@endphp

			                		@if($check_reward)
										<div class="well"><h4>This Transaction is requested with rewards [{{ $check_reward->reward }}] <br />Reward ID : #{{ $check_reward->id }}</h4></div>
			                		@endif

			                		<br />
			                		<h4>Transaction Logs</h4>
			                		<table class="table table-bordered table-striped">
			                			<thead>
					                        <tr>
					                            <th>Updated By</th>
					                            <th>Details</th>
					                            <th>Date & Time</th>
					                        </tr>
					                    </thead>
			                			<tbody>
			                				@foreach($logs as $log)
			                				<tr>
			                					<td>{{ $log->user->name }}</td>
			                					<td>{!! $log->detail !!}</td>
			                					<td>{{ $log->created_at->format('d/m/Y, h:i A') }}</td>
			                				</tr>
			                				@endforeach
			                			</tbody>
			                		</table>
			                		@endif

				                		@if(Auth::user()->id == 1)
					                		<form method="post" action="{{ url('admin/transaction/'.$transaction->id.'/delete') }}">
					                		@csrf
					                		@method('delete')
					                		<button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are you sure?');">Delete</button>
					                		</form>
					                	@endif
			                		@endif
			                		
			                	</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
		@if($transaction->bonus_id == 0)
			<div id="modal-bonus" tabindex="-1" role="dialog" class="modal fade">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
			            </div>
			            <div class="modal-body">
							<h3 class="text-center">This Transaction Has No Bonus</h3>
			            </div>
			            <div class="modal-footer"></div>
			        </div>
			    </div>
			</div> 	                					
		@else
			<div id="modal-bonus" tabindex="-1" role="dialog" class="modal fade">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
			            </div>
			            <div class="modal-body">
							<form method="POST" action="{{ url('admin/transaction/addbonus') }}">
							    @csrf
							    <input type="hidden" name="user_id" value="{{ $transaction->user_id }}">
							    <input type="hidden" name="transaction_id" value="{{ $transaction->id }}">

							    <div class="row">
							    	<div class="form-group col-md-12">
								        <label>Bonus Name</label>
								        <input type="text" name="bonus_name" class="form-control" value="{{ $transaction->bonus->name }}" readonly="true">
								    </div>
							    </div>
							    
							    <div class="row">
							    	<div class="form-group col-md-12">
								        <label>Bonus Amount (SGD)</label>
								        <input type="number" name="bonus_amount" step="0.01" class="form-control" required>
								    </div>
							    </div>
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info btn-block">Add Bonus</button>
							    </div>
							</form>
			            </div>
			            <div class="modal-footer"></div>
			        </div>
			    </div>
			</div>               					
		@endif
    </div>
@include('admin.footer')
</body></html>