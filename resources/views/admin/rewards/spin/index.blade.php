@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>All Reward Request</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <div class="row">
	                            <div class="col-xs-8 form-inline" style="position: absolute; z-index: 2;">
	                                <div class="input-daterange input-group" id="datepicker">
	                                	<span class="input-group-addon">From</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="from" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" />
	                                    <span class="input-group-addon">To</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="to" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}"/>
	                                </div>
	                            </div>
	                        </div>
			                <table id="banks-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Transaction ID</th>
			                            <th>Created At</th>
			                            <th>Member Username</th>
			                            <th>Amount</th>
			                            <th>Date & Time Win</th>
			                            <th>Product Game / Game ID</th>
			                            <th>Status</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>
	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy',
	  	autoclose: true,
	});

	$("input[name=from]").change(function(){
	    oTable.draw();
	});

	$("input[name=to]").change(function(){
	    oTable.draw();
	});

	$("#status").change(function(){
	    oTable.draw();
	});

    var oTable = $('#banks-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url('admin/rewards/spin-data') }}',
            data: function(d) {
                d.from_date = $('input[name=from]').val();
                d.to_date = $('input[name=to]').val();
        	},
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'user.username', name: 'user.username' },
            { data: 'reward', name: 'reward' },
            { data: 'created_at', name: 'created_at' },
            { data: 'game', name: 'game' },
            { data: 'status', name: 'status' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
</script>
</body></html>