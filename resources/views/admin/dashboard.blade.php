@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="well"><h3>Pending Transaction : {{ $pending_transaction_count }}</h3></div>
				</div>
				<div class="col-md-4">
					<div class="well"><h3>Today New Registered : {{ $user_today }}</h3></div>
				</div>
				<div class="col-md-4">
					<div class="well"><h3>Today Spin Reward : SGD {{ $total_reward }}</h3></div>
				</div>
				<div class="col-md-4">
					<div class="well"><h3>Today Deposit : SGD {{ $today_deposit_sum }}</h3></div>
				</div>
				<div class="col-md-4">
					<div class="well"><h3>Today Withdraw : SGD {{ $today_withdraw_sum }}</h3></div>
				</div>
				<div class="col-md-4">
					<div class="well"><h3>Today Bonus : SGD {{ $today_bonus_amount }}</h3></div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-12">
        			<h3>Pending Transaction</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <table id="transaction-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Transaction ID</th>
			                            <th>Member Username</th>
			                            <th>Transaction Type</th>
			                            <th>Amount</th>
			                            <th>Request Submitted</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                    @if($pending_transactions->count() == 0)
			                    	<tr><td colspan="6">No Pending Transaction</td></tr>
			                    @else
			                    <tbody>
			                    	@foreach($pending_transactions as $pending_transaction)
			                    	<tr>
			                    		<td>#
											@if($pending_transaction->transaction_type == 'deposit')
												@if($pending_transaction->deposit_type == 'birthday')
													BD{{ sprintf('%06d', $pending_transaction->id) }}
				                    			@else
													D{{ sprintf('%06d', $pending_transaction->id) }}
				                    			@endif

											@elseif($pending_transaction->transaction_type == 'withdraw')
												W{{ sprintf('%06d', $pending_transaction->id) }}
											@elseif($pending_transaction->transaction_type == 'transfer')
												T{{ sprintf('%06d', $pending_transaction->id) }}
											@endif
			                    		</td>
			                    		<td>{{ $pending_transaction->user->username }}</td>
			                    		<td style="text-transform: capitalize;">
			                    			@if($pending_transaction->deposit_type == 'birthday')
												Birthday Bonus
			                    			@else
												{{ $pending_transaction->transaction_type }}
			                    			@endif
			                    		</td>
                						<td>SGD {{ $pending_transaction->amount  }}</td>
			                    		<td>{{ $pending_transaction->created_at->format('d/m/Y, h:i A')  }}</td>
			                    		<td>
			                    			<a href="{{ URL::to('admin/transaction/'.$pending_transaction->id) }}" class="btn btn-xs btn-info">View</a>
			                    		</td>
			                    	</tr>
			                    	@endforeach
			                    </tbody>
			                    @endif
			                </table>
			                {{ $pending_transactions->links() }}
			                <br />
			            </div>
			        </div>
				</div>
        	</div>

            
                    
        </div>
    </div>
@include('admin.footer')
</body></html>