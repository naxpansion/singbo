@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>All Member List's</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <div class="row">
	                            <div class="col-xs-8 form-inline" style="position: absolute; z-index: 2;">
	                                <div class="input-daterange input-group" id="datepicker">
	                                	<span class="input-group-addon">From</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="from" value="01-01-2019" />
	                                    <span class="input-group-addon">To</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="to" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}"/>
	                                    <span style="display: none;" class="input-group-addon">User Role</span>
	                                    <select id="select_role" class="form-control input-sm" style="border-radius: 0; display: none;">
	                                    	<option value="">ALL</option>
	                                    	<option value="3">User</option>
	                                    	<option value="2">Staff</option>
	                                    	<option value="4">Affiliate</option>
	                                    </select>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="table-responsive">
			                <table id="games-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                        	
			                            <th>Member Username</th>
			                            <th>Phone No.</th>
			                            <th>Member VIP Tag</th>
			                            <th>Bank Account No</th>
			                            <th>Remarks</th>
			                            <th>Total Dpt. (SGD)</th>
			                            <th>Total Wtd. (SGD)</th>
			                            <th>Total W/L (SGD</th>
			                            <th>Referral By</th>
			                            <th>Date Created</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                </div>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>

	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy',
	  	autoclose: true,
	});

	$("input[name=from]").change(function(){
	    oTable.draw();
	});

	$("input[name=to]").change(function(){
	    oTable.draw();
	});

	$("#select_role").change(function(){
	    oTable.draw();
	});

    
    var oTable = $('#games-table').DataTable({
    	processing: true,
		serverSide: false,
		order: [[ 9, "desc" ]],
        ajax: {
            url: '{{ url('admin/users/data') }}',
            data: function(d) {
                d.from_date = $('input[name=from]').val();
                d.to_date = $('input[name=to]').val();
                d.role = $('#select_role').val();
        	},
        },
        columns: [
            { data: 'username', name: 'username', orderable: true, searchable: true  },
            { data: 'phone', name: 'phone' , orderable: false, searchable: true },
            { data: 'group_id', name: 'group_id', orderable: true, searchable: true },
            { data: 'bank_account_no', name: 'bank_account_no', orderable: false, searchable: true  },
            { data: 'remarks', name: 'remarks', orderable: true, searchable: true  },
            { data: 'sum_deposit', name: 'sum_deposit',searchable: true },
            { data: 'sum_withdraw', name: 'sum_withdraw',searchable: true },
            { data: 'win_lose', name: 'win_lose',searchable: true },
            { data: 'referred_by', name: 'referred_by',orderable: true,searchable: true },
            { data: 'created_at', name: 'created_at', orderable: true, searchable: true  },
            { data: 'actions', name: 'actions', orderable: false, searchable: true }
        ]
	});
	$('[data-toggle="datepicker"]').change(function(){
		oTable.ajax.reload();
	});
    

</script>
</body></html>