<audio id="buzzer" src="{{ secure_asset('sound.ogg') }}" type="audio/ogg"></audio> 
</div>
        <script src="{{ secure_asset('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/js/main.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/jqvmap/jquery.vmap.min.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
        <script src="{{ secure_asset('assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
        <script type="text/javascript" src="https://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/wysihtml5x/dist/wysihtml5x-toolbar.min.js"></script>
        <script type="text/javascript" src="https://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/handlebars/handlebars.runtime.min.js"></script>
        <script type="text/javascript" src="https://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/mouse0270/bootstrap-notify/6a83ec48/bootstrap-notify.js"></script>
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.4/datepicker.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
            	//initialize the javascript
            	App.init();
            
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'DD-MM-YYYY hh:mm A'
                });
            });
        </script>
        <script>
       // Pusher.logToConsole = true;

        var pusher = new Pusher('6915dcb559dae710e1a6', {
            cluster: 'ap1',
            encrypted: true
        });

        var channel = pusher.subscribe('sbdots');

        channel.bind('transaction', function(data) {

            var buzzer = $('#buzzer')[0]; 
            buzzer.play();

            $.notify({
                // options
                icon: 'glyphicon glyphicon-bell',
                title: 'Alert!',
                message: 'You have new pending request transaction!',
                url: '{{ url('/admin') }}',
                target: '_self'
            }, {
                // settings
                type: 'success'
            });
        });

        channel.bind('register', function(data) {

            var buzzer = $('#buzzer')[0]; 
            buzzer.play();

            $.notify({
                // options
                icon: 'glyphicon glyphicon-bell',
                title: 'Alert!',
                message: 'You have new user register!',
                url: '{{ url('/admin') }}',
                target: '_self'
            }, {
                // settings
                type: 'success'
            });
        });

        channel.bind('reward', function(data) {

            var buzzer = $('#buzzer')[0]; 
            buzzer.play();

            $.notify({
                // options
                icon: 'glyphicon glyphicon-bell',
                title: 'Alert!',
                message: 'You have new pending reward request!',
                url: '{{ url('/admin') }}',
                target: '_self'
            }, {
                // settings
                type: 'success'
            });
        });

        $('form').on('focus', 'input[type=number]', function (e) {
          $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
          })
        })
        $('form').on('blur', 'input[type=number]', function (e) {
          $(this).off('mousewheel.disableScroll')
        })

      </script>