@extends ('front.master')
@section('content')

<div id="ctl00_cphBody_bg" class="banking">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>Register</p>
    </div>
    <center>
        <!--<div id="bank2">
            <img src="/images/cimbbank.jpg" />
            <img src="/images/maybank.jpg" />
            <img src="/images/public.jpg" />
            <img src="/images/hongleong.jpg" />
            </div>-->
        <div id="b-type">
            <form method="POST" action="{{ route('register') }}">

                @csrf
                <div class="row">
                    <div class="col-md-12">
                        a
                    </div>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table borderless">
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Fullname</h6></td>
                        <td>
                            <input name="name" type="text" id="name" placeholder="Full Name" autocomplete="name" class="field-input" onkeyup="nospaces(this)" value="{{old('name') }}" required />
                            <sub>*The name must match with your bank account name for withdrawal*</sub>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Username</h6></td>
                        <td>
                            <input name="username" type="text" id="username" placeholder="Username (No space)" autocomplete="username" class="field-input" onkeyup="nospaces(this)" value="{{old('username') }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Mobile Number</h6></td>
                        <td>
                            <input name="phone" type="text" placeholder="60123456789" class="field-input " autocomplete="tel-national" value="{{old('phone') }}" required/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Password</h6></td>
                        <td>
                            <input name="password" type="password" autocomplete="new-password" placeholder="Password" class="field-input" required/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Confirm Password</h6></td>
                        <td>
                            <input name="password_confirmation" type="password" autocomplete="new-password" placeholder="Confirm Password" class="field-input" required/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Email</h6></td>
                        <td>
                            <input name="email" type="email" autocomplete="email" placeholder="Email Address" class="field-input" required/>
                        </td>
                    </tr>
                     <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Bank Name</h6></td>
                        <td>
                            <select name="bank_name" required class="field-input select">
                                <option value="">Select Bank</option>
                                <option value="POSB">POSB</option>
                                <option value="OCBC">OCBC</option>
                                <option value="DBS">DBS</option>
                                <option value="UOB">UOB</option>
                            </select>
                        </td>
                    </tr>
                     <tr>
                        <td style="text-align: left;" width="30%" class="align-middle"><h6>Bank Account Number</h6></td>
                        <td>
                            <input name="bank_account_no" type="text" id="bank_account_no" placeholder="Bank Account No" autocomplete="bank_account_no" class="field-input" value="{{old('bank_account_no') }}" required />
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td style="width: 375px;">
                            <div class="g-recaptcha" data-sitekey="6LdrOUoUAAAAAO3ICDvvbpAnGFz55RoADJ1YP4Vt"></div>
                        </td>
                    </tr>
                </table>
                <span class="text-error">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>You are already a registered member of us! Please Contact Customer Services for more assistance.</strong>
                        </span>
                    @endif
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ old('phone') }} has already used for registration. Please Contact Customer Services for more assistance.</strong>
                        </span>
                    @endif
                    </span>
                <input type="submit" name="btnsend" value="Submit" id="btnsend" class="btn btn-warning btn-more pull-right" />
            </form>
        </div>
    </center>
</div>
    
@endsection
@section('script')
<script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
<script type="text/javascript">
    $(".phonemask").mask('601000000000');
</script>
@endsection

