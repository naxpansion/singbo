@extends('mobile.master')
@section('content')
<div class="m-reg m-bg-cont">
    <div class="errMsg">
        <div style="width:90%;display:inline-block;">
        </div>
    </div>
    <div class="uni-title">
        <span class="txtreg">Login</span><br /><br />
        <span class="txtreg2">Enter your login details to proceed.</span>
    </div>
    <div id="ctl00_cphBody_pnlReg">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="reg-field">

                <label for="username" class="field-lbl">Username *</label>
                <input name="username" type="text" maxlength="16" id="username" class="field-input w95" placeholder="Username" required>

                <label for="password" class="field-lbl">Password *</label>
                <input name="password" type="password" maxlength="16" id="password" class="field-input w95" placeholder="Password" / required>
                <a href="{{ route('password.request')}}">Forget Password</a>
                @if ($errors->count() > 0)
                    <br />
                    <span class="errors" style="color: red; margin-left: 10px;">
                        <strong>{{ $errors->first() }}</strong>
                    </span>
                @endif
            </div>
            <div class="reg-btn">
                <div class="con-btn">
                    <button class="btn" type="submit">LOGIN</button>
                </div>
            </div>
        </form>
        <br /><br />
    </div>
</div>
@endsection