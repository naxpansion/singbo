@extends('mobile.master')
@section('banner')
<!-- ************************ BANNERS ************************ -->
<div id="divBan">
    <div id="slider">
        <div class="divBanS">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('mobile/images/ban_home1.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home2.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home3.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home4.jpg') }}" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div id="m-slider" style="display:none;">
        <div style="width:100%;">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('images/slides/1.png') }}" alt="Free bonus online casino" />
                    <img src="{{ secure_asset('images/slides/2.png') }}" alt="Free Daily bonus online casino" />
                    <img src="{{ secure_asset('images/slides/3.png') }}" alt="Member free bonus online casino singapore" />
                    <img src="{{ secure_asset('images/slides/4.png') }}" alt="Online casino Singapore free bonus" />
                    <img src="{{ secure_asset('images/slides/5.png') }}" alt="Best 918kiss slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/slide_7.jpeg') }}" alt="Mega888 slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/6.png') }}" alt="Best online casino referral bonus 25%" />
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ************************ BANNERS ************************ -->
@endsection
@section('content')

<div class="m-nav" id="m-nav">
    <div class="m-nav-container">
        <ul>
            <li class="" onclick="location.href='{{ url('/') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">SPORTBOOKS</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/live_casinos') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">LIVE CASINO</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/slots') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">SLOT</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/promotions') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">PROMOTIONS</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- ********************** MOBILE HOME PAGE SLOT LISTING ********************** -->

<div id="ctl00_cphBody_bg" class="banking">
                <div align="center">
                    <div class="m-bg-cont">
                        <center>
                            <div align="center" class="txt-vip">BANKING</div>
                            <div id="bank2">
                                <img src="{{ secure_asset('mobile/images/bank/1.png') }}" />
                                <img src="{{ secure_asset('mobile/images/bank/2.png') }}" />
                                <img src="{{ secure_asset('mobile/images/bank/3.png') }}" />
                                <img src="{{ secure_asset('mobile/images/bank/4.png') }}" />
                            </div>
                            <div id="b-type">
                                <div id="m-b-dep" class="b-method m-bank">
                                    <div class="bnkcaption">DEPOSIT</div>
                                    <table class="bnkcover">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <th class="br1">Banking Methods</th>
                                                        <td class="br2">Online transfer &amp; ATM &amp; CDM</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Minimum</th>
                                                        <td>SGD 30</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Maximum</th>
                                                        <td>SGD 100,000</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="br3">Processing Time</th>
                                                        <td class="br4">3-10 minutes</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="m-b-wd" class="b-method m-bank">
                                    <div class="bnkcaption">WITHDRAW</div>
                                    <table class="bnkcover">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <th class="br1">Banking Methods</th>
                                                        <td class="br2">Online transfer &amp; ATM &amp; CDM</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Minimum</th>
                                                        <td>SGD 50</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Maximum</th>
                                                        <td>SGD 10,000</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Processing Time</th>
                                                        <td>15 minutes</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="b-content" class="b-method">
                                    <ul>
                                        <li>Withdrawals will only be approve and release into the member’s bank account bearing the same name used to register at SINGBET9.</li>
                                        <li>Large withdrawal amount will take longer processing time.</li>
                                        <li>Member is required to meet at least one (1) time turnover of the deposited amount before withdrawal can be made.</li>
                                        <li>Withdrawal is not allowed if the rollover requirements for any bonus claimed have not been fulfilled.</li>
                                        <li>Please retain the Bank Receipt (if fund deposited via ATM) or Transaction Reference Number (if fund deposited via internet banking) as we will request for the proof of deposit.</li>
                                        <li>All deposits and withdrawals processing time are subject to internet banking availability.</li>
                                        <li>For other Local Banks, please liaise with our Customer Service via live chat for more info.</li>
                                        <li>Please refer to the terms and condition for more details.</li>
                                        <li>Receipt will be forfeit after 24 hours.</li>
                                    </ul>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
<div class="spaceFoot"></div>
@endsection
