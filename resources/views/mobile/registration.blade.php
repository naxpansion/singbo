@extends('mobile.master')
@section('content')
<div class="m-reg m-bg-cont">
    <div class="errMsg">
        <div style="width:90%;display:inline-block;">
            @if($errors->isEmpty())

            @else
                <p style="color: #fff; text-align: center;">{{ $errors->first() }}</p>
            @endif
        </div>
    </div>
    <div class="uni-title">
        <span class="txtreg">Register</span><br /><br />
        <span class="txtreg2">Please create your SINGBET9 member account.</span>
    </div>
    <div id="ctl00_cphBody_pnlReg">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="reg-field">
                <label for="name" class="field-lbl">Full Name *</label>
                <input name="name" type="text" id="name" class="field-input w95" placeholder="Full Name" required>
                <span class="alertfont reg_phone_format">
                    <span class="red">*</span> 
                    The name must match with your bank account name for withdrawal <span class="red">*
                    </span>
                </span>

                <label for="username" class="field-lbl">Username *</label>
                <input name="username" type="text" maxlength="16" id="username" class="field-input w95" placeholder="Username" required>

                <label for="password" class="field-lbl">Password *</label>
                <input name="password" type="password" maxlength="16" id="password" class="field-input w95" placeholder="Password" / required>

                <label for="repassword" class="field-lbl">Confirm Password *</label>
                <input name="password_confirmation" type="password" maxlength="16" id="password_confirmation" class="field-input w95" placeholder="Confirm Password" required>

                <label for="email" class="field-lbl">Select Bank *</label>
                <select name="bank_name" id="bank_name" class="field-input select w95" required>
                    <option value="">- Please Select -</option>
                    <option value="POSB">POSB</option>
                    <option value="OCBC">OCBC</option>
                    <option value="DBS">DBS</option>
                    <option value="UOB">UOB</option>
                </select>

                <label for="email" class="field-lbl">Bank Acc No *</label>
                <input name="bank_account_no" type="text" id="bank_account_no" class="field-input w95" placeholder="Bank Acc No" required>

                <label for="phone" class="field-lbl">Contact No *</label>
                <input name="phone" type="text" id="phone" class="field-input w95" placeholder="Contact No" required>
                <span class="alertfont reg_phone_format"><span class="red">*</span> Phone number format should be in +65123456789 <span class="red">*</span></span>

                <label for="email" class="field-lbl">Email *</label>
                <input name="email" type="email" id="email" class="field-input w95" placeholder="Your Email" required>
            </div>
            <div class="reg-btn">
                <div class="con-btn">
                    <button class="btn" type="submit">REGISTER</button>
                </div>
            </div>
        </form>
        <br /><br />
    </div>
</div>
@endsection