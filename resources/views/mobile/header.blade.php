<head>
  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
<link rel="icon" type="image/ico" href="{{ asset('images/common/favicon.png') }}" />

<title>Singbet9</title>

<link href="{{ asset('mobile/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('mobile/css/animate.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('mobile/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('mobile/css/constant.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('mobile/css/slick.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('mobile/css/slick-theme.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ url('lib/js/nivo/themes/default/default.css') }}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{ url('lib/js/nivo/nivo-slider.css') }}" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://88659552w8.cdnasiaclub.com/wcv2/css/jquery.bxslider.css" />
<!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
<script type="text/javascript" src="{{ asset('mobile/js/jquery-1.11.3.min.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('mobile/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('mobile/js/wow.min.js') }}"></script>
<script>    new WOW().init();</script>
<script type="text/javascript" src="{{ asset('mobile/js/snap.js') }}"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<script type="text/javascript">
    function openLoginModal() {
        $('#login').modal({ show: true });
    }
    function closeLoginModal() {
        $('#login').modal('hide');
    }
    function openForgotModal() {
        $('#forgot').modal({ show: true });
    }
    function closeForgotModal() {
        $('#forgot').modal('hide');
    }
    function openNotificationModal() {
        $('#divnotification').modal({ show: true });
    }
    function openSecurityModal() {
        $('#pnlsecurity').modal({ show: true });
    }
    function closeSecurityModal() {
        $('#pnlsecurity').modal('hide');
    }
    function logout() { document.getElementById("ctl00_imgbtnlogout").click(); }
    function callmain() { document.getElementById("ctl00_btnmain").click(); }
    function viewdesktop() { document.getElementById("ctl00_lnkbtn_viewdesktop").click(); }
</script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link href="{{ secure_asset('css/constant.css') }}" rel="stylesheet" type="text/css" />
        <!-- Magnific Popup core CSS file -->
        <link rel="stylesheet" href="{{ secure_asset('plugins/magnific-popup.css') }}">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <link rel="canonical" href="Home" />

    <style type="text/css">
    .tablelogin {}
    .tablelogin .tdp {padding: 5px;}
        .overlay
        {
          position: fixed;
          z-index: 100002;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
            background-color: #808080;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }
        .overlayContent
        {
          z-index: 100003;
          margin: 250px auto;
          padding: 15px;
          width: 65px;
          height: 65px;
          background-color: #000000;
          -moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;
        }
        .overlayContent img
        {
          width: 35px;
          height: 35px;
        }
        .errmsg{color: #f59c5d; font-size: 11px; margin-top: 5px;}

        .bx-wrapper {
        background: rgba(0,0,0,0) !important;
        box-shadow: 0 0 0px #ccc !important;
        border: 0 !important;
    }

    .bx-viewport {
        background-color: rgba(0,0,0,0);
    }

    .bx-prev {
        background-size: 83px 64px;
    }

    .bx-next {
        background-size: 83px 65px;
    }
    

    .slider-sport-bg {
        background-image: url('../sport/field.png');
        background-size: 100% 100%;
        position: relative;
    }

    .bx-wrapper, .bx-viewport {
        height: 153px !important; //provide height of slider
    }


    #home .bx-wrapper, .bx-viewport {
        height: 115px !important; 
        margin-bottom: 0px;
    }

    #profile .bx-wrapper, .bx-viewport {
        height: 115px !important; 
        margin-bottom: 0px;
    }

    #messages .bx-wrapper, .bx-viewport {
        height: 115px !important; 
        margin-bottom: 0px;
    }

    #settings .bx-wrapper, .bx-viewport {
        height: 145px !important; 
        margin-bottom: 0px;
    }


    .team {
        padding-top: 1px;
    }

    .team .flag {
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 54%;
    }

    .team .btn-bet {
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: 54%;
    }

    .team p {
        color: #f1be55;
        font-size: 16px;
    }

    .slider-slots-bg {
        background-image: url('../sport/slot.jpg');
        background-size: cover;
        position: relative;
    }

    .nav-tabs {
        border-bottom: 0 solid #ddd;
        margin-bottom: 10px;
        margin-left: 18px;
    }

    .nav-tabs .nav-link {
        border: 0 solid transparent;
        border-top-right-radius: .25rem;
        border-top-left-radius: .25rem;
    }

    .nav-tabs>li a, .nav-tabs>li a:focus, .nav-tabs>li a:hover {
        color: white;
    }



    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: gold;
        cursor: default;
        background-color: rgba(0,0,0,0);

        border: 0;
        border-bottom: 1px solid #ddd;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        text-decoration: none;
        background-color: rgba(0,0,0,0);
        border: 0;
        border-bottom: 1px solid #ddd;
    }
    </style>
</head>