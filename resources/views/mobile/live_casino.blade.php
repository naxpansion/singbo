@extends('mobile.master')
@section('metas')
<title>Play Casino Games Online in Singapore | Online Betting Games</title>
<meta name="description" content="Trusted Online Site for Live Casino Game in Singapore: Play and Win Cash with Live Casino Games at SINGBET9. Blackjack, baccarat, roulette, arcade games & more.">
<link rel="canonical" href="https://singbet9.com<?php echo $_SERVER['REQUEST_URI'];?>">
@endsection
@section('banner')
<!-- ************************ BANNERS ************************ -->
<div id="divBan">
    <div id="slider">
        <div class="divBanS">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('mobile/images/ban_home1.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home2.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home3.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home4.jpg') }}" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div id="m-slider" style="display:none;">
        <div style="width:100%;">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('images/slides/1.png') }}" alt="Free bonus online casino" />
                    <img src="{{ secure_asset('images/slides/2.png') }}" alt="Free Daily bonus online casino" />
                    <img src="{{ secure_asset('images/slides/3.png') }}" alt="Member free bonus online casino singapore" />
                    <img src="{{ secure_asset('images/slides/4.png') }}" alt="Online casino Singapore free bonus" />
                    <img src="{{ secure_asset('images/slides/5.png') }}" alt="Best 918kiss slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/slide_7.jpeg') }}" alt="Mega888 slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/6.png') }}" alt="Best online casino referral bonus 25%" />
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ************************ BANNERS ************************ -->
@endsection
@section('content')

<div class="m-nav" id="m-nav">
    <div class="m-nav-container">
        <ul>
            <li class="" onclick="location.href='{{ url('/') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Sportbooks</div>
                </div>
            </li>
            <li class="mnc-over" onclick="location.href='{{ url('/best-online-live-casino-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Live Casino</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/best-online-slots-game-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Slot Game</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/online-casino-promotion-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Promotions</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- ********************** MOBILE HOME PAGE SLOT LISTING ********************** -->
<input type="submit" name="ctl00$cphMIndexBody$btnReqAcc" value=" " id="ctl00_cphMIndexBody_btnReqAcc" class="dumbutt" />
<input type="hidden" id="hdAddCMID" name="hdAddCMID" value="" />
<div class="ind-slot-m">
    <div class="m-slot">
        <div class="m-slot-d1">
            <div class="m-slot-d2" style="background-image: url('mobile/images/855.png');" >
                <div class="m-slot-d3">
                    {{-- <img src="{{ secure_asset('mobile/images/855.png') }}" alt="" /> --}}
                </div>
            </div>
            <div class="m-slot-d4">
                <div class="m-slot-d6">
                    <div align="center">
                        <a href="{{ \App\Game::find(5)->desktop_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(5)->android_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(5)->ios_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-slot">
        <div class="m-slot-d1">
            <div class="m-slot-d2" style="background-image: url('mobile/images/36sa.png');" >
                <div class="m-slot-d3">
                    {{-- <img src="{{ secure_asset('mobile/images/855.png') }}" alt="" /> --}}
                </div>
            </div>
            <div class="m-slot-d4">
                <div class="m-slot-d6">
                    <div align="center">
                        <a href="{{ \App\Game::find(6)->desktop_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(6)->android_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(6)->ios_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-slot">
        <div class="m-slot-d1">
            <div class="m-slot-d2" style="background-image: url('mobile/images/joker.png');" >
                <div class="m-slot-d3">
                    {{-- <img src="{{ secure_asset('mobile/images/855.png') }}" alt="" /> --}}
                </div>
            </div>
            <div class="m-slot-d4">
                <div class="m-slot-d6">
                    <div align="center">
                        <a href="{{ \App\Game::find(7)->desktop_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(7)->android_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(7)->ios_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-slot">
        <div class="m-slot-d1">
            <div class="m-slot-d2" style="background-image: url('mobile/images/suncity.png');" >
                <div class="m-slot-d3">
                    {{-- <img src="{{ secure_asset('mobile/images/855.png') }}" alt="" /> --}}
                </div>
            </div>
            <div class="m-slot-d4">
                <div class="m-slot-d6">
                    <div align="center">
                        <a href="{{ \App\Game::find(13)->desktop_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(13)->android_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" />
                        </a>
                        &nbsp;
                        <a href="{{ \App\Game::find(13)->ios_link }}" target="_blank">
                            <img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="" style="padding-left: 5%; padding-right: 5%; color: #fff; font-size: 10px; ">
    <h1 style="text-align: center; color: gold; font-size: 17px;">Best Online Casino Games in Singapore</h1>
    <h2  style="text-align: center; color: gold;">Play and Win Easy Cash</h2>
    <h4 style="font-size: 12px; color: gold;">About Us</h4>
    <p>SINGBET9 is the number one online casino gaming site for Singaporean players looking to enjoy the best online slots, blackjack, baccarat, roulette, arcade games, sportsbook betting and other exciting games. The games in SINGBET9 are divided into suites, each with its own uniqueness and superlative features to ensure you will enjoy a prolonged gaming experience.</p>
    <p>SINGBET9 Online Casino and Sportsbook is powered by the industry’s top software platforms – Maxbet, SBO, Rollex, Lucky Palace, 918Kiss, Newtown, Ace333, 3win8, JOKER and Playboy. We offering safe and fair gaming services in online slots and games, live dealer casino, and sports betting. </p>

    <h4 style="font-size: 12px; color: gold;">Download Free Online Casino Software or Play Instantly</h4>
    <p>Play SINGBET9’s Casino games the way you like! SINGBET9 offers a 'no download' Flash gaming platform, enabling you to instantly play a huge and diverse range of over 150 popular, as well as up and coming new games. Most of the popular game titles are available with free trials and complimentary credits, without any risks involved, under the ‘fun play’ option. Or, you may choose to add some thrill and excitement to your game with real money! Choosing to download our free casino software directly onto your computer gives you access to a bigger and wider collection of games that you may not find in the instant play 'no download' version. For those who are always on the move, SINGBET9’s mobile casino will keep you entertained wherever you go. Our mobile casino consists of multiple gaming applications which supports Android and iOS. It will definitely be worth your while if you happen to be stuck waiting for something and you might just want to kill time by slipping in some casino fun time!</p>

    <h4 style="font-size: 12px; color: gold;">The Bonuses Just Get Bigger and It Never Ends</h4>
    <p>One of the most popular bonuses for newly registered players is SINGBET9’s Welcome Bonus, which offers a 100% Bonus of up to SGD 388. Other promotions which are open to all SINGBET9 members got First Daily Deposit, Special Daily Deposit, Referral BONUS and many! Almost all of SINGBET9’s fabulous promotions run 365 days a year, meaning there will never be a day you’ll play without a bonus!</p>

    <h4 style="font-size: 12px; color: gold;">Secured, Trusted & Reliable Online Casino Singapore</h4>
    <p>SINGBET9 Online Casino Singapore offers all Singaporeans the most secured, trusted and reliable online gaming and sportsbook betting services, allowing everyone to play online casino games or bet on their favourite sports matches conveniently with peace of mind. We guarantee you that we are the best online casino site, and we ensure your privacy of sensitive personal information and the security of all money transactions are not shared to anyone at all costs. SINGBET9’s casino and betting software system has been installed with the latest firewall and encryption technology. All money and wagering transactions are carried out in Singapore Dollars (SGD) via local online transfer or manual transfer via local cash deposit machines.</p>

    <h4 style="font-size: 12px; color: gold;">Quality Customer Service 24/7</h4>
    <p>To ensure the best experience and hassle-free casino playing session at SINGBET9, we have made our customer support team available to all, 24 hours a day and 7 days a week. Our in-house support team are not robots, but they are a group of skilled customer service representatives who know exactly how to help and assist you with anything related to SINGBET9. The fastest and most efficient way to reach us is by chatting with us on LiveChat, where you’ll get your answers anytime of the day within seconds. You can also contact us via Whatsapp or WeChat, details are available at our website page.
    </p>
</div>
<div class="spaceFoot"></div>
@endsection
