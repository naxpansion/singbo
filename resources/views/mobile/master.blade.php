<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta property="og:image" content="https://singbet9.com/sharer-image.png"/>
        @yield('metas')
        <link rel="stylesheet" href="{{ secure_asset('mobile/css/style.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ secure_asset('mobile/css/mod.css') }}" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700italic,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Marcellus" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/jquery-1.12.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/jquery.form.min.js') }}"></script>
        <link rel="icon" type="image/png" href="{{ secure_asset('favicon.ico') }}" />
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato" />
        <script src="{{ secure_asset('plugins/winwheel.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
        <link rel="stylesheet" href="{{ url('css/constant.css') }}" type="text/css" />
        {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css"> --}}
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- ************************** LOGIN ************************** -->
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/checkFields.js') }}"></script>
        <script type="text/javascript">
            function doSubmit() {
                var txtUsername = document.getElementById("ctl00_txtUsername");
                var txtPass = document.getElementById("ctl00_txtPass");

                if (txtUsername.value == "") { alert("Please key in Username."); txtUsername.focus(); return false; }
                else if (txtPass.value == "") { alert("Please key in Password."); txtPass.focus(); return false; }
                else { return true; }
            }

            function doDeskVer() {
                var btnDeskVer = document.getElementById("ctl00_btnDeskVer");
                btnDeskVer.click();
            }
        </script>
        <!-- ************************** LOGIN ************************** -->
        <!-- ************************** SCRIPT: TICKER ************************** -->
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/jquery.marquee/jquery.marquee.min.js') }}"></script>
        <!-- ************************** SCRIPT: TICKER ************************** -->
        <!-- *************** NIVO SLIDER *************** -->
        <link rel="stylesheet" href="{{ secure_asset('mobile/lib/js/nivo/themes/default/default.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ secure_asset('mobile/lib/js/nivo/nivo-slider.css') }}" type="text/css" media="screen" />
        <!-- *************** NIVO SLIDER *************** -->
        <script type="text/javascript">
            function doReqAcc(dmCMID) {
                var btnReqAcc = document.getElementById("ctl00_cphMIndexBody_btnReqAcc");
                var conf = confirm("Are you sure you want to add this Product?");
                if (conf) {
                    document.getElementById("hdAddCMID").value = dmCMID;
                    btnReqAcc.click();
                }
            }
        </script>
        <style type="text/css">
            body {
            background-color: #0b1320 !important;
            }
        </style>
    </head>
    <body style="-webkit-overflow-scrolling: touch;">
        <!-- ------------- MARQUEE ------------- -->
        <div id="cover-layer" class="mask" onclick="closeNav()"></div>
        <div id="forheader">
            <div id="header">
                <div id="logo">
                    <div id="menu-open" class="m-menu" onclick="openNav()">
                        <img src="{{ secure_asset('images/m_select.png') }}" class="selectpic" alt="" />
                    </div>
                    <div id="menu-close" class="m-menu" onclick="closeNav()" style="display: none;">
                        <img src="{{ secure_asset('images/close-btn.png') }}" class="selectpic" alt="" />
                    </div>
                    <a href="{{ url('/') }}"><img src="{{ secure_asset('images/logo.png') }}" class="m_logo" alt="" /></a>
                    <div class="m-login">
                        @auth
                            <input type="button" class="btn btn-xm" value="LOGOUT" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"/>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @else
                            <a style="color: gold;" href="{{ url('/login') }}" class="btn-login btn-login-m">LOGIN</a>
                            <a href="{{ url('/registration') }}" class="btn btn-xm">REGISTER</a>
                        @endauth
                    </div>
                    <div class="l_button" onclick="location.href='{{ url('/login') }}'">
                        <span class="l_button_effect">
                            <p>LOGIN</p>
                        </span>
                    </div>
                </div>
                <div hidden class="contact" >
                    <img src="{{ secure_asset('mobile/images/call.png') }}" alt="" />
                    <p>016-3020303 / 016-3020505</p>
                    <img src="{{ secure_asset('mobile/images/wechat.png') }}" alt="" />
                    <p>singbet9 / singbet9</p>
                    <img src="{{ secure_asset('mobile/images/mail.png') }}" alt="" />
                    <p><a href="cdn-cgi/l/email-protection/index.html" class="__cf_email__" data-cfemail="5a3339362f38631a3d373b333674">[email&#160;protected]</a>com</p>
                </div>
                <div style="float: right;">
                    <div class="center">
                        <div class="center-panel">
                            <div id="ctl00_pnlProfile">
                                <div class="login">
                                    <input name="ctl00$txtUsername" type="text" id="ctl00_txtUsername" tabindex="1" class="headfld" placeholder="Username" />
                                    <div style="position: relative;">
                                        <div>
                                            <input name="ctl00$txtPass" type="password" maxlength="16" id="ctl00_txtPass" tabindex="2" class="headfld" placeholder="Password" />
                                        </div>
                                        <input id="pop-btnFp" type="button" value="Forgot?" onclick="doFP()" class="btnfp" disabled />
                                    </div>
                                    <input type="submit" name="ctl00$btnSubmit" value="LOGIN" onclick="return doSubmit();" id="ctl00_btnSubmit" tabindex="3" class="btn-login" disabled="" />
                                    <input id="pop-btnReg" type="button" value="REGISTER" onclick="location.href='{{ url('/register') }}'" class="btn" disabled />
                                </div>
                            </div>
                        </div>
                        <div id="language" class="lang" style="display:none">
                            <input type="image" name="ctl00$btnLangCN" id="ctl00_btnLangCN" src="{{ secure_asset('mobile/images/lang_cn.png') }}" border="0" style="margin-left: 2px;" />
                            <input type="image" name="ctl00$btnLangEN" id="ctl00_btnLangEN" src="{{ secure_asset('mobile/images/lang_en.png') }}" border="0" style="margin-left: 2px;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="mySidenav" class="sidenav">
            <div class="sidenav-container">
                <a href="{{ url('/') }}"">HOME</a>
                @auth
                    <a href="{{ url('player/deposit/step1') }}">DEPOSIT</a>
                    <a href="{{ url('player/withdrawal/step1') }}">WITHDRAW</a>
                    <a href="{{ url('player/transfer/step1') }}">TRANSFER</a>
                    <a href="{{ url('player/transaction') }}">TRANSACTION</a>
                    <a href="{{ url('player/rewards') }}">REWARD</a>
                    <a href="{{ url('player/profile') }}">PROFILE</a>
                @else
                    <a href="{{ url('/registration') }}">REGISTER</a>
                @endauth
                <a href="{{ url('/') }}">SPORTSBOOK</a>
                <a href="{{ url('/best-online-live-casino-singapore') }}">LIVE CASINO</a>
                <a href="{{ url('/best-online-slots-game-singapore') }}">SLOT GAME</a>
                <a href="{{ url('/online-casino-promotion-singapore') }}">PROMOTIONS</a>
                <a href="{{ url('/vip') }}">VIP CLUB</a>
                <a href="{{ url('/banking') }}">BANKING</a>
                <a href="{{ url('/affiliates') }}">AFFILIATE</a>
                <a href="#" class="mini_spin">SPIN</a>
                <div style="width:100%; clear:both; height: 15vh;"></div>
            </div>
            <div class="sidenav-footer">
                <img style="width:15px" src="{{ url('images/facebook.png') }}">
                <img style="width:15px" src="{{ url('images/instagram.png') }}">
                <img style="width:15px" src="{{ url('images/twitter.png') }}">
                <img style="width:15px" src="{{ url('images/youtube.png') }}">
                <p>Copyright© 2018 SingBet9 | <span onclick="window.location='/sitemap'">Sitemap</span></p>
            </div>
        </div>
        <div>
            <script data-cfasync="false" src="{{ secure_asset('mobile/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js') }}"></script>
            <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/navbar.js') }}"></script>
        </div>
        <div class="nav">
            <div class="nav-container">
                <ul>
                    <li class="nav-over">
                        <div onclick="window.location='/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_home.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>HOME</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/p/sport/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_sport.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>SPORTSBOOK</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/p/live/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_live.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>LIVE CASINO</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/p/slot/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_slot.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>SLOT GAME</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/promo/'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_promo.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>PROMOTIONS</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/c/?i=c6'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_vip.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>VIP CLUB</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/c/?i=c2'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_bank.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>BANKING</p>
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div onclick="window.location='/c/?i=c2'" class="nav-items">
                            <div class="nav-items-img">
                                <img src="{{ secure_asset('mobile/images/icon_bank.png') }}" alt="Home" />
                            </div>
                            <div class="nav-items-text">
                                <p>AFFILIATE</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        @yield('banner')
        @yield('content')

        @auth
        <div class="p-contact">
            <div class="m-social">
                <div class="m-xapps " onclick="location.href='/player'">
                    <img src="{{ secure_asset('mobile/images/m-home.png') }}" alt="" />
                    HOME
                </div>
                <div class="m-xapps " onclick="location.href='{{ url('player/deposit/step1') }}'">
                    <img src="{{ secure_asset('mobile/images/m-dep.png') }}" alt="" />
                    DEPOSIT
                </div>
                <div class="m-xapps " onclick="location.href='{{ url('player/withdrawal/step1') }}'">
                    <img src="{{ secure_asset('mobile/images/m-wd.png') }}" alt="" />
                    WITHDRAW
                </div>
                <div class="m-xapps " onclick="location.href='{{ url('player/transfer/step1') }}'">
                    <img src="{{ secure_asset('mobile/images/m-tfr.png') }}" alt="" />
                    TRANSFER
                </div>
                <div class="m-xapps" onclick="window.open('https://secure.livechatinc.com/licence/10962212/open_chat.cgi');">
                    <img src="{{ secure_asset('mobile/images/m-livechat.png') }}" alt="" />
                    LIVE CHAT
                </div>
            </div>
        </div>
        @else
        <div class="p-contact">
            <div class="m-social">
                <div class="m-xapps m-xapps-over" onclick="location.href='/'">
                    <img src="{{ secure_asset('mobile/images/m-home.png') }}" alt="" />
                    HOME
                </div>
                <div class="m-xapps">
                    <img src="{{ secure_asset('mobile/images/m-whatsapp.png') }}" alt="" onclick="window.open('{{ \App\Setting::find(8)->value }}');" />
                    WHATSAPP
                </div>
                <div class="m-xapps" onclick="window.open('https://secure.livechatinc.com/licence/10962212/open_chat.cgi');">
                    <img src="{{ secure_asset('mobile/images/m-livechat.png') }}" alt="" />
                    LIVE CHAT
                </div>
                <div class="m-xapps" onclick="location.href='api/switch-desktop'">
                    <img src="{{ secure_asset('mobile/images/m-desktop.png') }}" alt="" />
                    DESKTOP
                </div>
                <div class="m-xapps " >
                    <img class="mini_spin" src="{{ secure_asset('mobile/images/spin.png') }}" alt="" />
                    SPIN
                </div>
            </div>
        </div>
        @endauth
        <!-- *************************************  MAGNIFIC POPUP ************************************* -->

        <a href="#mfp-reg" class="open-popup-link" id="mfpReg"></a>
        <div id="mfp-reg" class="black-popup2 mfpReg mfp-hide"></div>
        <a href="#mfp-fp" class="open-popup-link" id="mfpfp"></a>
        <div id="mfp-fp" class="black-popup2 mfpfp mfp-hide"></div>
        @yield('popup')
        <div id="spin-wheel" class="spin-wheel mfp-hide" style="background-image: url({{ url('wheel/bg.jpeg')  }});">
            <div class="row">
                {{-- <div class="col-md-12" style="text-align: center; color: white;">
                    <h4>Every visitor are awarded with 1 free spin</h4>
                    <h4>Simply click on the 'start spin' to try your luck</h4>
                    <h4>Good Luck!</h4>
                </div> --}}
            </div>
            <div id="canvasContainer" style="">
                <canvas id='spinner' width='330' height='330'  style="margin: 0; display: block; left: 0; right: 0; margin-left: -2px;">
                    Canvas not supported, use another browser.
                </canvas>
                <img id="prizePointer" src="{{ url('wheel/mobile_arrow.png') }}" style="left: 0; right: 0;" alt="V" />
            </div>
            <br />
            <div class="row" style="text-align: center;">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <button id="spin_button" class="btn btn-block btn-lg btn-warning btn-spin" onClick="startSpin();">Start Spin</button>
                    <button id="reset_button" style="display: none;" class="btn btn-block btn-lg btn-warning" onClick="resetWheel(); return false;">Play Again</button>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row" style="text-align: center;">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <br />
                    <table class="table table-bordered table-condensed" style="width:100%; background-color: rgba(0,0,0,0.5); color: white; border-collapse: collapse;
border-radius: 5px;
padding-top: 5px;
padding-bottom: 5px;
border-style: hidden; /* hide standard table (collapsed) border */
box-shadow: 0 0 0 2px #fff; /* this draws the table border  */ ">
                        <tr>
                            <td style="vertical-align: middle;">TOKEN REMAINING :
                                @auth
                                    <span id="token">{{ \Auth::user()->token }}</span>
                                @else
                                    @php
                                        $token_check = \App\SpinIp::where('ip',\Request::ip())->count();
                                        if($token_check == 0)
                                        {
                                            $token = 0;
                                        }
                                        else
                                        {
                                            $token = 0;
                                        }
                                    @endphp
                                    <span id="token">{{ $token }}</span>
                                @endauth
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <script data-cfasync="false" src="{{ secure_asset('mobile/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js') }}"></script><script type="text/javascript">
            function doFP() {

                $.get("/m/fp.aspx", function (respondHtml) {
                    if (respondHtml) { $('#mfp-fp').html(respondHtml); $('#mfpfp').trigger('click'); }
                })
                //$('#mfp-fp').html("Hello <b>world</b>!"); $('#mfpfp').trigger('click');

            }
            function doReg() {

                $.get("/r/index.aspx").done(function (respondHtml) {
                    if (respondHtml) { $('#mfp-reg').html(respondHtml); $('#mfpReg').trigger('click'); }
                })
                //$('#mfp-reg').html("Hello <b>world</b>!"); $('#mfpReg').trigger('click');

            }

            // -- ******************  MAGNIFIC POPUP ****************** -- //
            $(window).load(function () {
                $("#pop-btnFp, #pop-btnReg, #pop-btnJoin, #ctl00_btnSubmit").removeAttr("disabled");
            });
            $(document).ready(function () {
                // --  MFP -- //
                $('.open-popup-link').magnificPopup({
                    type: 'inline',
                    midClick: true,
                    callbacks: {
                        close: function () {
                            var elemID = this.currItem.src;


                            if (elemID == "#mfp-post-login") {

                            }
                            else if ((elemID == "#mfp-txn") || (elemID == "#mfp-toreb") || (elemID == "#mfp-agrep") || (elemID == "#mfp-agdl") || (elemID == "#mfp-ann")) {
                                // -- DO NOTHING -- //
                            }
                            else {
                                location.reload();
                            }
                        }
                    }
                });
            });

            // -- ****************** JQUERY.MARQUEE ****************** -- //
            $('.marquee').marquee({
                //speed in milliseconds of the marquee
                duration: 15000,
                //gap in pixels between the tickers
                gap: 50,
                //time in milliseconds before the marquee will start animating
                delayBeforeStart: 0,
                //'left' or 'right'
                direction: 'left',
                //true or false - should the marquee be duplicated to show an effect of continues flow
                duplicated: true,
                pauseOnHover: true
            });
        </script>
        <!-- *************************************  MAGNIFIC POPUP ************************************* -->
        <link rel="stylesheet" href="{{ secure_asset('mobile/lib/js/mfp/mfp.css') }}" />
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/mfp/mfp.js') }}"></script>
        <!-- *************************************  MAGNIFIC POPUP ************************************* -->
        <script type="text/javascript" src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/nivo/jquery.nivo.slider.js') }}"></script>
        <script type="text/javascript">
            $(window).load(function () {
                $('.homeslider').nivoSlider({ controlNav: false, effect: 'fade' });
                //Hide Nivo Left Right Nav if only one image
                var count_nivoimg = $(".nivoSlider>img").length //count children with img tag
                if (count_nivoimg <= 2) {
                    $(".nivoSlider .nivo-directionNav").hide();
                }
            })


        </script>
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            if (matchMedia('only screen and (min-width: 641px)').matches) {
              window.__lc = window.__lc || {};
              window.__lc.license = 10564322;
              (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
              })();
            }
        </script>
        <noscript>
            <a href="https://www.livechatinc.com/chat-with/10564322/" rel="nofollow">Chat with us</a>,
            powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
        </noscript>
        <!-- End of LiveChat code -->
        <script type="text/javascript">
            //document.getElementById("m-slider").style.display = "block";
        </script>
        <script type="text/javascript">



            var spinPopup = $('.mini_spin').magnificPopup({
                items: {
                    src: '#spin-wheel'
                },
                type: 'inline',
                callbacks: {
                    open: function() {
                        closeNav();
                    },
                    close: function() {
                      // Will fire when popup is closed
                    }
                    // e.t.c.
                }
            }, 0);

            // let theWheel = new Winwheel({
            //     'canvasId'        : 'spinner',
            //     'drawMode'     : 'image',    // drawMode must be set to image.
            //     'numSegments'  : 8,          // The number of segments must be specified.
            //     'imageOverlay' : true,       // Set imageOverlay to true to display the overlay.
            //     'lineWidth'    : 1,          // Overlay uses wheel line width and stroke style so can set these
            //     'strokeStyle'  : 'red'       // as desired to change appearance of the overlay.
            // });

            let theWheel = new Winwheel({
                'canvasId'        : 'spinner',
                'drawMode'     : 'image',
                'responsive'      : true,
                'outerRadius'     : 212,        // Set outer radius so wheel fits inside the background.
                'innerRadius'     : 75,         // Make wheel hollow so segments dont go all way to center.
                'drawText'          : false,         // Need to set this true if want code-drawn text on image wheels.
                // 'textFontSize'      : 18,
                // 'textOrientation'   : 'curved',     // Note use of curved text.
                // 'textDirection'     : 'reversed',   // Set other text options as desired.
                // 'textAlignment'     : 'outer',
                // 'textMargin'        : 20,   // Align text to outside of wheel.
                'numSegments'     : 8,         // Specify number of segments.
                'segments'        :             // Define segments including colour and text.
                [                               // font size and text colour overridden on backrupt segments.
                   @foreach(\App\Wheel::all() as $wheel)
                        {'fillStyle' : '#ee1c24', 'text' : '{{ $wheel->name }}'},
                   @endforeach
                ],
                'animation' :           // Specify the animation to use.
                {
                    'type'     : 'spinToStop',
                    'duration' : 10,
                    'spins'    : 3,
                    'callbackFinished' : alertPrize,  // Function to call whent the spinning has stopped.
                    'callbackSound'    : playSound,   // Called when the tick sound is to be played.
                    'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound.
                },
                'pins' :                // Turn pins on.
                {
                    'visible'    : false,
                    'responsive' : true,
                    'number'     : 24,
                    'fillStyle'  : 'silver',
                    'outerRadius': 4,
                }
            });

            // Create new image object in memory.
            let loadedImg = new Image();

            // Create callback to execute once the image has finished loading.
            loadedImg.onload = function()
            {
                theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
                theWheel.draw();                    // Also call draw function to render the wheel.
            }

            // Set the image source, once complete this will trigger the onLoad callback (above).
            loadedImg.src = "{{ secure_asset('wheel/mobile_spin.png') }}";


            // Loads the tick audio sound in to an audio object.
                let audio = new Audio('{{ secure_asset('wheel/tick.mp3') }}');

                // This function is called when the sound is to be played.
                function playSound()
                {
                    // Stop and rewind the sound if it already happens to be playing.
                    audio.pause();
                    audio.currentTime = 0;

                    // Play the sound.
                    audio.play();
                }

                // Vars used by the code in this page to do power controls.
                let wheelPower    = 0;
                let wheelSpinning = false;

                // -------------------------------------------------------
                // Click handler for spin button.
                // -------------------------------------------------------
                function startSpin()
                {
                    var token = parseInt($("#token").text());

                    if(token < 1)
                    {
                        @auth

                        swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                            buttons: {
                                cancel: "Deposit",
                            },
                            icon: "warning",
                            title: "Whoops!!!!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ url('registration') }}');
                                    break;

                                default:
                                    window.location.replace('{{ url('login') }}');

                            }
                        });

                        @else

                        swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                            buttons: {
                                cancel: "Login",
                                catch: {
                                    text: "Register",
                                    value: "register",
                                },
                            },
                            icon: "warning",
                            title: "Whoops!!!!",
                        })
                        .then((value) => {
                            switch (value) {

                                case "register":
                                    window.location.replace('{{ url('registration') }}');
                                    break;

                                default:
                                    var magnificPopup = $.magnificPopup.instance;
                                    magnificPopup.close();
                                    window.location.replace('{{ url('login') }}');

                            }
                        });

                        @endauth

                    }
                    else
                    {
                        if (wheelSpinning == false)
                        {

                            var d = Math.random();
                            console.log(d);

                            var section_1 = {{ \App\Wheel::find(1)->percentage }} * 0.01;
                            var section_2 = {{ \App\Wheel::find(2)->percentage }} * 0.01;
                            var section_3 = {{ \App\Wheel::find(3)->percentage }} * 0.01;
                            var section_4 = {{ \App\Wheel::find(4)->percentage }} * 0.01;
                            var section_5 = {{ \App\Wheel::find(5)->percentage }} * 0.01;
                            var section_6 = {{ \App\Wheel::find(6)->percentage }} * 0.01;
                            var section_7 = {{ \App\Wheel::find(7)->percentage }} * 0.01;
                            var section_8 = {{ \App\Wheel::find(8)->percentage }} * 0.01;

                            if (d < section_1)
                            {
                                var prize = 23; //Try again
                            }
                            else if (d < section_1 + section_2)
                            {
                                var prize = 70; //RM8
                            }
                            else if (d < section_1 + section_2 + section_3)
                            {
                                var prize = 121; //30% Daily
                            }
                            else if (d < section_1 + section_2 + section_3 + section_4)
                            {
                                var prize = 160; //Jackpot
                            }
                            else if (d < section_1 + section_2 + section_3 + section_4 + section_5)
                            {
                                var prize = 200; //30% Daily
                            }
                            else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6)
                            {
                                var prize = 260; //RM4
                            }
                            else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7)
                            {
                                var prize = 290; //RM4
                            }
                            else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7 + section_8)
                            {
                                var prize = 340; //RM4
                            }
                            // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                            // to rotate with the duration of the animation the quicker the wheel spins.
                            theWheel.animation.spins = 6;

                            // Disable the spin button so can't click again while wheel is spinning.
                            $("#spin_button").hide();

                            // Begin the spin animation by calling startAnimation on the wheel object.
                            theWheel.animation.stopAngle = prize;
                            theWheel.startAnimation();

                            // Set to true so that power can't be changed and spin button re-enabled during
                            // the current animation. The user will have to reset before spinning again.
                            wheelSpinning = true;

                            $.post("{{ url('api/spin_ip') }}", { 'ip':'{{ \Request::ip() }}'}, function(result,status){
                                console.log(result);
                            });

                            var new_token = token - 1;
                            $("#token").text(new_token);


                        }
                    }


                }

                // -------------------------------------------------------
                // Function for reset button.
                // -------------------------------------------------------
                function resetWheel()
                {
                    theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                    theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                    theWheel.draw();                // Call draw to render changes to the wheel.

                    wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
                    $("#spin_button").show();
                    $("#reset_button").hide();
                }

                // -------------------------------------------------------
                // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
                // -------------------------------------------------------
                function alertPrize(indicatedSegment)
                {
                    // Just alert to the user what happened.
                    // In a real project probably want to do something more interesting than this with the result.
                    // alert("You have won " + indicatedSegment.endAngle);
                    @auth
                        $.post("{{ url('api/reward/auth') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                console.log(result);
                            });

                        if(indicatedSegment.endAngle == 45)
                        {
                            swal("To claim this reward please click button below.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(1)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/rewards') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 90)
                        {
                            swal("To claim this reward please make a deposit.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(2)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/deposit/step1') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 135)
                        {
                            swal("You can try again by making any deposit to us!", {
                                buttons: {
                                    cancel: "Try Again",
                                    catch: {
                                        text: "Get Token By Deposit",
                                        value: "register",
                                    },
                                },
                                icon: "warning",
                                title: "Awww! We Are So Sorry :(",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('player/deposit/step1') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 180)
                        {
                            swal("To claim this reward please click button below.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(4)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/rewards') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 225)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(5)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ route('register') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 270)
                        {

                            swal("To claim this reward please click button below.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(6)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/rewards') }}');
                                        break;

                                    default:

                                }
                            });
                        }

                        else if(indicatedSegment.endAngle == 315)
                        {

                            swal("To claim this reward please click button below.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(7)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/rewards') }}');
                                        break;

                                    default:

                                }
                            });
                        }

                        else if(indicatedSegment.endAngle == 360)
                        {
                            swal("To claim this reward please click button below.", {
                                buttons: {
                                    catch: {
                                        text: "Claim",
                                        value: "claim",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(8)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "claim":
                                        window.location.replace('{{ url('player/rewards') }}');
                                        break;

                                    default:

                                }
                            });
                        }
                    @else

                        $.post("{{ url('api/reward/guest') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                console.log(result);
                            });

                        if(indicatedSegment.endAngle == 45)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(1)->name }}!!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');
                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 90)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(2)->name }}",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 135)
                        {
                            swal("You can try again by making any deposit to us!", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "warning",
                                title: "Awww! We Are So Sorry :(",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 180)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(4)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 225)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(5)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 270)
                        {

                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(6)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 315)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(7)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                        else if(indicatedSegment.endAngle == 360)
                        {
                            swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "success",
                                title: "You Won {{ App\Wheel::find(8)->name }}!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ url('registration') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance;
                                        magnificPopup.close();
                                        window.location.replace('{{ route('login') }}');

                                }
                            });
                        }
                    @endauth
                    $("#reset_button").show();
                }
        </script>
    </body>
</html>
<!-- Localized -->
