<div id="sidebar" class="page-sidebar">
    <div class="page-sidebar-scroll">
                     
        <div class="navigation-items">
            
            @guest
            <div class="nav-item">
                <a href="{{ url('registration') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_join_now.png);">Join Now</a>
            </div>
            @else

            @endguest
            <div class="nav-item">
                <a href="{{ url('/') }}" class="main-nav active" style="background-image: url(mobile/images/common/icon_home.png);">Home</a>
            </div>
            <div class="nav-item">
                <a href="{{ url('sportsbooks') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_sportsbook.png);">Sportsbook</a>
            </div>
            <div class="nav-item">
                <a href="{{ url('live_casinos') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_live_casino.png);">Live Casino</a>
            </div>
            <div class="nav-item">
                <a href="{{ url('slots') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_slots.png);">Slots</a>
            </div>
            <div class="nav-item">
                <a href="{{ url('promotions') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_gift.png);">Promotions</a>
            </div>
            <div class="nav-item">
                <a href="{{ url('vip') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_arcade.png);">VIP</a>
            </div> 
            <div class="nav-item">
                    <a href="{{ url('banking') }}" class="main-nav " style="background-image: url(mobile/images/common/icon_faqs.png);">BANKING</a>
            </div>
            
        </div>      
        <p class="sidebar-copyright center-text">singbet9.com © Copyrights 2019. </br> All rights reserved.</p>
        <!-- <div class="desktop"><a href="javascript:document.getElementById('ctl00_lnkbtn_viewdesktop').click();">Switch to Desktop View</a></div>
        <a id="ctl00_lnkbtn_viewdesktop" href="javascript:__doPostBack('ctl00$lnkbtn_viewdesktop','')" style="color: #2980b9; visibility: hidden; position: absolute;">Switch to Desktop View</a> -->
                
    </div>
</div>