@extends('mobile.master')
@section('content')
<link href="{{ secure_asset('mobile/css/m_profile.css') }}" rel="Stylesheet" type="text/css" />
<style type="text/css">
    th {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
    }

    .trans-td {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
        text-align: center;
    }
</style>
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        @if(Session::has('message'))
            <p style="color: #fff; text-align: center;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="title-prof">
            TRANSACTION
        </div>
        <div id="productdetail" class="accframe">
            <div class="divmyacc1">
                <div align="center">
                    <div class="divmyacc2">
                        <table class="myacc">
                            <tr>
                                <td class="myacctd1">
                                    <a href="{{ url('player/transaction?tab=deposit') }}" style="color: #fff;">Deposit</a>
                                </td>
                                <td style="width: 30px;">&nbsp;</td>
                                <td class="myacctd2">
                                    <a href="{{ url('player/transaction?tab=withdrawal') }}">Withdrawal</a>
                                </td>
                                <td style="width: 30px;">&nbsp;</td>
                                <td class="myacctd2">
                                    <a href="{{ url('player/transaction?tab=transfer') }}">Transfer</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearboth"></div>
            <div id="hdMyAcc">
                <div class="acc-prodd" style="color: #fff;">
                    <div id="b-type">
                        <div id="m-b-dep" class="b-method m-bank">
                            <br />
                            @foreach($transactions as $transaction)
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th width="30%" scope="col">Transaction No</th>
                                    <td class="trans-td">#{{ sprintf('%06d', $transaction->id) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Type</th>
                                    <td class="trans-td">
                                        @php

                                            $data = json_decode($transaction->data, true);
                                            $game = \App\Game::find($data['game_id']);

                                        @endphp
                                        {{ $transaction->deposit_type }}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="col">Deposit To</th>
                                    <td class="trans-td">{{ $game->name }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Amount</th>
                                    <td class="trans-td">SGD {{ $transaction->amount }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Date</th>
                                    <td class="trans-td">
                                        {{ $transaction->updated_at->format('d M Y, h:iA') }}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="col">Status</th>
                                    <td class="trans-td">
                                        @if($transaction->status == 1)
                                            <span class="label label-info">Processing</span>
                                        @elseif($transaction->status == 3)
                                            <span class="label label-danger">Declined</span>
                                        @elseif($transaction->status == 2)
                                            <span class="label label-success">Successful</span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                            <br />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="spaceFoot"></div>
<div style="display: none;">
</div>
@endsection