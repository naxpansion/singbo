@extends('mobile.master')
@section('content')
<link href="{{ secure_asset('mobile/css/m_profile.css') }}" rel="Stylesheet" type="text/css" />
<style type="text/css">
    th {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
    }

    .trans-td {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
        text-align: center;
    }
</style>
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        @if(Session::has('message'))
            <p style="color: #fff; text-align: center;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="title-prof">
            REWARD
        </div>
        <div id="productdetail" class="accframe">
            <div id="hdMyAcc">
                <div class="acc-prodd" style="color: #fff;">
                    <div id="b-type">
                        <div id="m-b-dep" class="b-method m-bank">
                            <br />
                            @foreach($rewards as $reward)
                                @if($reward->reward == "Try Again")
                                    
                                @else
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <th width="30%" scope="col">Reward</th>
                                            <td class="trans-td">{{ $reward->reward }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Status</th>
                                            <td class="trans-td">
                                                @if($reward->status == 1)
                                                    <span class="label label-info">Pending</span>
                                                @elseif($reward->status == 3)
                                                    <span class="label label-success">Approved</span>
                                                @elseif($reward->status == 2)
                                                    <span class="label label-warning">In Progress</span>
                                                @elseif($reward->status == 4)
                                                    <span class="label label-danger">Reject</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Game</th>
                                            <td class="trans-td">{{ $reward->game->name ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Remarks</th>
                                            <td class="trans-td">{{ $reward->remarks ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Date</th>
                                            <td class="trans-td">
                                                {{ $reward->created_at->format('d M Y, h:iA') }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Action</th>
                                            <td class="trans-td">
                                                @if($reward->reward_is_percentage == 0)
                                                    @if($reward->status == 1)
                                                        <a href="{{ url('player/rewards/claim/'.$reward->id) }}"><span class="btn">Click Here To Claimed</span></a>
                                                    @else

                                                    @endif
                                                @else
                                                    @if($reward->status == 1)
                                                        <a href="{{ url('player/deposit/step1') }}"><span class="btn">Click Here To Claimed</span></a>
                                                    @else

                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                @endif
                            @endforeach
                            <span style="color: white; padding-left: 5%;">Terms & Condition</span>
                            <ol style="color: white;">
                                <li>Members can deposit UNLIMITED time per day to earn UNLIMITED game token. No maximum limit on the total deposits per day, per month or per year.</li>
                                <li>Members can enjoy this promotion with any other promotion available unless otherwise stated.</li>
                                <li>The prize amount has to be rollover at least 1x time before withdrawal can be made.</li>
                                <li>SingBet9 reserved the rights to cancel this promotion at any time, either for all players or individual player without prior notice.</li>
                                <li>To ensure fairness, all randomly awarded prizes are determined by our certified RNG (Random Number Generator), which is a computational device designed to generate a sequence of numbers or symbols that lac k any pattern and cannot be predicted based on a known sequence, so all final results will be based on the backend system’s report.</li>
                                <li>General Terms of Use specified on this site applies to all promotions.</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="spaceFoot"></div>
<div style="display: none;">
</div>
@endsection