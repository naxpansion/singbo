@extends('mobile.master')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<style type="text/css">
    th {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
    }

    .trans-td {
        border: solid 1px gold;
        margin: 0;
        background-color: #00003f;
        padding-top: 3%;
        padding-bottom: 3%;
        text-align: center;
    }
</style>
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        @if(Session::has('message'))
            <p style="color: #fff; text-align: center;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="form-acc">
            <div class="accframe">
                <div class="title-top"></div>
                <h2 style="color:#fff;">TRANSFER</h2>
                <form method="post" action="{{ url('player/transfer/step2') }}">
                    @csrf
                    <div class="proddet-cont-pad">
                        <div class="div-acc-dep">

                            <br />
                            <label class="field-lbl">Credit Transfer From *</label>
                            <select name="from_game" class="field-input select w95" required>
                                <option value="">Select Product</option>
                                <optgroup label="SportBooks">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Live Casino">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Slots">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>

                            <label class="field-lbl">Credit Transfer To *</label>
                            <select name="to_game" class="field-input select w95" required>
                                <option value="">Select Product</option>
                                <optgroup label="SportBooks">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Live Casino">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Slots">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>

                            <label class="field-lbl">Transfer Amount *</label>
                            <input name="amount" type="number" step="0.01" class="field-input w95" value="{{ old('amount') }}" required />

                            
                            
                        </div>
                    </div>
                    <div class="dep-btn">
                        <div class="con-btn">
                            <button class="btn" type="submi">SUBMIT</button>
                        </div>
                    </div>
                </form>
                <div class="space50"></div>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
</div>
<div class="spaceFoot"></div>
@endsection