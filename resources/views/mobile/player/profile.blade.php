@extends('mobile.master')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#menu6 > ul li:nth-child(2)").addClass("active");
    
        $("#ctl00_cphProfile_flUpload").change(function () {
            var filename = $("#ctl00_cphProfile_flUpload")[0].files[0].name;
            $(".filename").text(filename);
        });
    });
    $(function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var today = (month < 10 ? '0' : '') + month + '/' + (day < 10 ? '0' : '') + day + '/' + d.getFullYear();
    
        $("#datepicker").datepicker({
            dateFormat: "dd/mm/yy",
            buttonImageOnly: true,
            showAnim: 'slideDown',
            duration: 'fast',
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "1900:2019"
        });
    });
</script>
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        @if(Session::has('message'))
            <p style="color: #fff; text-align: center;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="form-acc">
            <div class="accframe">
                <div class="title-top"></div>
                <h2 style="color:#fff;">PROFILE UPDATE</h2>
                <form method="post" action="{{ url('player/profile/update') }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
                    <div class="proddet-cont-pad">
                        <div class="div-acc-dep">
                            

                            <label class="field-lbl">Email</label>
                            <input name="email" type="text" class="field-input w95" value="{{ \Auth::user()->email }}" readonly="readonly" />

                            <label class="field-lbl">Full Name</label>
                            <input name="email" type="text" class="field-input w95" value="{{ \Auth::user()->name }}" readonly="readonly" />

                            <label class="field-lbl">Contact Number</label>
                            <input name="email" type="text" class="field-input w95" value="{{ \Auth::user()->phone }}" readonly="readonly" />

                            <label class="field-lbl">Gender</label>
                            {{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], \Auth::user()->gender , ['class' => 'field-input select w95']) }}
                            
                            <label class="field-lbl">Date Of Birth</label>
                            <input type="text" name="dob" id="datepicker" value="{{ \Auth::user()->dob }}" readonly class="field-input w95" />
                            <br />
                            <button class="btn" type="submi">UPDATE</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="accframe">
                <form method="post" action="{{ url('player/profile/password') }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
                    <div class="proddet-cont-pad">
                        <div class="div-acc-dep">
                            

                            <label class="field-lbl">Current Password</label>
                            <input name="current_pass" type="password" class="field-input w95" required />

                            <label class="field-lbl">New Password</label>
                            <input name="new_pass" type="password" class="field-input w95" required />

                            <label class="field-lbl">Confirm Password</label>
                            <input name="confirm_pass" type="password" class="field-input w95" required />
                            <br />

                            <button class="btn" type="submi">UPDATE</button>
                        </div>
                    </div>
                </form>
                <div class="space50"></div>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
</div>
<div class="spaceFoot"></div>
@endsection