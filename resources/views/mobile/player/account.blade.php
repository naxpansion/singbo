@extends('mobile.master')
@section('content')
<script type="text/javascript">
    $(document).ready(function () {
        $("#menu6 > ul li:first-child").addClass("active");
        var numberBank = $("#tblbank tr th").length;
        $("#tblbank th").css("width", "calc(100%/" + numberBank + ")")
    });
    
    function doReqAC(dmCMID) {
        var btnAddAC = document.getElementById("ctl00_cphProfile_btnAddAC");
        var hdAddCMID = document.getElementById("hdAddCMID");
        hdAddCMID.value = dmCMID;
        var conf = confirm("Are you sure you want to add this Product?");
        if (conf) { btnAddAC.click(); }
    }
    
    function doSH(x) {
        var hdMyAcc = document.getElementById("hdMyAcc");
        var hdProf = document.getElementById("hdProf");
    
        if (x == "1") {
            hdMyAcc.style.display = ""; hdProf.style.display = "none";
            $("#mac1").removeClass("myacctd2").addClass("myacctd1");
            $("#mac2").removeClass("myacctd1").addClass("myacctd2");
        }
        else if (x == "2") {
            hdMyAcc.style.display = "none"; hdProf.style.display = "";
            $("#mac1").removeClass("myacctd1").addClass("myacctd2");
            $("#mac2").removeClass("myacctd2").addClass("myacctd1");
        }
    }
</script>
<link href="{{ secure_asset('mobile/css/m_profile.css') }}" rel="Stylesheet" type="text/css" />
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="/m/"><span>MY ACCOUNT</span></a></li>
                <li><a href="/m/dep/"><span>DEPOSIT</span></a></li>
                <li><a href="/m/wd/"><span>WITHDRAW</span></a></li>
                <li><a href="/m/tfr/"><span>TRANSFER</span></a></li>
                <li><a href="/m/pw.aspx" style="padding-top:2px;"><span>CHANGE PASSWORD</span></a></li>
                <li><a href="/m/logout.aspx"><span>LOGOUT</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p style="text-align: center; color: #fff;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="title-prof">
            PROFILE
        </div>
        <div id="productdetail" class="accframe">
            <div class="divmyacc1">
                <div align="center">
                    <div class="divmyacc2">
                        <table class="myacc">
                            <tr>
                                <td class="myacctd1" id="mac1" onclick="doSH('1')">My Game Accounts</td>
                                <td style="width: 30px;">&nbsp;</td>
                                <td class="myacctd2" id="mac2" onclick="doSH('2')">Profile & Bank Info</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="divVIPL" style="color: #fff;">
                @php

                    $my_deposit = \App\Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('user_id', \Auth::user()->id)->where('bday_mark',0)->sum('amount');

                @endphp
                @if($my_deposit < 30)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            You have no VIP Level Yet. Contact Us To Find Out More.<br /><br />
                            </div>
                        </div>
                    </div>
                @elseif($my_deposit > 30 && $my_deposit < 10000)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            <div class="vip-badge" style="width:100% !important">
                                <img src="/images/vip-badge-silver.png" alt="" />
                                <div class="space20"></div>
                                <div class="m-vip-1">
                                    <div class="m-vip-t1">CURRENT TOTAL DEPOSIT</div>
                                    <div class="m-vip-t2">SGD {{ number_format($my_deposit,2) }}</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">WITHDRAW LIMIT PER DAY</div>
                                    <div class="m-vip-t2">3</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">BIRTHDAY SURPRISE</div>
                                    <div class="m-vip-t2">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif($my_deposit > 9999 && $my_deposit < 49999)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            <div class="vip-badge" style="width:100% !important">
                                <img src="/images/vip-badge-gold.png" alt="" />
                                <div class="space20"></div>
                                <div class="m-vip-1">
                                    <div class="m-vip-t1">CURRENT TOTAL DEPOSIT</div>
                                    <div class="m-vip-t2">SGD {{ number_format($my_deposit,2) }}</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">WITHDRAW LIMIT PER DAY</div>
                                    <div class="m-vip-t2">4</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">BIRTHDAY SURPRISE</div>
                                    <div class="m-vip-t2">SGD 100</div>
                                </div>
                                <br />&nbsp;<br />
                                <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                            </div>
                        </div>
                    </div>
                
                @elseif($my_deposit > 50000 && $my_deposit < 99999)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            <div class="vip-badge" style="width:100% !important">
                                <img src="/images/vip-badge-plat.png" alt="" />
                                <div class="space20"></div>
                                <div class="m-vip-1">
                                    <div class="m-vip-t1">CURRENT TOTAL DEPOSIT</div>
                                    <div class="m-vip-t2">SGD {{ $my_deposit }}</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">WITHDRAW LIMIT PER DAY</div>
                                    <div class="m-vip-t2">5</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">BIRTHDAY SURPRISE</div>
                                    <div class="m-vip-t2">SGD 500</div>
                                </div>
                                <br />&nbsp;<br />
                                <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                            </div>
                        </div>
                    </div>
                @elseif($my_deposit > 99999 && $my_deposit < 149999)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            <div class="vip-badge" style="width:100% !important">
                                <img src="/images/vip-badge-diam.png" alt="" />
                                <div class="space20"></div>
                                <div class="m-vip-1">
                                    <div class="m-vip-t1">CURRENT TOTAL DEPOSIT</div>
                                    <div class="m-vip-t2">SGD {{ number_format($my_deposit,2) }}</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">WITHDRAW LIMIT PER DAY</div>
                                    <div class="m-vip-t2">7</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">BIRTHDAY SURPRISE</div>
                                    <div class="m-vip-t2">SGD 1,000</div>
                                </div>
                                <br />&nbsp;<br />
                                <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                            </div>
                        </div>
                    </div>
                @elseif($my_deposit > 149999)
                    <div align="center">
                        <div class="divVIPL2">
                            <br />MY VIP LEVEL<br /><br />
                            <div class="vip-badge" style="width:100% !important">
                                <img src="/images/vip-badge-sig.png" alt="" />
                                <div class="space20"></div>
                                <div class="m-vip-1">
                                    <div class="m-vip-t1">CURRENT TOTAL DEPOSIT</div>
                                    <div class="m-vip-t2">SGD {{ number_format($my_deposit,2) }}</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">WITHDRAW LIMIT PER DAY</div>
                                    <div class="m-vip-t2">Unlimited</div>
                                </div>
                                <div class="m-vip-1 m-vip-2">
                                    <div class="m-vip-t1">BIRTHDAY SURPRISE</div>
                                    <div class="m-vip-t2">SGD 2,000</div>
                                </div>
                                <br />&nbsp;<br />
                                <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                            </div>
                        </div>
                    </div>
                @endif
                <br /><hr />
            </div>
            <div class="clearboth"></div>
            <div id="hdMyAcc">
                <div class="acc-prod">
                    
                    @foreach(\App\Game::where('status',1)->get() as $game)
                        @php
                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first()
                        @endphp
                        @if($account === null)
                        <div class="productcontainer">
                            <div class="xcnt">
                                <div class="acc-prod-img">
                                    <img src="{{ secure_asset('storage/games/'.$game->logo) }}" alt="" />
                                </div>
                                <div class="acc-box" align="center">
                                    <div class="acc-box1 h80">
                                        <div class="acc-space"></div>
                                        <div align="center">
                                            <a href="{{ url('player/request/'.$game->id) }}" class="sport-req-acc">REQUEST ACCOUNT</a>
                                        </div>
                                    </div>
                                    <div class="acc-box1 acc-box1-bot">
                                        <div align="center">
                                            <a href="{{ $game->android_link }}"" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" /></a> 
                                            <a href="{{ $game->ios_link }}" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" /></a> 
                                            <a href="{{ $game->desktop_link }}" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="productcontainer">
                                <div class="xcnt">
                                    <div class="acc-prod-img">
                                        <img src="{{ secure_asset('storage/games/'.$game->logo) }}" alt="" />
                                    </div>
                                    <div class="acc-box" align="center">
                                        <div class="acc-box2">
                                            <div class="acc-txt">
                                                <span class="acc-txt-tit">GAME ID</span><br />
                                                <b>{{ $account->username }}</b>
                                            </div>
                                        </div>
                                        <div class="acc-box2">
                                            <div class="acc-txt">
                                                <span class="acc-txt-tit">PASSWORD</span><br />
                                                <b>{{ $account->password }}</b>
                                            </div>
                                        </div>
                                        <div class="acc-box3">
                                            <div class="acc-txt" style="float: left;">
                                                <span class="acc-txt-tit">GAME ID</span><br />
                                                <b>{{ $account->username }}</b>
                                            </div>
                                            <div class="acc-txt" style="float: right;">
                                                <span class="acc-txt-tit">PASSWORD</span><br />
                                                <b>{{ $account->password }}</b>
                                            </div>
                                        </div>
                                        <div class="acc-box1 acc-box1-bot">
                                            <div align="center">
                                                <a href="{{ $game->android_link }}"" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-android.png') }}" /></a> 
                                                <a href="{{ $game->ios_link }}" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-ios.png') }}" /></a> 
                                                <a href="{{ $game->desktop_link }}" target="_blank"><img alt="" src="{{ secure_asset('mobile/images/icon-pc.png') }}" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
            <div id="hdProf" style="display: none;">
                <div class="proddet-cont">
                    <div class="proddet-cont-pad">
                        <div class="txtprof">PROFILE DETAILS</div>
                        <div class="div-acc-prof">
                            <div class="acc-prof">
                                <div class="acc-prof-txt">
                                    <span class="acc-txt-tit">FULL NAME</span><br />
                                    <span id="ctl00_cphProfile_lbFName" style="font-weight:bold;">{{ \Auth::user()->name }}</span>
                                </div>
                            </div>
                            <div class="acc-prof">
                                <div class="acc-prof-txt">
                                    <span class="acc-txt-tit">USERNAME</span><br />
                                    <span id="ctl00_cphProfile_lbUN" style="font-weight:bold;">{{ \Auth::user()->username }}</span>
                                </div>
                            </div>
                            <div class="acc-prof">
                                <div class="acc-prof-txt">
                                    <span class="acc-txt-tit">CONTACT NO</span><br />
                                    <span id="ctl00_cphProfile_lbContNo" style="font-weight:bold;">{{ \Auth::user()->phone }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="div-acc-prof">
                            <div class="acc-prof">
                                <div class="acc-prof-txt">
                                    <span class="acc-txt-tit">BANK</span><br />
                                    <span id="ctl00_cphProfile_lbBank" style="font-weight:bold;">{{ \Auth::user()->bank_name }}</span>
                                </div>
                            </div>
                            <div class="acc-prof">
                                <div class="acc-prof-txt">
                                    <span class="acc-txt-tit">BANK ACC NO</span><br />
                                    <span id="ctl00_cphProfile_lbBankACNo" style="font-weight:bold;">{{ \Auth::user()->bank_account_no }}</span>
                                </div>
                            </div>
                        </div>
                        <br />&nbsp;<br />
                        <a href="{{ url('player/profile') }}" class="btn">EDIT PROFILE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="spaceFoot"></div>
<div style="display: none;">
</div>
@endsection