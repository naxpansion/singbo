@extends('mobile.master')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#menu6 > ul li:nth-child(2)").addClass("active");
    
        $("#ctl00_cphProfile_flUpload").change(function () {
            var filename = $("#ctl00_cphProfile_flUpload")[0].files[0].name;
            $(".filename").text(filename);
        });

        $('#now').click(function(){
            
            $('#deposit_date').val('{{ \Carbon\Carbon::now()->format('d/m/Y') }}');
            $('#deposit_hour').val('{{ \Carbon\Carbon::now()->format('h') }}');
            $('#deposit_minutes').val('{{ \Carbon\Carbon::now()->format('i') }}');
            $('#deposit_stamp').val('{{ \Carbon\Carbon::now()->format('A') }}');

        });
    });
    $(function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var today = (month < 10 ? '0' : '') + month + '/' + (day < 10 ? '0' : '') + day + '/' + d.getFullYear();
    
        $("#datepicker").datepicker({
            minDate: "-1w",
            maxDate: today
        });
    });


</script>
<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="/m/"><span>MY ACCOUNT</span></a></li>
                <li><a href="/m/dep/"><span>DEPOSIT</span></a></li>
                <li><a href="/m/wd/"><span>WITHDRAW</span></a></li>
                <li><a href="/m/tfr/"><span>TRANSFER</span></a></li>
                <li><a href="/m/pw.aspx" style="padding-top:2px;"><span>CHANGE PASSWORD</span></a></li>
                <li><a href="/m/logout.aspx"><span>LOGOUT</span></a></li>
            </ul>
        </div>
        <div class="form-acc">
            <div class="accframe">
                @if(Session::has('message'))
                    <p style="color: #fff; text-align: center; background-color: gold; padding: 10px;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="title-top"></div>
                <h2 style="color:#fff;">DEPOSIT</h2>
                <form method="post" action="{{ url('player/deposit') }}" enctype="multipart/form-data" id="depo-form">
                    @csrf
                    <div class="proddet-cont-pad">
                        <div class="div-acc-dep3">
                            <div class="txtprof">BANK INFO</div>
                            <div class="divbank-dep">
                                @foreach($banks as $bank)
                                <div class="divbnk">
                                    <div class="divbnk1">
                                        {{ $bank->name }}
                                    </div>
                                    <div class="divbnk2">
                                        <span class="acc-txt-tit">ACCOUNT NAME</span><br />
                                        {{ $bank->account_name }}<br /><br />
                                        <span class="acc-txt-tit">ACCOUNT NUMBER</span><br />
                                        {{ $bank->account_no }}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="div-acc-dep">
                            <label class="field-lbl">Select Product *</label>
                            <select name="game_id" class="field-input select w95" required>
                                <option value="">Select Product</option>
                                <optgroup label="SportBooks">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Live Casino">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Slots">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>

                            @if(\App\GameAccount::where('user_id',\Auth::user()->id)->count() == 0)
                                <sub style="color: #fff; text-align: left !important;" >You have no any active ID with us, please request game id in <a style="color: gold;" href="{{ url('player') }}">[My Account Page]</a></sub>
                                @endif

                            <label class="field-lbl">Deposit Amount *</label>
                            <input name="amount" class="field-input w95" type="number" min="{{ \App\Setting::find(5)->value }}" max="{{ \App\Setting::find(6)->value }}" step="0.01" placeholder="Deposit Amount" required/>

                            <label class="field-lbl">Deposit Method *</label>
                            <select name="payment_method" class="field-input select w95" required>
                                <option value="">- Please Select -</option>
                                <option selected="selected" value="method_online">Online Transfer</option>
                                <option value="method_atm">Cash Deposit</option>
                                <option value="method_cash">ATM Transfer</option>
                            </select>

                            <span id="ctl00_cphProfile_lbPromoErr" style="color:Red;font-weight:bold;"></span>
                            <label class="field-lbl">Deposit To Bank *</label>
                            <select name="bank" class="field-input select w95" required>
                                <option value=""> -Please Select -</option>
                                @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{ $bank->name }} - {{ $bank->account_name }}</option>
                                @endforeach
                            </select>

                            <label class="field-lbl">Date / Time *</label>
                            <input type="text" name="deposit_date" id="datepicker" value="8/14/2019" readonly class="field-input w95" />
                            <div class="time-pick" style="font-size: 0px;">
                                <select name="deposit_hour" id="deposit_hour" class="field-input select" required>
                                    <option selected="selected" value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select name="deposit_minutes" id="deposit_minutes" class="field-input select" required> 
                                    <option value="00">00</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option selected="selected" value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                </select>
                                <select name="deposit_stamp" id="deposit_stamp" class="field-input select" required>
                                    <option value="AM">AM</option>
                                    <option selected="selected" value="PM">PM</option>
                                </select>
                            </div>
                            
                            <button id="now" style="height: 25px; padding-top: 3px; width: 100%; margin: 0;" type="button" class="btn btn-default">Now</button>

                            <label class="field-lbl">Reference No.</label>
                            <input name="refference_no" type="text" id="refference_no" class="field-input w95" placeholder="Reference No." required/>
                            
                            <label class="field-lbl">Upload File *</label>
                            <label for="ctl00_cphProfile_flUpload" class="field-input w95" style="pointer-events:none;">
                            <span class="fileinput">CHOOSE FILE</span>
                            <span class="filename">No file chosen</span>
                            </label>
                            <input type="file" name="receipt" id="ctl00_cphProfile_flUpload" type="file" class="field-input" style="display:none;"/>

                            <label class="field-lbl">Select Promotion *</label>
                            <select name="bonus_code" id="bonus_select" class="field-input select w95">
                                <option selected="selected" value="">- No Bonus -</option>
                                @foreach($bonuses as $bonus)
                                    @php
                                        $exclude_arr = explode(',', $bonus->exclude_games);

                                        $prefixed_array = preg_filter('/^/', 'exclude_', $exclude_arr);

                                        $exclude_class = implode(" ", $prefixed_array);

                                    @endphp
                                    <option class="bonus_option {{ $exclude_class }}" value="{{ $bonus->bonus_code }}">{{ $bonus->name }}</option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
                    <div class="dep-btn">
                        <div class="con-btn">
                            <button class="btn" type="submi">SUBMIT</button>
                        </div>
                    </div>
                </form>
                <div class="space50"></div>
            </div>
        </div>
    </div>
</div>
<div style="display: none;">
</div>
<div class="spaceFoot"></div>
@endsection