@extends('mobile.master')
@section('metas')
<title>Promotions: Online Casino Games with Free Bonus | SINGBET9</title>
<meta name="description" content="Free online games to win real money no deposit - 100% WELCOME BONUS UP TO SGD 388, FIRST DAILY DEPOSIT BONUS 30%, MEMBER DAILY DEPOSIT BONUS 20% and more.">
<link rel="canonical" href="https://singbet9.com<?php echo $_SERVER['REQUEST_URI'];?>">
@endsection
@section('banner')
<!-- ************************ BANNERS ************************ -->
<div id="divBan">
    <div id="slider">
        <div class="divBanS">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('mobile/images/ban_home1.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home2.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home3.jpg') }}" alt="" />
                    <img src="{{ secure_asset('mobile/images/ban_home4.jpg') }}" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div id="m-slider" style="display:none;">
        <div style="width:100%;">
            <div class="slider-wrapper theme-default">
                <div class="nivoSlider homeslider">
                    <img src="{{ secure_asset('images/slides/1.png') }}" alt="Free bonus online casino" />
                    <img src="{{ secure_asset('images/slides/2.png') }}" alt="Free Daily bonus online casino" />
                    <img src="{{ secure_asset('images/slides/3.png') }}" alt="Member free bonus online casino singapore" />
                    <img src="{{ secure_asset('images/slides/4.png') }}" alt="Online casino Singapore free bonus" />
                    <img src="{{ secure_asset('images/slides/5.png') }}" alt="Best 918kiss slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/slide_7.jpeg') }}" alt="Mega888 slot game free bonus" />
                    <img src="{{ secure_asset('images/slides/6.png') }}" alt="Best online casino referral bonus 25%" />
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ************************ BANNERS ************************ -->
@endsection
@section('content')

<div class="m-nav" id="m-nav">
    <div class="m-nav-container">
        <ul>
            <li class="" onclick="location.href='{{ url('/') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Sportbooks</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/best-online-live-casino-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Live Casino</div>
                </div>
            </li>
            <li class="" onclick="location.href='{{ url('/best-online-slots-game-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Slot Game</div>
                </div>
            </li>
            <li class="mnc-over" onclick="location.href='{{ url('/online-casino-promotion-singapore') }}'">
                <div class="m-nav-items">
                    <div class="m-nav-items-text">Promotions</div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- ********************** MOBILE HOME PAGE SLOT LISTING ********************** -->
<div id="ctl00_cphBody_bg" class="promotion">
    <div align="center">
        <div class="space30"></div>
        ﻿
        <link rel="stylesheet" type="text/css" href="{{ secure_asset('mobile/lib/js/animatedscroller/accordion.css') }}" />

        @foreach(\App\Promotion::get() as $promotion)
            @php
                $cats_raws = explode(',',$promotion->category);
                $cats = '';

                foreach($cats_raws as $cats_raw)
                {
                    $cats .= str_replace(' ','',$cats_raw).' ';
                }
            @endphp
            <!-- ************************************* 100% WELCOME BONUS *************************************-->
            <div class="accordionBanner">
                <table class="tb-promo" border="0">
                    <tr>
                        <td width="*">{{ $promotion->title }}</td>
                        <td width="45"><img src="{{ secure_asset('mobile/images/promo-head.png') }}" /></td>
                        <td width="150" class="td-promo">&nbsp;</td>
                    </tr>
                </table>
                <img src="{{ secure_asset('storage/promotions/'.$promotion->img_mobile) }}" class="promo-large-img" />
                <img src="{{ secure_asset('storage/promotions/'.$promotion->img_mobile) }}" class="promo-small-img" />
            </div>
            <div class="accordionContent">
                <div style="padding:10px;" class="dinpro txt14">
                    {!! $promotion->mobile !!}
                </div>
            </div>
            <div class="space20"></div>
        @endforeach


        <div class="space30"></div>
        <script data-cfasync="false" src="{{ secure_asset('mobile/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js') }}"></script>
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/animatedscroller/ddaccordion.js') }}"></script>
        <script type="text/javascript" src="{{ secure_asset('mobile/lib/js/animatedscroller/accordian.js') }}"></script>
    </div>
</div>
<div class="spaceFoot"></div>
@endsection
