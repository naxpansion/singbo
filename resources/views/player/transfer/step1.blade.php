@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li class="active"><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4>Transfer</h4>
        <div class="member-main">
            @if(\App\GameAccount::where('user_id',\Auth::user()->id)->count() == 0)
            <sub style="color: white;">(You have no any active ID with us, please request game id in <a href="{{ url('player') }}">[My Account Page]</a></sub>
            @endif
            <br /><br />
            <form method="post" action="{{ url('player/transfer/step2') }}">
            @csrf
            <div class="member-row">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                    <tr>
                        <td style="width: 200px;">Credit Transfer From</td>
                        <td>
                            <select name="from_game" class="optiongroup field-txtbox">
                                <option value="">Select Product</option>
                                <optgroup label="SportBooks">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Live Casino">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Slots">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>

                            <span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>Credit Transfer To</td>
                        <td>
                            <select name="to_game"  class="optiongroup field-txtbox">
                                <option value="">Select Product</option>
                                <optgroup label="SportBooks">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Live Casino">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                                <optgroup label="Slots">
                                    @php
                                        $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                    @endphp
                                    @foreach($games as $game)
                                        @php
                                            $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                        @endphp
                                        @if($account)
                                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            </select>
                            <span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>Amount (SGD)</td>
                        <td>
                            <input name="amount" min="{{ \App\Setting::find(14)->value }}" max="{{ \App\Setting::find(15)->value }}" type="number" step="0.01" class="field-txtbox" required/>
                            <span class="text-error">
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clearfix">
                <input type="submit" name="ctl00$MainContent$btnstep1next" value="SUBMIT" id="ctl00_MainContent_btnstep1next" class="btn btn-warning btn-more pull-right" />
            </div>
            </form>
        </div>
    </div>
@endsection