@extends ('front.master')
@section('content')
<style type="text/css">
    a.disabled {
      pointer-events: none;
      cursor: default;
    }
</style>

<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li class="active"><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>

        <div id="productdetail">
            <div class="row">
                <div class="col-md-8">
                    <div class="uni-title">
                        <div class="tgacc">
                            <img src="/images/splash.png" alt="" />
                            <p>MY GAME ACCOUNTS</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-4" style="">
                    <br />
                    @php

                        $my_deposit = \App\Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('user_id', \Auth::user()->id)->where('bday_mark',0)->sum('amount');

                    @endphp
                    <h5 style="color: gold;">VIP LEVEL</h5>
                    @if($my_deposit < 10000)
                        <table class="tbvip" border="0" style="color: white; border: double gold;">
                            <tr>
                                <td style="padding: 10px; text-align: center;">

                                    <div class="noVIP">
                                        <div class="noVIP2">
                                            <img src="{{ url('images/vip-badge-silver.png') }}" alt="Singbet9 online casino vip silver" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <table class="tbvip2" border="0">
                                            <tr>
                                                <td>Current total deposit</td>
                                                <td>SGD {{ number_format($my_deposit,2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Withdraw limit per day</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>Birthday Surprise</td>
                                                <td>No</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    @elseif($my_deposit > 9999 && $my_deposit < 49999)
                        <table class="tbvip" border="0" style="color: white; border: double gold;">
                            <tr>
                                <td style="padding: 10px; text-align: center;">

                                    <div class="noVIP">
                                        <div class="noVIP2">
                                            <img src="{{ url('images/vip-badge-gold.png') }}" alt="Singbet9 online casino vip gold" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <table class="tbvip2" border="0">
                                            <tr>
                                                <td>Current total deposit</td>
                                                <td>SGD {{ number_format($my_deposit,2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Withdraw limit per day</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td>Birthday Surprise</td>
                                                <td>SGD 100.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="text-align: center;">
                        </div>
                    @elseif($my_deposit > 50000 && $my_deposit < 99999)
                        <table class="tbvip" border="0" style="color: white; border: double gold;">
                            <tr>
                                <td style="padding: 10px; text-align: center;">

                                    <div class="noVIP">
                                        <div class="noVIP2">
                                            <img src="{{ url('images/vip-badge-plat.png') }}" alt="" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <table class="tbvip2" border="0">
                                            <tr>
                                                <td>Current total deposit</td>
                                                <td>SGD {{ number_format($my_deposit,2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Withdraw limit per day</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <td>Birthday Surprise</td>
                                                <td>SGD 500.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="text-align: center;">
                        </div>
                    @elseif($my_deposit > 99999 && $my_deposit < 149999)
                        <table class="tbvip" border="0" style="color: white; border: double gold;">
                            <tr>
                                <td style="padding: 10px; text-align: center;">

                                    <div class="noVIP">
                                        <div class="noVIP2">
                                            <img src="{{ url('images/vip-badge-diam.png') }}" alt="Singbet9 online casino vip diamond" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <table class="tbvip2" border="0">
                                            <tr>
                                                <td>Current total deposit</td>
                                                <td>SGD {{ number_format($my_deposit,2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Withdraw limit per day</td>
                                                <td>7</td>
                                            </tr>
                                            <tr>
                                                <td>Birthday Surprise</td>
                                                <td>SGD 1,000.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="text-align: center;">
                        </div>
                    @elseif($my_deposit > 149999)
                        <table class="tbvip" border="0" style="color: white; border: double gold;">
                            <tr>
                                <td style="padding: 10px; text-align: center;">

                                    <div class="noVIP">
                                        <div class="noVIP2">
                                            <img src="{{ url('images/vip-badge-sig.png') }}" alt="Singbet9 online casino vip signature" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center">
                                        <table class="tbvip2" border="0">
                                            <tr>
                                                <td>Current total deposit</td>
                                                <td>SGD {{ number_format($my_deposit,2) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Withdraw limit per day</td>
                                                <td>Unlimited</td>
                                            </tr>
                                            <tr>
                                                <td>Birthday Surprise</td>
                                                <td>SGD 2,000.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ url('player/birthday') }}" class="btn5 btn-block" style="text-align: center; width: 100%;">Claim Birthday Bonus</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="text-align: center;">
                        </div>
                    @else
                    <table class="tbvip" border="0" style="color: white; border: double gold;">
                        <tr>
                            <td style="padding: 10px; text-align: center;">

                                <div class="noVIP">
                                    <div class="noVIP2">
                                        You have no VIP level yet, please contact our customer service for more information
                                    </div>
                                </div>

                            </td>
                        </tr>
                    </table>
                    @endif

                </div>
            </div>
            <div style="clear:both;"></div>

            <div class="acc-prod">
                @foreach(\App\Game::where('status',1)->get() as $game)
                    @php
                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first()
                    @endphp
                    @if($account === null)
                    <div class="productcontainer">
                        <div class="xcnt">
                            <div class="acc-prod-img">
                                <img src="{{ secure_asset('storage/games/'.$game->logo) }}" alt="" />
                            </div>
                            <div class="upperslot2">
                                <a href="{{ url('player/request/'.$game->id) }}" class="btn5">REQUEST ACCOUNT</a>
                            </div>
                            <div class="img_container">
                                <a href="{{ $game->desktop_link }}" target="_blank"><img alt="" src="/shared/images/btn pc.png" /></a>
                                <a href="{{ $game->android_link }}" target="_blank"><img alt="" src="/shared/images/btn android.png" /></a>
                                <a href="{{ $game->ios_link }}"><img alt="" src="/shared/images/btn ios.png" /></a>
                            </div>
                            <div class="bottomslot"">
                            </div>
                            <div class="h-line" style="height:0.3vh;display:none;"></div>
                        </div>
                    </div>
                    @else
                        <div class="productcontainer">
                            <div class="xcnt">
                                <div class="acc-prod-img">
                                    <img src="{{ secure_asset('storage/games/'.$game->logo) }}" alt="" />
                                </div>
                                <div class="upperslot">
                                    <div class="up-usr">
                                        <p>USERNAME</p>
                                        <p>{{ $account->username }}</p>
                                    </div>
                                    <div class="up-usr">
                                        <p>PASSWORD</p>
                                        <p>{{ $account->password }}</p>
                                    </div>
                                </div>
                                <div class="img_container">
                                    <a href="{{ $game->desktop_link }}" target="_blank"><img alt="" src="/shared/images/btn pc.png" /></a>
                                    <a href="{{ $game->android_link }}" target="_blank"><img alt="" src="/shared/images/btn android.png" /></a>
                                    <a href="{{ $game->ios_link }}"><img alt="" src="/shared/images/btn ios.png" /></a>
                                </div>
                                <div class="bottomslot"">
                                </div>
                                <div class="h-line" style="height:0.3vh;display:none;"></div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $('a').click(function(){

        $(".btn5").addClass('disabled');
        
    });
</script>

@endsection
