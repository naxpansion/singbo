@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li class="active"><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4>Transaction</h4>               
        <div class="member-main">
            <div class="member-row">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class=""><a href="{{ url('player/transaction?tab=deposit') }}">DEPOSIT</a></li>&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;
                    <li role="presentation" class="active"><a href="{{ url('player/transaction?tab=withdrawal') }}">WITHDRAWAL</a></li>&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;
                    <li role="presentation" class=""><a href="{{ url('player/transaction?tab=transfer') }}">TRANSFER</a></li>
                </ul>
                <div id="ctl00_MainContent_UpdatePanel1">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="deposit">
                            <div>
                                <table class="table table-bordered" cellspacing="0" border="0" id="ctl00_MainContent_dgDeposit" style="width:100%;border-collapse:collapse; color: white;">
                                    <tr>
                                        <td colspan="7">
                                            <table cellspacing="0" border="0" style="width:100%;border-collapse:collapse;" class="table-member">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Transaction No</th>
                                                    <th scope="col">Withdraw From</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                                @if($transactions->count() == 0)
                                                <tr >
                                                    <th scope="col" colspan="7" style="padding-top: 10px; text-align: center">No Transaction</th>
                                                </tr>
                                                @else
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($transactions as $transaction)
                                                        <tr>
                                                            <td scope="col">{{ $i }}</td>
                                                            <td scope="col">#{{ sprintf('%06d', $transaction->id) }}</td>
                                                            @php

                                                                $data = json_decode($transaction->data, true);
                                                                $game = \App\Game::find($data['game_id']);

                                                            @endphp
                                                            <td scope="col">{{ $game->name }}</td>
                                                            <td scope="col">SGD {{ $transaction->amount }}</td>
                                                            <td scope="col">{{ $transaction->created_at->format('d M Y h:i A') }}</td>
                                                            <td scope="col">
                                                                @if($transaction->status == 1)
                                                                    <span class="label label-info">Processing</span>
                                                                @elseif($transaction->status == 3)
                                                                    <span class="label label-danger">Rejected</span>
                                                                @elseif($transaction->status == 2)
                                                                    <span class="label label-success">Succesfull</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                    @endforeach

                                                @endif
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection