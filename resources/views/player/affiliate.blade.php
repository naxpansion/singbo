@extends ('front.master')
@section('content')
<style type="text/css">
    .ui-datepicker-trigger {
        display: none !important;
    }
</style>

<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class=""><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li class=""><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li class=""><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>

                <li class="active"><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        <br /><br />
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br />
                        
        <div class="member-main text-center">
            @if(\Auth::user()->apply_aff == 0)
                <div class="member-row">
                <h4>Affiliate</h4>

                    <p style="color: #fff;">Click button below to request to be a Singbet9 Affiliate Members</p>
                    <a href="{{ url('player/affiliate/request') }}" onclick="return confirm('Are you sure?');" class="btn btn-info">Request To Be Affiliate</a>
                </div>
            @elseif(\Auth::user()->apply_aff == 1)
                <div class="member-row">
                <h4>Affiliate</h4>

                    <p style="color: #fff;">Your request are still in progress. Please wait for admin approval.</p>
                </div>
            @elseif(\Auth::user()->apply_aff == 2)
                <div class="member-row">
                    <h4>Affiliate Details</h4>
                    <br />
                    <div class="member-main">
                        <div class="member-row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                                @php
                                    $user = \App\User::find(\Auth::user()->id);

                                    if(isset($_GET['from']))
                                    {
                                        $arrStart = explode("-", $_GET['from']);
                                        $arrEnd = explode("-", $_GET['to']);

                                        $from = \Carbon\Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
                                        $to = \Carbon\Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);
                                    }
                                    else
                                    {
                                        $from = \Carbon\Carbon::now()->startOfMonth();
                                        $to = \Carbon\Carbon::now()->endOfMonth();
                                    }

                                    

                                    $members = \App\User::where('referred_by',$user->affiliate_id)->get();

                                    $deposit_sum = 0;
                                    $withdraw_sum = 0;
                                    $bonus_sum = 0;

                                    foreach($members as $member)
                                    {
                                        $dep = \App\Transaction::where('user_id',$member->id)->where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('created_at','>=',$from)->where('created_at','<=',$to)->sum('amount');
                                        $with = \App\Transaction::where('user_id',$member->id)->where('transaction_type','withdraw')->where('status',2)->where('created_at','>=',$from)->where('created_at','<=',$to)->sum('amount');

                                        $bonus = \App\Transaction::where('user_id',$member->id)->where('transaction_type','deposit')->whereIn('deposit_type',['bonus','rebate','birthday'])->where('status',2)->where('created_at','>=',$from)->where('created_at','<=',$to)->sum('amount');

                                        $deposit_sum = $deposit_sum + $dep;
                                        $withdraw_sum = $withdraw_sum + $with;
                                        $bonus_sum = $bonus_sum + $bonus;
                                    }

                                    $winlose = $deposit_sum - $withdraw_sum - $bonus_sum;

                                    $commision_rate = $user->affiliate_rate;
                                    $final_commision = $commision_rate / 100 * $winlose;
                                @endphp
                                <tr>
                                    <td width="40%">Affiliate ID</td>
                                    <td>{{ \Auth::user()->affiliate_id }}</td>
                                </tr>
                                <tr>
                                    <td>Affiliate URL</td>
                                    <td style="text-transform: lowercase;">{{ url('/') }}/?ref={{ \Auth::user()->affiliate_id }}</td>
                                </tr>
                            </table>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <form method="get">
                                        <br />
                                        <br />
                                        <h4>Affiliate Report</h4>
                                        <br />
                                        <label style="color: white;">From</label>
                                        <input name="from" type="text" maxlength="10" class="datepicker" value="{{ $from->format('d-m-Y') }}" required/>
                                        <label style="color: white;">To</label>
                                        <input name="to" type="text" maxlength="10" class="datepicker" value="{{ $to->format('d-m-Y') }}" required/>
                                        &nbsp;<button type="submit" class="btn btn-info">Filter</button>
                                        <br />&nbsp;<br />
                                    </form>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                                <tr>
                                    <td width="40%">Total Affiliate Members Register</td>
                                    <td>{{ $members->count() }}</td>
                                </tr>
                                <tr>
                                    <td>Total Affiliate Members Deposit</td>
                                    <td>SGD {{ number_format($deposit_sum,2) }}</td>
                                </tr>
                                <tr>
                                    <td>Total Affiliate Members Withdrawal</td>
                                    <td>SGD {{ number_format($withdraw_sum,2) }}</td>
                                </tr>
                                <tr>
                                    <td>Total Affiliate Members Bonus</td>
                                    <td>SGD {{ number_format($bonus_sum,2) }}</td>
                                </tr>

                                <tr>
                                    <td>This Month Commision</td>
                                    <td>SGD {{ number_format($final_commision,2) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br /><br />
                    
                </div>
            @endif
        </div>
    </div>

@endsection
@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        $(".datepicker").datepicker({
                dateFormat: "dd-mm-yy",
                showOn: 'both',
                buttonImage: "{{ secure_asset('images/date.png') }}",
                buttonText: "Open datepicker",
                buttonImageOnly: true,
                showAnim: 'slideDown',
                duration: 'fast',
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:2019"
            });
    </script>

@endsection