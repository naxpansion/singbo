@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class=""><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4 style="color: gold;">Please enter all information for your birthday bonus</h4>
        
        <div class="member-main">
            @php

                $my_deposit = \App\Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('user_id', \Auth::user()->id)->where('bday_mark',0)->sum('amount');

            @endphp
            <form method="post" action="{{ url('player/birthday') }}" enctype="multipart/form-data">
                @csrf
                @if($my_deposit > 9999 && $my_deposit < 49999)
                    <input type="hidden" name="amount" value="100">
                @elseif($my_deposit > 50000 && $my_deposit < 99999)
                    <input type="hidden" name="amount" value="500">
                @elseif($my_deposit > 100000 && $my_deposit < 149999)
                    <input type="hidden" name="amount" value="1000">
                @elseif($my_deposit > 150000)
                    <input type="hidden" name="amount" value="2000">
                @endif
                <div class="member-row">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed borderless" style="color: white;">
                        <tr>
                            <td class="align-middle">Product Games</td>
                            <td>
                                <select name="game_id"  class="field-register field-input select" required>
                                    <option value="">Select Game</option>
                                    @foreach($games as $game)
                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">Scanned / Copy of your NRIC</td>
                            <td>
                                <div id="ctl00_MainContent_UpdatePanel5">
                                    <input type="file" name="receipt" class="field-input" id="receipt" value="{{ old('receipt') }}" required/>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <ul style="color: white;">
                        <li>Your VIP level will reset to SILVER again after this request.</li>
                        <li>You can select only 1 product to get the bonus.</li>
                        <li>SINGBET9 have a right to decline your bonus request.</li>
                    </ul>
                </div>
                <br /><br /><br />
                <div class="clearfix">
                    <a href="{{ URL::previous() }}"><button type="button" class="btn btn-warning btn-more pull-left">BACK</button></a>
                    <button type="submit" class="btn btn-warning btn-more pull-right">SUBMIT</button>
                </div>
            </form>
        </div>
        <br />
        <br />
        <br />
@endsection