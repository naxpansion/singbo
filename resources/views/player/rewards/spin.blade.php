@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class=""><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li class="active"><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
                        
        <div class="member-main">
            <div class="member-row">
                <h4>Reward</h4>
                <br />
                <div id="ctl00_MainContent_UpdatePanel1">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="deposit">
                            <div>
                                <table class="table-member" cellspacing="0" border="0" id="ctl00_MainContent_dgDeposit" style="width:100%;border-collapse:collapse;">
                                    <tr>
                                        <td colspan="7">
                                            <table cellspacing="0" border="0" style="width:100%;border-collapse:collapse; color: white;" class="table table-bordered">
                                                <tr>
                                                    <th scope="col">Reward</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col" width="10%">Game</th>
                                                    <th scope="col" width="10%">Remarks</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                                @if($rewards->count() == 0)
                                                <tr >
                                                    <th scope="col" colspan="7" style="padding-top: 10px; text-align: center">No Reward</th>
                                                </tr>
                                                @else
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($rewards as $reward)
                                                        @if($reward->reward == "Try Again")
                                                            
                                                        @else
                                                            <tr>
                                                                <td scope="col">{{ $reward->reward }}</td>
                                                                <td scope="col">
                                                                    @if($reward->status == 1)
                                                                        <span class="label label-info">Pending</span>
                                                                    @elseif($reward->status == 3)
                                                                        <span class="label label-success">Approved</span>
                                                                    @elseif($reward->status == 2)
                                                                        <span class="label label-warning">In Progress</span>
                                                                    @elseif($reward->status == 4)
                                                                        <span class="label label-danger">Reject</span>
                                                                    @endif
                                                                </td>
                                                                <td>{{ $reward->game->name ?? '-' }}</td>
                                                                <td>{{ $reward->remarks ?? '-' }}</td>
                                                                <td scope="col">{{ $reward->created_at->format('d M Y, h:iA') }}</td>
                                                                <td scope="col">
                                                                    @if($reward->reward_is_percentage == 0)
                                                                        @if($reward->status == 1)
                                                                            <a href="{{ url('player/rewards/claim/'.$reward->id) }}"><span class="label label-default">Click Here To Claimed</span></a>
                                                                        @else

                                                                        @endif
                                                                    @else
                                                                        @if($reward->status == 1)
                                                                            <a href="{{ url('player/deposit/step1') }}"><span class="label label-default">Click Here To Claimed</span></a>
                                                                        @else

                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @php
                                                        $i++;
                                                    @endphp
                                                    @endforeach

                                                @endif
                                            </table>
                                            {{ $rewards->links() }}
                                            <span style="color: white;">Terms & Condition</span>
                                            <ol style="color: white;">
                                                <li>Members can deposit UNLIMITED time per day to earn UNLIMITED game token. No maximum limit on the total deposits per day, per month or per year.</li>
                                                <li>Members can enjoy this promotion with any other promotion available unless otherwise stated.</li>
                                                <li>The prize amount has to be rollover at least 1x time before withdrawal can be made.</li>
                                                <li>SingBet9 reserved the rights to cancel this promotion at any time, either for all players or individual player without prior notice.</li>
                                                <li>To ensure fairness, all randomly awarded prizes are determined by our certified RNG (Random Number Generator), which is a computational device designed to generate a sequence of numbers or symbols that lac k any pattern and cannot be predicted based on a known sequence, so all final results will be based on the backend system’s report.</li>
                                                <li>General Terms of Use specified on this site applies to all promotions.</li>
                                            </ol>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection