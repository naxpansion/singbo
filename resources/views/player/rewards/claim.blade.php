@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class=""><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li class="active"><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4>Step 1 : Reward Request.</h4>
        <div class="member-main">
            <form method="post" action="{{ url('player/rewards/claim/'.$spin->id) }}">
            @csrf
            <div class="member-row">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                    <tr>
                        <td style="width: 200px;">Reward</td>
                        <td>
                            {{ $spin->reward }}
                        </td>
                    </tr>
                    <tr>
                        <td>Reward transfer To</td>
                        <td>
                            <select name="game"  class="optiongroup field-txtbox" required>
                                <option selected="selected" value="">- Select -</option>
                                @foreach($games as $game)
                                    <option value="{{ $game->id }}">{{ $game->name }}</option>
                                @endforeach
                            </select>
                            <span class="text-error">
                            </span>
                        </td>
                    </tr>
                </table>
                <span style="color:white;">Notes:</span>
                <ol style="color:white;">
                    <li>Reward cant be transfer again after approval.</li>
                </ol>
            </div>
            <div class="clearfix">
                <input type="submit" name="ctl00$MainContent$btnstep1next" value="SUBMIT" id="ctl00_MainContent_btnstep1next" class="btn btn-warning btn-more pull-right" />
            </div>
            </form>
        </div>
    </div>
@endsection