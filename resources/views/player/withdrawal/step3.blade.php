@extends ('front.master')
@section('content')

<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li class="active"><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <div class="member-main">
            <div class="member-row">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                    <tr>
                        <td style="width: 200px;">Withdrawal From</td>
                        <td>
                            @php

                            $data = json_decode($transaction->data, true);
                            $game = \App\Game::find($data['game_id']);

                            @endphp

                            {{ $game->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>Amount (SGD)</td>
                        <td>{{ $transaction->amount }}</td>
                    </tr>
                    <tr>
                        <td>Withdrawal To</td>
                        <td>{{ \Auth::user()->bank_name }}</td>
                    </tr>
                    <tr>
                        <td>Account Name</td>
                        <td>{{ \Auth::user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Account No</td>
                        <td>{{ \Auth::user()->bank_account_no }}</td>
                    </tr>
                    <tr>
                        <td>Transaction No</td>
                        <td>{{ $transaction->transaction_id }}</td>
                    </tr>
                </table>
            </div>
            <br />
            <div class="clearfix">
                <a class="btn" href="{{ url('player') }}">Back To My Account</a>
            </div>
        </div>
    </div>
@endsection