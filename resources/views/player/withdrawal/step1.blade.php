@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li class="active"><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4>Withdraw</h4>
        <div class="member-main">
            <div class="member-row">
                <form method="post" action="{{ url('player/withdrawal/step3') }}">
                @csrf
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                    <tr>
                        <td style="width: 200px;">Bank Name</td>
                        <td>{{ \Auth::user()->bank_name }}</td>
                    </tr>
                    <tr>
                        <td>Account Name</td>
                        <td>{{ \Auth::user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>{{ \Auth::user()->bank_account_no }}</td>
                    </tr>
                </table>
                <p style="color: white;">* Please contact our customer service if any inaccurate of your bank info.</p>
                <br />
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table borderless" style="color: white;">
                    <tr>
                        <td width="40%" class="align-middle">Withdrawal From</td>
                        <td>
                            <div id="ctl00_MainContent_upddl">
                                <select name="game_id" id="game_id" class="field-input select" required>
                                    <option value="">Select Product</option>
                                    <optgroup label="SportBooks">
                                        @php
                                            $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                        @endphp
                                        @foreach($games as $game)
                                            @php
                                                $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                            @endphp
                                            @if($account)
                                                <option value="{{ $game->id }}">{{ $game->name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Live Casino">
                                        @php
                                            $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                        @endphp
                                        @foreach($games as $game)
                                            @php
                                                $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                            @endphp
                                            @if($account)
                                                <option value="{{ $game->id }}">{{ $game->name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Slots">
                                        @php
                                            $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                        @endphp
                                        @foreach($games as $game)
                                            @php
                                                $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                            @endphp
                                            @if($account)
                                                <option value="{{ $game->id }}">{{ $game->name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                                @if(\App\GameAccount::where('user_id',\Auth::user()->id)->count() == 0)
                                <sub>(You have no any active ID with us, please request game id in <a href="{{ url('player') }}">[My Account Page]</a></sub>
                                @endif
                                <span class="text-error"></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle">Amount (SGD)</td>
                        <td>
                            <input name="amount" type="number" step="0.01" min="{{ \App\Setting::find(12)->value }}" max="{{ \App\Setting::find(13)->value }}" id="amount" class="field-input" required/>
                            <span class="text-error">
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <br /><br />
            <div class="clearfix">
                <button type="submit" class="btn btn-warning btn-more pull-right">NEXT</button>

                </form>
            </div>

        </div>
    </div>
    @endsection